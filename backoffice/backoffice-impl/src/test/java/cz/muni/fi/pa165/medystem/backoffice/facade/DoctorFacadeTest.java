package cz.muni.fi.pa165.medystem.backoffice.facade;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import cz.muni.fi.pa165.medystem.backoffice.entity.Doctor;
import cz.muni.fi.pa165.medystem.backoffice.mapper.DoctorMapper;
import cz.muni.fi.pa165.medystem.backoffice.service.DoctorService;
import cz.muni.fi.pa165.medystem.backoffice.service.PatientService;
import cz.muni.fi.pa165.medystem.backoffice.util.TestData;
import dto.doctor.CreateDoctorRequestDto;
import dto.doctor.DoctorViewResponseDto;
import dto.doctor.UpdateDoctorRequestDto;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class DoctorFacadeTest {

  @Mock
  private DoctorService doctorService;
  @Mock
  private PatientService patientService;

  @Mock
  private DoctorMapper doctorMapper;

  @InjectMocks
  private DoctorFacade doctorFacade;


  @Test
  void createDoctor_validDoctor_doctorSaved() {
    CreateDoctorRequestDto dtoIn = TestData.getCreateDoctorRequestDto();
    Doctor doctor = TestData.getDoctor();
    DoctorViewResponseDto expectedDto = TestData.getDoctorViewResponseDto();

    when(doctorMapper.createDtoToEntity(dtoIn)).thenReturn(doctor);
    when(doctorService.create(any())).thenReturn(doctor);
    when(doctorMapper.entityToReadDto(any())).thenReturn(expectedDto);

    DoctorViewResponseDto result = doctorFacade.create(dtoIn);

    assertThat(result).isEqualTo(expectedDto);
  }
  @Test
  void addPatient_patientValid_patientAdded() {
    when(patientService.get(any())).thenReturn(Optional.of(TestData.getPatient()));
    when(doctorService.get(any())).thenReturn(Optional.of(TestData.getDoctor()));
    when(doctorMapper.entityToReadDto(any())).thenReturn(TestData.getDoctorViewResponseDto());

    doctorFacade.addPatient(1L, 1L);

    Mockito.verify(doctorService).update(any());
  }

  @Test
  void addPatient_patientNotExist_exceptionThrown() {
   assertThrows(IllegalArgumentException.class, () -> doctorFacade.addPatient(1L, 1L));
  }


  @Test
  void addPatient_doctorNotExist_exceptionThrown() {
    when(patientService.get(any())).thenReturn(Optional.of(TestData.getPatient()));
    assertThrows(IllegalArgumentException.class, () -> doctorFacade.addPatient(1L, 1L));
  }

  @Test
  void removeDoctor_doctorValid_doctorRemoved() {
    doctorFacade.delete(1L);
    Mockito.verify(doctorService).delete(any());
  }

  @Test
  void updateDoctor_doctorValid_doctorUpdated() {
    UpdateDoctorRequestDto dtoIn = TestData.getUpdateDoctorRequestDto();
    Doctor doctor = TestData.getUpdatedDoctor();
    DoctorViewResponseDto expectedDto = TestData.getUpdatedDoctorViewResponseDto();

    when(doctorMapper.updateDtoToEntity(dtoIn)).thenReturn(doctor);
    when(doctorService.get(any())).thenReturn(Optional.of(doctor));
    when(doctorService.update(any())).thenReturn(doctor);
    when(doctorMapper.entityToReadDto(any())).thenReturn(expectedDto);

    DoctorViewResponseDto result = doctorFacade.update(1L, dtoIn);

    assertThat(result.getSpecialization()).isEqualTo(expectedDto.getSpecialization());
  }


  @Test
  void updateDoctor_doctorNotExist_exceptionThrown() {
    UpdateDoctorRequestDto dtoIn = TestData.getUpdateDoctorRequestDto();
    assertThrows(IllegalArgumentException.class, () -> doctorFacade.update(1L, dtoIn));
  }

  @Test
  void getDoctor_doctorValid_doctorReturned() {
    Doctor doctor = TestData.getDoctor();
    DoctorViewResponseDto expectedDto = TestData.getDoctorViewResponseDto();

    when(doctorService.get(any())).thenReturn(Optional.of(doctor));
    when(doctorMapper.entityToReadDto(doctor)).thenReturn(expectedDto);

    DoctorViewResponseDto result = doctorFacade.get(1L);

    assertThat(result).isEqualTo(expectedDto);
  }




}
