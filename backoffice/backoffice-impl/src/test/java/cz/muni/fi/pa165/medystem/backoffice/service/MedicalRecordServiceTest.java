package cz.muni.fi.pa165.medystem.backoffice.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import cz.muni.fi.pa165.medystem.backoffice.entity.MedicalRecord;
import cz.muni.fi.pa165.medystem.backoffice.repository.MedicalRecordRepository;
import cz.muni.fi.pa165.medystem.backoffice.util.TestData;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class MedicalRecordServiceTest {

  @Mock
  private MedicalRecordRepository medicalRecordRepository;

  @InjectMocks
  private MedicalRecordService medicalRecordService;

  @Test
  void createMedicalRecord_medicalRecordCreated_returnMedicalRecord() {

    MedicalRecord medicalRecord = TestData.getMedicalRecord();
    Mockito.when(medicalRecordRepository.save(medicalRecord)).thenReturn(medicalRecord);

    MedicalRecord createdMedicalRecord = medicalRecordService.create(medicalRecord);

    assertEquals(createdMedicalRecord, medicalRecord);
  }

  @Test
  void get_medicalRecordFound_returnMedicalRecord() {

    MedicalRecord medicalRecord = TestData.getMedicalRecord();
    Mockito.when(medicalRecordRepository.findById(1L)).thenReturn(Optional.of(medicalRecord));

    Optional<MedicalRecord> foundMedicalRecord = medicalRecordService.get(1L);

    assertEquals(foundMedicalRecord.get(), medicalRecord);
  }

  @Test
  void get_medicalRecordNotFound_throwException() {

    Mockito.when(medicalRecordRepository.findById(1L)).thenThrow(new IllegalArgumentException("MedicalRecord not found"));

    assertThrows(IllegalArgumentException.class, () -> medicalRecordService.get(1L));
  }

  @Test
  void update_medicalRecordUpdated() {

    MedicalRecord medicalRecord = TestData.getMedicalRecord();
    Mockito.when(medicalRecordRepository.save(medicalRecord)).thenReturn(medicalRecord);

    MedicalRecord updated = medicalRecordService.update(medicalRecord);
    assertEquals(updated, medicalRecord);

  }

  @Test
  void delete_medicalRecordDeleted() {
    medicalRecordService.delete(1L);
    Mockito.verify(medicalRecordRepository).deleteById(1L);
  }

  @Test
  void getByPatientId_medicalRecordFound_returnMedicalRecord() {
    List<MedicalRecord> medicalRecords = List.of(TestData.getMedicalRecord());
    Mockito.when(medicalRecordRepository.findAllByPatientId(1L)).thenReturn(List.of(medicalRecords.get(0)));

    List<MedicalRecord> foundMedicalRecord = medicalRecordService.getMedicalRecordsByPatientId(1L);

    assertEquals(foundMedicalRecord.size(), medicalRecords.size());
  }

}
