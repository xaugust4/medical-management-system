package cz.muni.fi.pa165.medystem.backoffice.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import cz.muni.fi.pa165.medystem.backoffice.entity.Doctor;
import cz.muni.fi.pa165.medystem.backoffice.repository.DoctorRepository;
import cz.muni.fi.pa165.medystem.backoffice.util.TestData;
import dto.doctor.CreateDoctorRequestDto;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
@AutoConfigureMockMvc
public class CreateDoctorIntegrationTest {

  @Autowired
  private MockMvc mockMvc;

  @Autowired
  private DoctorRepository doctorRepository;

  private static ObjectMapper objectMapper = new ObjectMapper();

  @BeforeAll
  public static void setupMapper() {
    objectMapper = new ObjectMapper();
    objectMapper.registerModule(new JavaTimeModule());
  }

  @Test
  @Transactional
  public void createDoctor_validDoctor_returnCreatedDoctor() throws Exception {
    CreateDoctorRequestDto doctorCreateRequestDto = TestData.getCreateDoctorRequestDto();
    String doctorCreateRequestDtoPayload = new ObjectMapper().writeValueAsString(doctorCreateRequestDto);

    var payload = mockMvc.perform(post("/api/backoffice/doctors/create")
            .contentType(MediaType.APPLICATION_JSON)
            .content(doctorCreateRequestDtoPayload))
        .andExpect(status().isCreated())
        .andExpect(jsonPath("$.userId").value(doctorCreateRequestDto.getUserId()))
        .andExpect(jsonPath("$.specialization").value(doctorCreateRequestDto.getSpecialization()))
        .andReturn()
        .getResponse()
        .getContentAsString();

    Doctor retrievedDoctorFromHttp = objectMapper.readValue(payload, Doctor.class);
    Doctor retrievedDoctor = doctorRepository.getReferenceById(retrievedDoctorFromHttp.getId());

    assertNotNull(retrievedDoctor);
    assertEquals(retrievedDoctorFromHttp.getId(), retrievedDoctor.getId());
  }

}
