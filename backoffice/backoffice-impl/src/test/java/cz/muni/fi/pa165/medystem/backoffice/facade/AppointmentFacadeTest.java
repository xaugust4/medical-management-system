package cz.muni.fi.pa165.medystem.backoffice.facade;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import cz.muni.fi.pa165.medystem.backoffice.entity.Appointment;
import cz.muni.fi.pa165.medystem.backoffice.entity.AppointmentStatus;
import cz.muni.fi.pa165.medystem.backoffice.mapper.AppointmentMapper;
import cz.muni.fi.pa165.medystem.backoffice.service.AppointmentService;
import cz.muni.fi.pa165.medystem.backoffice.service.DoctorService;
import cz.muni.fi.pa165.medystem.backoffice.service.PatientService;
import cz.muni.fi.pa165.medystem.backoffice.service.validation.AppointmentValidation;
import cz.muni.fi.pa165.medystem.backoffice.util.TestData;
import dto.appointment.AppointmentViewResponseDto;
import dto.appointment.CreateAppointmentRequestDto;
import dto.appointment.UpdateAppointmentRequestDto;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class AppointmentFacadeTest {

  @Mock
  private AppointmentService appointmentService;
  @Mock
  private DoctorService doctorService;
  @Mock
  private PatientService patientService;

  @Mock
  private AppointmentMapper appointmentMapper;

  @Mock
  private AppointmentValidation appointmentValidation;

  @InjectMocks
  private AppointmentFacade appointmentFacade;

  @Test
  void createAppointment_validAppointment_appointmentSaved() {
    CreateAppointmentRequestDto dtoIn = TestData.getCreateAppointmentRequestDto();
    Appointment appointment = TestData.getAppointment();
    AppointmentViewResponseDto expectedDto = TestData.getAppointmentViewResponseDto();

    when(appointmentMapper.createDtoToEntity(dtoIn)).thenReturn(appointment);
    when(appointmentService.create(any())).thenReturn(appointment);
    when(doctorService.get(any())).thenReturn(Optional.of(TestData.getDoctor()));
    when(patientService.get(any())).thenReturn(Optional.of(TestData.getPatient()));
    when(appointmentMapper.entityToReadDto(any())).thenReturn(expectedDto);
    doNothing().when(appointmentValidation).isAppointmentValid(any(), any());

    AppointmentViewResponseDto result = appointmentFacade.create(dtoIn);

    assertThat(result.getId()).isEqualTo(expectedDto.getId());
  }

  @Test
  void createAppointment_patientNotExists_appointmentNotSaved() {
    CreateAppointmentRequestDto dtoIn = TestData.getCreateAppointmentRequestDto();
    assertThrows(IllegalArgumentException.class, () -> appointmentFacade.create(dtoIn));
  }

  @Test
  void createAppointment_doctorNotExists_appointmentNotSaved() {
    CreateAppointmentRequestDto dtoIn = TestData.getCreateAppointmentRequestDto();
    Appointment appointment = TestData.getAppointment();
    AppointmentViewResponseDto expectedDto = TestData.getAppointmentViewResponseDto();
    when(patientService.get(any())).thenReturn(Optional.of(TestData.getPatient()));

    assertThrows(IllegalArgumentException.class, () -> appointmentFacade.create(dtoIn));
  }


  @Test
  void updateAppointment_validAppointment_appointmentUpdated() {

    Appointment appointment = TestData.getAppointment();
    Appointment updatedAppointment = TestData.getUpdatedAppointment();
    UpdateAppointmentRequestDto dtoIn = TestData.getUpdateAppointmentRequestDto();
    AppointmentViewResponseDto expectedDto = TestData.getAppointmentViewResponseDto();

    when(appointmentMapper.entityFromUpdateEntity(any(), any())).thenReturn(appointment);
    when(appointmentMapper.entityToReadDto(updatedAppointment)).thenReturn(expectedDto);
    when(appointmentMapper.updateDtoToEntity(dtoIn)).thenReturn(appointment);

    when(appointmentService.get(any())).thenReturn(Optional.of(appointment));
    when(appointmentService.update(appointment)).thenReturn(updatedAppointment);
    when(doctorService.get(any())).thenReturn(Optional.of(TestData.getDoctor()));

    doNothing().when(appointmentValidation).isAppointmentValid(any(), any());

    AppointmentViewResponseDto result = appointmentFacade.update(1L, dtoIn);

    assertThat(result).isEqualTo(expectedDto);
  }

  @Test
  void updateAppointment_appointmentNotExists_appointmentNotUpdated() {
    UpdateAppointmentRequestDto dtoIn = TestData.getUpdateAppointmentRequestDto();
    assertThrows(IllegalArgumentException.class, () -> appointmentFacade.update(1L, dtoIn));
  }

  @Test
  void changeAppointmentStatus_validAppointment_appointmentStatusChanged() {
    doNothing().when(appointmentService).changeAppointmentStatus(1L, AppointmentStatus.CONFIRMED);
    appointmentFacade.changeAppointmentStatus(1L, AppointmentStatus.CONFIRMED);
  }


  @Test
  void listAppointmentsByDoctorId_validDoctorId_appointmentsListed() {
    Appointment appointment = TestData.getAppointment();
    AppointmentViewResponseDto dto = TestData.getAppointmentViewResponseDto();
    List<Appointment> appointments = List.of(appointment);

    when(appointmentService.listAppointmentsOfWeek(1L, dto.getAppointmentFrom())).thenReturn(appointments);
    when(appointmentMapper.entityToReadDto(appointment)).thenReturn(dto);

    Collection<AppointmentViewResponseDto> results =
            appointmentFacade.listAppointmentsOfWeek(1L, dto.getAppointmentFrom());

    assertThat(results).containsExactly(dto);
  }

}
