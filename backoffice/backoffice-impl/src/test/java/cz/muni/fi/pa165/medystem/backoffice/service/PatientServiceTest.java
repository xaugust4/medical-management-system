package cz.muni.fi.pa165.medystem.backoffice.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import cz.muni.fi.pa165.medystem.backoffice.entity.Patient;
import cz.muni.fi.pa165.medystem.backoffice.repository.PatientRepository;
import cz.muni.fi.pa165.medystem.backoffice.util.TestData;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class PatientServiceTest {

  @Mock
  private PatientRepository patientRepository;

  @InjectMocks
  private PatientService patientService;

  @Test
  void createPatient_patientCreated_returnPatient() {

    Patient patient = TestData.getPatient();
    Mockito.when(patientRepository.save(patient)).thenReturn(patient);

    Patient createdPatient = patientService.create(patient);

    assertEquals(createdPatient, patient);
  }

  @Test
  void get_patientFound_returnPatient() {

    Patient patient = TestData.getPatient();
    Mockito.when(patientRepository.findById(1L)).thenReturn(Optional.of(patient));

    Optional<Patient> foundPatient = patientService.get(1L);

    assertEquals(foundPatient.get(), patient);
  }

  @Test
  void get_patientNotFound_throwException() {

    Mockito.when(patientRepository.findById(1L)).thenThrow(new IllegalArgumentException("Patient not found"));

    assertThrows(IllegalArgumentException.class, () -> patientService.get(1L));
  }

  @Test
  void update_patientUpdated() {

    Patient patient = TestData.getPatient();
    Mockito.when(patientRepository.save(patient)).thenReturn(patient);

    Patient updated = patientService.update(patient);
    assertEquals(updated, patient);

  }

  @Test
  void delete_patientDeleted() {
    patientService.delete(1L);
    Mockito.verify(patientRepository).deleteById(1L);
  }

  @Test
  void getPatientByPersonalId_patientFound_returnPatient() {
    Patient patient = TestData.getPatient();
    Mockito.when(patientRepository.findByBirthNumber("123456789")).thenReturn(Optional.of(patient));

    Patient foundPatient = patientService.getPatientByBirthNumber("123456789");

    assertEquals(foundPatient, patient);
  }

}
