package cz.muni.fi.pa165.medystem.backoffice.facade;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import cz.muni.fi.pa165.medystem.backoffice.entity.Patient;
import cz.muni.fi.pa165.medystem.backoffice.mapper.PatientMapper;
import cz.muni.fi.pa165.medystem.backoffice.service.PatientService;
import cz.muni.fi.pa165.medystem.backoffice.util.TestData;
import dto.patient.CreatePatientRequestDto;
import dto.patient.PatientViewResponseDto;
import dto.patient.UpdatePatientRequestDto;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class PatientFacadeTest {

  @Mock
  private PatientService patientService;

  @Mock
  private PatientMapper patientMapper;

  @InjectMocks
  private PatientFacade patientFacade;


  @Test
  void createPatient_validPatient_patientSaved() {
    CreatePatientRequestDto dtoIn = TestData.getCreatePatientRequestDto();
    Patient patient = TestData.getPatient();
    PatientViewResponseDto expectedDto = TestData.getPatientViewResponseDto();

    when(patientMapper.createDtoToEntity(dtoIn)).thenReturn(patient);
    when(patientService.create(any())).thenReturn(patient);
    when(patientMapper.entityToReadDto(any())).thenReturn(expectedDto);

    PatientViewResponseDto result = patientFacade.create(dtoIn);

    assertThat(result).isEqualTo(expectedDto);
  }

  @Test
  void updatePatient_invalidPatient_exceptionThrown() {
    UpdatePatientRequestDto dtoIn = TestData.getUpdatePatientRequestDto();
    assertThrows(IllegalArgumentException.class, () -> patientFacade.update(1L, dtoIn));
  }

  @Test
  void removePatient_patientValid_patientRemoved() {
    patientFacade.delete(1L);
    Mockito.verify(patientService).delete(any());
  }

  @Test
  void updatePatient_patientValid_patientUpdated() {
    UpdatePatientRequestDto dtoIn = TestData.getUpdatePatientRequestDto();
    Patient patient = TestData.getUpdatedPatient();
    PatientViewResponseDto expectedDto = TestData.getUpdatedPatientViewResponseDto();

    when(patientMapper.updateDtoToEntity(dtoIn)).thenReturn(patient);
    when(patientService.get(any())).thenReturn(Optional.of(patient));
    when(patientService.update(any())).thenReturn(patient);
    when(patientMapper.entityToReadDto(any())).thenReturn(expectedDto);

    PatientViewResponseDto result = patientFacade.update(1L, dtoIn);

    assertThat(result.getLastName()).isEqualTo(expectedDto.getLastName());
  }

  @Test
  void getPatient_patientValid_patientReturned() {
    Patient patient = TestData.getPatient();
    PatientViewResponseDto expectedDto = TestData.getPatientViewResponseDto();

    when(patientService.get(any())).thenReturn(Optional.of(patient));
    when(patientMapper.entityToReadDto(patient)).thenReturn(expectedDto);

    PatientViewResponseDto result = patientFacade.get(1L);

    assertThat(result).isEqualTo(expectedDto);
  }

  @Test
  void findPatientByBirthNumber_patientValid_patientReturned() {
    Patient patient = TestData.getPatient();
    PatientViewResponseDto expectedDto = TestData.getPatientViewResponseDto();

    when(patientService.getPatientByBirthNumber(any())).thenReturn(patient);
    when(patientMapper.entityToReadDto(patient)).thenReturn(expectedDto);

    PatientViewResponseDto result = patientFacade.findPatientByBirthNumber("12345678/1234");

    assertThat(result).isEqualTo(expectedDto);
  }

}
