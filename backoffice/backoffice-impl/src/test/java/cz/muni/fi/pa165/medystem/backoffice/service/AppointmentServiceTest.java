package cz.muni.fi.pa165.medystem.backoffice.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import cz.muni.fi.pa165.medystem.backoffice.entity.Appointment;
import cz.muni.fi.pa165.medystem.backoffice.entity.AppointmentStatus;
import cz.muni.fi.pa165.medystem.backoffice.repository.AppointmentRepository;
import cz.muni.fi.pa165.medystem.backoffice.util.TestData;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class AppointmentServiceTest {

  @Mock
  private AppointmentRepository appointmentRepository;

  @InjectMocks
  private AppointmentService appointmentService;

  @Test
  void createAppointment_appointmentCreated_returnAppointment() {

    Appointment appointment = TestData.getAppointment();
    Mockito.when(appointmentRepository.save(appointment)).thenReturn(appointment);

    Appointment createdAppointment = appointmentService.create(appointment);

    assertEquals(createdAppointment, appointment);
  }

  @Test
  void get_appointmentFound_returnAppointment() {

    Appointment appointment = TestData.getAppointment();
    Mockito.when(appointmentRepository.findById(1L)).thenReturn(Optional.of(appointment));

    Optional<Appointment> foundAppointment = appointmentService.get(1L);

    assertEquals(foundAppointment.get(), appointment);
  }

  @Test
  void get_appointmentNotFound_throwException() {

    Mockito.when(appointmentRepository.findById(1L)).thenThrow(new IllegalArgumentException("Appointment not found"));

    assertThrows(IllegalArgumentException.class, () -> appointmentService.get(1L));
  }

  @Test
  void update_appointmentUpdated() {

    Appointment appointment = TestData.getAppointment();
    Mockito.when(appointmentRepository.save(appointment)).thenReturn(appointment);

    Appointment updated = appointmentService.update(appointment);
    assertEquals(updated, appointment);

  }

  @Test
  void delete_appointmentDeleted() {
    appointmentService.delete(1L);
    Mockito.verify(appointmentRepository).deleteById(1L);
  }

  @Test
  void changeAppointmentStatus_appointmentStatusChanged() {

    Appointment appointment = TestData.getAppointment();
    Mockito.when(appointmentRepository.findById(1L)).thenReturn(Optional.of(appointment));

    appointmentService.changeAppointmentStatus(1L, AppointmentStatus.CONFIRMED);

    assertEquals(appointment.getStatus(), AppointmentStatus.CONFIRMED);
  }

  @Test
  void listAppointmentsOfWeek_appointmentsFound_returnAppointments() {

    Appointment appointment = TestData.getAppointment();
    Mockito.when(appointmentRepository.findAllByDoctorId(1L)).thenReturn(List.of(appointment));

    List<Appointment> appointments = appointmentService.listAppointmentsOfWeek(1L, LocalDateTime.now());

    assertEquals(appointments.size(), 1);
  }
}
