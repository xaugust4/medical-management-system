package cz.muni.fi.pa165.medystem.backoffice.facade;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import cz.muni.fi.pa165.medystem.backoffice.entity.MedicalRecord;
import cz.muni.fi.pa165.medystem.backoffice.mapper.MedicalRecordMapper;
import cz.muni.fi.pa165.medystem.backoffice.service.MedicalRecordService;
import cz.muni.fi.pa165.medystem.backoffice.service.PatientService;
import cz.muni.fi.pa165.medystem.backoffice.util.TestData;
import dto.medrecord.CreateMedicalRecordRequestDto;
import dto.medrecord.MedicalRecordViewResponseDto;
import dto.medrecord.UpdateMedicalRecordRequestDto;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class MedicalRecordFacadeTests {

  @Mock
  private PatientService patientService;
  @Mock
  private MedicalRecordService medicalRecordService;

  @Mock
  private MedicalRecordMapper medicalRecordMapper;

  @InjectMocks
  private MedicalRecordFacade medicalRecordFacade;

  @Test
  void createMedicalRecord_validMedicalRecord_medicalRecordSaved() {
    CreateMedicalRecordRequestDto dtoIn = TestData.getCreateMedicalRecordRequestDto();
    MedicalRecord medicalRecord = TestData.getMedicalRecord();
    MedicalRecordViewResponseDto expectedDto = TestData.getMedicalRecordViewResponseDto();

    when(medicalRecordMapper.createDtoToEntity(dtoIn)).thenReturn(medicalRecord);
    when(medicalRecordService.create(any())).thenReturn(medicalRecord);
    when(patientService.get(any())).thenReturn(Optional.of(TestData.getPatient()));
    when(medicalRecordMapper.entityToReadDto(any())).thenReturn(expectedDto);

    MedicalRecordViewResponseDto result = medicalRecordFacade.create(dtoIn);

    assertThat(result).isEqualTo(expectedDto);
  }

  @Test
  void createMedicalRecord_PatientNotFound_throwException() {
    CreateMedicalRecordRequestDto dtoIn = TestData.getCreateMedicalRecordRequestDto();
    when(patientService.get(any())).thenReturn(Optional.empty());
    assertThrows(IllegalArgumentException.class, () -> medicalRecordFacade.create(dtoIn));
  }

  @Test
  void removeMedicalRecord_medicalRecordValid_medicalRecordRemoved() {
    medicalRecordFacade.delete(1L);
    Mockito.verify(medicalRecordService).delete(any());
  }

  @Test
  void updateMedicalRecord_medicalRecordValid_medicalRecordUpdated() {
    UpdateMedicalRecordRequestDto dtoIn = TestData.getUpdateMedicalRecordRequestDto();
    MedicalRecord medicalRecord = TestData.getUpdatedMedicalRecord();
    MedicalRecordViewResponseDto expectedDto = TestData.getUpdatedMedicalRecordViewResponseDto();

    when(medicalRecordMapper.updateDtoToEntity(dtoIn)).thenReturn(medicalRecord);
    when(medicalRecordService.update(any())).thenReturn(medicalRecord);
    when(medicalRecordService.get(any())).thenReturn(Optional.of(medicalRecord));
    when(medicalRecordMapper.entityToReadDto(any())).thenReturn(expectedDto);

    MedicalRecordViewResponseDto result = medicalRecordFacade.update(1L, dtoIn);

    assertThat(result).isEqualTo(expectedDto);
  }

  @Test
  void getMedicalRecord_medicalRecordValid_medicalRecordReturned() {
    MedicalRecord medicalRecord = TestData.getMedicalRecord();
    when(medicalRecordService.get(1L)).thenReturn(Optional.of(medicalRecord));
    when(medicalRecordMapper.entityToReadDto(medicalRecord)).thenReturn(TestData.getMedicalRecordViewResponseDto());
    MedicalRecordViewResponseDto result = medicalRecordFacade.get(1L);
    assertThat(result.getPatientId()).isEqualTo(TestData.getMedicalRecordViewResponseDto().getPatientId());
  }

  @Test
  void getMedicalRecord_medicalRecordNotFound_throwException() {
    assertThrows(IllegalArgumentException.class, () -> medicalRecordFacade.get(1L));
  }


}
