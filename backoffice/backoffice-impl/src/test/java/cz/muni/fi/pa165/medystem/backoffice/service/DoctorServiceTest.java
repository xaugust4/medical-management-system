package cz.muni.fi.pa165.medystem.backoffice.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import cz.muni.fi.pa165.medystem.backoffice.entity.Doctor;
import cz.muni.fi.pa165.medystem.backoffice.repository.DoctorRepository;
import cz.muni.fi.pa165.medystem.backoffice.util.TestData;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class DoctorServiceTest {

  @Mock
  private DoctorRepository doctorRepository;

  @InjectMocks
  private DoctorService doctorService;

  @Test
  void createDoctor_doctorCreated_returnDoctor() {

    Doctor doctor = TestData.getDoctor();
    Mockito.when(doctorRepository.save(doctor)).thenReturn(doctor);

    Doctor createdDoctor = doctorService.create(doctor);

    assertEquals(createdDoctor, doctor);
  }

  @Test
  void get_doctorFound_returnDoctor() {

    Doctor doctor = TestData.getDoctor();
    Mockito.when(doctorRepository.findById(1L)).thenReturn(Optional.of(doctor));

    Optional<Doctor> foundDoctor = doctorService.get(1L);

    assertEquals(foundDoctor.get(), doctor);
  }

  @Test
  void get_doctorNotFound_throwException() {

    Mockito.when(doctorRepository.findById(1L)).thenThrow(new IllegalArgumentException("Doctor not found"));

    assertThrows(IllegalArgumentException.class, () -> doctorService.get(1L));
  }

  @Test
  void update_doctorUpdated() {

    Doctor doctor = TestData.getDoctor();
    Mockito.when(doctorRepository.save(doctor)).thenReturn(doctor);

    Doctor updated = doctorService.update(doctor);
    assertEquals(updated, doctor);

  }

  @Test
  void delete_doctorDeleted() {
    doctorService.delete(1L);
    Mockito.verify(doctorRepository).deleteById(1L);

  }
}

