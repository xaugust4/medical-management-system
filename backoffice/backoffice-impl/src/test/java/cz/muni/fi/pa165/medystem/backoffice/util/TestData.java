package cz.muni.fi.pa165.medystem.backoffice.util;

import cz.muni.fi.pa165.medystem.backoffice.entity.Appointment;
import cz.muni.fi.pa165.medystem.backoffice.entity.AppointmentStatus;
import cz.muni.fi.pa165.medystem.backoffice.entity.Doctor;
import cz.muni.fi.pa165.medystem.backoffice.entity.MedicalRecord;
import cz.muni.fi.pa165.medystem.backoffice.entity.MedicalRecordType;
import cz.muni.fi.pa165.medystem.backoffice.entity.Patient;
import dto.appointment.AppointmentViewResponseDto;
import dto.appointment.CreateAppointmentRequestDto;
import dto.appointment.UpdateAppointmentRequestDto;
import dto.doctor.CreateDoctorRequestDto;
import dto.doctor.DoctorViewResponseDto;
import dto.doctor.UpdateDoctorRequestDto;
import dto.medrecord.CreateMedicalRecordRequestDto;
import dto.medrecord.MedicalRecordViewResponseDto;
import dto.medrecord.UpdateMedicalRecordRequestDto;
import dto.patient.CreatePatientRequestDto;
import dto.patient.PatientViewResponseDto;
import dto.patient.UpdatePatientRequestDto;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;

public final class TestData {

  public static AppointmentViewResponseDto getAppointmentViewResponseDto() {
    return new AppointmentViewResponseDto(
        1L,
        1L,
        1L,
        LocalDateTime.now(),
        LocalDateTime.now(),
        "CONFIRMED"
    );
  }

  public static CreateAppointmentRequestDto getCreateAppointmentRequestDto() {
    return new CreateAppointmentRequestDto(
        1L,
        1L,
        LocalDateTime.now(),
        LocalDateTime.now()
    );
  }

  public static UpdateAppointmentRequestDto getUpdateAppointmentRequestDto() {
    return new UpdateAppointmentRequestDto(
        1L,
        LocalDateTime.now(),
        LocalDateTime.now(),
        "CONFIRMED"
    );
  }

  public static Appointment getAppointment() {
    var appointment = new Appointment();
    appointment.setId(1L);
    appointment.setDoctor(getDoctor());
    appointment.setPatient(getPatient());
    appointment.setStatus(AppointmentStatus.PLANNED);
    appointment.setAppointmentFrom(LocalDateTime.now());
    return appointment;
  }

  public static Appointment getUpdatedAppointment() {
    var appointment = new Appointment();
    appointment.setId(1L);
    appointment.setDoctor(getDoctor());
    appointment.setPatient(getPatient());
    appointment.setStatus(AppointmentStatus.CONFIRMED);
    appointment.setAppointmentFrom(LocalDateTime.now());
    return appointment;
  }

  public static Patient getPatient() {
    var patient = new Patient();
    patient.setFirstName("John");
    patient.setLastName("Doe");
    patient.setId(1L);
    return patient;
  }

  public static Doctor getDoctor() {
    var doctor = new Doctor();
    doctor.setUserId(1L);
    doctor.setId(1L);
    return doctor;
  }

  public static Doctor getUpdatedDoctor() {
    var doctor = new Doctor();
    doctor.setUserId(1L);
    doctor.setId(1L);
    return doctor;
  }

  public static DoctorViewResponseDto getDoctorViewResponseDto() {
    return new DoctorViewResponseDto(
        1L,
        "4",
        "doctor");
  }

  public static DoctorViewResponseDto getUpdatedDoctorViewResponseDto() {
    return new DoctorViewResponseDto(
        1L,
        "4",
        "surgeon");
  }

  public static CreateDoctorRequestDto getCreateDoctorRequestDto() {
    return new CreateDoctorRequestDto(
        "4",
        "doctor",
        new HashSet<>()
        );
  }
  public static UpdateDoctorRequestDto getUpdateDoctorRequestDto() {
    return new UpdateDoctorRequestDto(
        "surgeon",
        new HashSet<>()
        );
  }

  //test data for patient
  public static PatientViewResponseDto getPatientViewResponseDto() {
    return new PatientViewResponseDto(
        1L,
        "John",
        "Doe",
        "1234567890",
        "111",
        "12345678/1234",
        LocalDate.of(1990,9, 12)
    );
  }

  public static Patient getUpdatedPatient() {
    var patient = new Patient();
    patient.setFirstName("John");
    patient.setLastName("Undoe");
    patient.setTelephoneNumber("456789123");
    patient.setInsuranceCompany("111");
    patient.setBirthNumber("12345678/1234");
    patient.setBirthDate(LocalDate.of(1990,9, 12));
    return patient;
  }

  public static PatientViewResponseDto getUpdatedPatientViewResponseDto() {
    return new PatientViewResponseDto(
        1L,
        "John",
        "Undoe",
        "456789123",
        "111",
        "12345678/1234",
        LocalDate.of(1990,9, 12)
    );
  }

  public static CreatePatientRequestDto getCreatePatientRequestDto() {
    return new CreatePatientRequestDto(
        "John",
        "Doe",
        "1234567890",
        "111",
        "12345678/1234",
        LocalDate.of(1990,9, 12)
    );
  }

  public static UpdatePatientRequestDto getUpdatePatientRequestDto() {
    return new UpdatePatientRequestDto(
        "John",
        "Undoe",
        "456789123",
        "111"
    );
  }

  public static MedicalRecord getMedicalRecord() {
    var medicalRecord = new MedicalRecord();
    medicalRecord.setPatient(getPatient());
    medicalRecord.setRecordDetails("Details");
    medicalRecord.setMedicalRecordType(MedicalRecordType.TREATMENT);
    return medicalRecord;
  }

  public static MedicalRecord getUpdatedMedicalRecord() {
    var medicalRecord = new MedicalRecord();
    medicalRecord.setPatient(getPatient());
    medicalRecord.setRecordDetails("Details");
    medicalRecord.setMedicalRecordType(MedicalRecordType.UNCATEGORIZED);
    return medicalRecord;
  }


  public static MedicalRecordViewResponseDto getMedicalRecordViewResponseDto() {
    return new MedicalRecordViewResponseDto(
        1L,
       dto.medrecord.MedicalRecordType.TREATMENT,
        "Details"
    );
  }

  public static CreateMedicalRecordRequestDto getCreateMedicalRecordRequestDto() {
    return new CreateMedicalRecordRequestDto(
        1L,
        dto.medrecord.MedicalRecordType.TREATMENT,
        "Details"
    );
  }

  public static UpdateMedicalRecordRequestDto getUpdateMedicalRecordRequestDto() {
    return new UpdateMedicalRecordRequestDto(
        dto.medrecord.MedicalRecordType.UNCATEGORIZED,
        "Details"
    );
  }

  public static MedicalRecordViewResponseDto getUpdatedMedicalRecordViewResponseDto() {
    return new MedicalRecordViewResponseDto(
        1L,
        dto.medrecord.MedicalRecordType.UNCATEGORIZED,
        "Details"
    );
  }



}
