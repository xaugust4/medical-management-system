package cz.muni.fi.pa165.medystem.backoffice.facade;

import cz.muni.fi.pa165.medystem.backoffice.entity.Doctor;
import cz.muni.fi.pa165.medystem.backoffice.entity.Patient;
import cz.muni.fi.pa165.medystem.backoffice.mapper.DoctorMapper;
import cz.muni.fi.pa165.medystem.backoffice.repository.DoctorRepository;
import cz.muni.fi.pa165.medystem.backoffice.service.DoctorService;
import cz.muni.fi.pa165.medystem.backoffice.service.PatientService;
import dto.doctor.CreateDoctorRequestDto;
import dto.doctor.DoctorViewResponseDto;
import dto.doctor.UpdateDoctorRequestDto;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Getter
@RequiredArgsConstructor
@Service
public class DoctorFacade extends CrudFacade<
        Doctor,
        CreateDoctorRequestDto,
        DoctorViewResponseDto,
        UpdateDoctorRequestDto,
        DoctorRepository,
        DoctorService,
        DoctorMapper> {

    private final DoctorService service;
    private final PatientService patientService;
    private final DoctorMapper mapper;

    public DoctorViewResponseDto addPatient(Long doctorId, Long patientId) {
        Patient toAdd = patientService.get(patientId).orElseThrow(
                () -> new IllegalArgumentException("Patient with id " + patientId + " not found")
        );
        Doctor doctor = service.get(doctorId).orElseThrow(
                () -> new IllegalArgumentException("Doctor with id " + doctorId + " not found")
        );

        doctor.getPatients().add(toAdd);
        return mapper.entityToReadDto(service.update(doctor));
    }
}
