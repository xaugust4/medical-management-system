package cz.muni.fi.pa165.medystem.backoffice.mapper;

import cz.muni.fi.pa165.medystem.backoffice.entity.Patient;
import dto.patient.CreatePatientRequestDto;
import dto.patient.PatientViewResponseDto;
import dto.patient.UpdatePatientRequestDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface PatientMapper extends AbstractMapper<
        Patient,
        CreatePatientRequestDto,
        PatientViewResponseDto,
        UpdatePatientRequestDto> {

    @Override
    CreatePatientRequestDto entityToCreateDto(Patient entity);

    @Override
    PatientViewResponseDto entityToReadDto(Patient entity);

    @Mapping(target = "medicalRecords", ignore = true)
    @Mapping(target = "appointments", ignore = true)
    @Override
    Patient createDtoToEntity(CreatePatientRequestDto create);

    @Override
    Patient readDtoToEntity(PatientViewResponseDto read);

    @Override
    Patient updateDtoToEntity(UpdatePatientRequestDto update);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "birthNumber", ignore = true)
    @Mapping(target = "birthDate", ignore = true)
    @Mapping(target = "medicalRecords", ignore = true)
    @Mapping(target = "appointments", ignore = true)
    @Override
    Patient entityFromUpdateEntity(@MappingTarget Patient entity, Patient update);
}
