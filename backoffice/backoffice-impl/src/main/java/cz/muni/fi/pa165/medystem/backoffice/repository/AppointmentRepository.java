package cz.muni.fi.pa165.medystem.backoffice.repository;

import cz.muni.fi.pa165.medystem.backoffice.entity.Appointment;
import cz.muni.fi.pa165.medystem.backoffice.entity.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AppointmentRepository extends JpaRepository<Appointment, Long> {
    List<Appointment> findAllByDoctorId(Long doctor_id);
}
