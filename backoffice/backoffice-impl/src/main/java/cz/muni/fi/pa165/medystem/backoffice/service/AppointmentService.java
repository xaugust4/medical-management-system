package cz.muni.fi.pa165.medystem.backoffice.service;

import cz.muni.fi.pa165.medystem.backoffice.entity.Appointment;
import cz.muni.fi.pa165.medystem.backoffice.entity.AppointmentStatus;
import cz.muni.fi.pa165.medystem.backoffice.repository.AppointmentRepository;
import jakarta.persistence.EntityNotFoundException;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.temporal.WeekFields;
import java.util.List;
import java.util.Locale;

@Getter
@Setter
@RequiredArgsConstructor
@Service
public class AppointmentService extends CrudService<Appointment, AppointmentRepository> {

    private final AppointmentRepository repository;

    @Transactional
    @Override
    public Appointment create(Appointment appointment) {
        List<Appointment> overlappingAppointments = findOverlappingAppointments(
                appointment.getDoctor().getId(),
                appointment.getAppointmentFrom(),
                appointment.getAppointmentTo()
        );

        if (!overlappingAppointments.isEmpty()) {
            //TODO implement custom error handling
            throw new IllegalArgumentException("Appointment overlaps with another appointment");
        }

        return super.create(appointment);
    }

    @Transactional
    @Override
    public Appointment update(Appointment appointmentDetails) {
        List<Appointment> overlappingAppointments = findOverlappingAppointments(
                appointmentDetails.getDoctor().getId(),
                appointmentDetails.getAppointmentFrom(),
                appointmentDetails.getAppointmentTo()
        );

        if (!overlappingAppointments.isEmpty()) {
            //TODO implement custom error handling
            throw new IllegalArgumentException("Appointment overlaps with another appointment");
        }

        return super.update(appointmentDetails);
    }

    @Transactional
    public void changeAppointmentStatus(Long id, AppointmentStatus newStatus) {
        Appointment appointment = getRepository().findById(id).orElseThrow(() ->
                new EntityNotFoundException("Appointment not found for this id :: " + id));
        appointment.setStatus(newStatus);
        getRepository().save(appointment);
    }

    @Transactional(readOnly = true)
    public List<Appointment> findAllByDoctorId(Long doctorId) {
        return getRepository().findAllByDoctorId(doctorId);
    }

    @Transactional(readOnly = true)
    public List<Appointment> listAppointmentsOfWeek(Long doctorId, LocalDateTime dateOfTheWeek) {
        List<Appointment> allByDoctors = getRepository().findAllByDoctorId(doctorId);

        return getSameWeekElements(dateOfTheWeek, allByDoctors).stream()
                .filter(appointment -> appointment.getStatus() == AppointmentStatus.PLANNED || appointment.getStatus() == AppointmentStatus.CONFIRMED)
                .toList();
    }

    private List<Appointment> findOverlappingAppointments(Long doctorId, LocalDateTime from, LocalDateTime to) {
        List<Appointment> allByDoctors = getRepository().findAllByDoctorId(doctorId);
        return allByDoctors.stream()
                .filter(appointment -> appointment.getStatus() == AppointmentStatus.FINISHED || appointment.getStatus() == AppointmentStatus.PLANNED)
                .filter(appointment -> appointment.getAppointmentFrom().isBefore(to) && appointment.getAppointmentTo().isAfter(from))
                .toList();
    }

    private List<Appointment> getSameWeekElements(LocalDateTime inputDateTime, List<Appointment> dateTimeCollection) {

        WeekFields weekFields = WeekFields.of(Locale.getDefault());
        int weekNumber = inputDateTime.get(weekFields.weekOfWeekBasedYear());

        return dateTimeCollection.stream()
                .filter(appointment -> appointment.getAppointmentFrom().get(weekFields.weekOfWeekBasedYear()) == weekNumber)
                .toList();

    }

}
