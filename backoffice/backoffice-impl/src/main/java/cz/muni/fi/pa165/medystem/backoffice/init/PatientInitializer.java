package cz.muni.fi.pa165.medystem.backoffice.init;

import cz.muni.fi.pa165.medystem.backoffice.entity.Patient;
import cz.muni.fi.pa165.medystem.backoffice.init.framework.InitOrderConstants;
import cz.muni.fi.pa165.medystem.backoffice.init.framework.data.DataPicker;
import cz.muni.fi.pa165.medystem.backoffice.init.framework.entity.AbstractInitializer;
import cz.muni.fi.pa165.medystem.backoffice.repository.PatientRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Component
@Order(InitOrderConstants.PATIENT_ORDER)
@ConditionalOnExpression("${init.patient.enabled:false} || ${init.enabled-all:false}")
public class PatientInitializer extends AbstractInitializer<Long, Patient, PatientRepository> {

    public static final int ORDER = InitOrderConstants.PATIENT_ORDER;

    @Value("${init.patient.size:100}")
    private long seedSize;

    @Value("${init.patient.userIdStart:1000}")
    private long userIdStart;

    @Value("${init.patient.patientIdStart:1000}")
    private long patientIdStart;

    @Autowired
    protected PatientInitializer(PatientRepository repository) {
        super(repository, ORDER);
    }

    @Override
    protected List<Patient> initializeEntities() {
        List<Patient> entities = new ArrayList<>();

        for (int i = 1; i <= seedSize; i++) {
            entities.add(initializeEntity());
        }
        return entities;
    }

    private Patient initializeEntity() {
        var patient = new Patient();
        patient.setId(patientIdStart++);
        patient.setInsuranceCompany(DataPicker.RandomGenerator.randomInsuranceCompany());
        patient.setFirstName(       DataPicker.RandomGenerator.randomFirstName());
        patient.setLastName(        DataPicker.RandomGenerator.randomLastName());
        patient.setBirthDate(       DataPicker.RandomGenerator.randomDateBetween(1930, 2020));
        patient.setBirthNumber(     DataPicker.RandomGenerator.randomBirthNumber());
        patient.setTelephoneNumber( DataPicker.RandomGenerator.randomTelephoneNumber());
        patient.setUserId(userIdStart++);

        return patient;
    }
}
