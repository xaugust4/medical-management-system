package cz.muni.fi.pa165.medystem.backoffice.facade;

import cz.muni.fi.pa165.medystem.backoffice.entity.Appointment;
import cz.muni.fi.pa165.medystem.backoffice.entity.AppointmentStatus;
import cz.muni.fi.pa165.medystem.backoffice.entity.Doctor;
import cz.muni.fi.pa165.medystem.backoffice.entity.Patient;
import cz.muni.fi.pa165.medystem.backoffice.mapper.AppointmentMapper;
import cz.muni.fi.pa165.medystem.backoffice.repository.AppointmentRepository;
import cz.muni.fi.pa165.medystem.backoffice.service.AppointmentService;
import cz.muni.fi.pa165.medystem.backoffice.service.DoctorService;
import cz.muni.fi.pa165.medystem.backoffice.service.PatientService;
import cz.muni.fi.pa165.medystem.backoffice.service.validation.AppointmentValidation;
import dto.appointment.AppointmentViewResponseDto;
import dto.appointment.CreateAppointmentRequestDto;
import dto.appointment.UpdateAppointmentRequestDto;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Getter
@RequiredArgsConstructor
@Service
public class AppointmentFacade extends CrudFacade<
        Appointment,
        CreateAppointmentRequestDto,
        AppointmentViewResponseDto,
        UpdateAppointmentRequestDto,
        AppointmentRepository,
        AppointmentService,
        AppointmentMapper> {

    private final AppointmentService service;
    private final DoctorService doctorService;
    private final PatientService patientService;
    private final AppointmentMapper mapper;
    private final AppointmentValidation validation;

    public AppointmentViewResponseDto create(CreateAppointmentRequestDto dto) {
        Optional<Doctor> appointmentDoctor = doctorService.get(dto.getDoctorId());
        Optional<Patient> appointmentPatient = patientService.get(dto.getPatientId());

        Patient patient = appointmentPatient.orElseThrow(
                () -> new IllegalArgumentException("Patient with id " + dto.getPatientId() + " does not exist")
        );
        Doctor doctor = appointmentDoctor.orElseThrow(
                () -> new IllegalArgumentException("Doctor with id " + dto.getDoctorId() + " does not exist")
        );

        Appointment toCreate = mapper.createDtoToEntity(dto);
        toCreate.setDoctor(doctor);
        toCreate.setPatient(patient);

        validation.isAppointmentValid(toCreate, appointmentDoctor.get());

        return mapper.entityToReadDto(service.create(toCreate));
    }

    public AppointmentViewResponseDto update(Long id, UpdateAppointmentRequestDto dto) {
        Doctor appointmentDoctor = doctorService.get(dto.getDoctorId()).orElseThrow(
                () -> new IllegalArgumentException("Doctor with id " + dto.getDoctorId() + " does not exist")
        );

        Appointment appointment = service.get(id).orElseThrow(
                () -> new IllegalArgumentException("Appointment with id " + id + " does not exist")
        );


        Appointment toUpdate = mapper.entityFromUpdateEntity(
                appointment,
                mapper.updateDtoToEntity(dto)
        );
        toUpdate.setDoctor(appointmentDoctor);

        validation.isAppointmentValid(toUpdate, appointmentDoctor);

        return mapper.entityToReadDto(service.update(toUpdate));
    }

    public void changeAppointmentStatus(Long id, AppointmentStatus status) {
        service.changeAppointmentStatus(id, status);
    }

    public List<AppointmentViewResponseDto> listAppointmentsOfWeek(Long doctorId, LocalDateTime dayOfWeek) {
        return service.listAppointmentsOfWeek(doctorId, dayOfWeek).stream()
                .map(mapper::entityToReadDto)
                .toList();
    }
}
