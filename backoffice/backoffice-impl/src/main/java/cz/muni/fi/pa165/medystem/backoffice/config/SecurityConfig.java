package cz.muni.fi.pa165.medystem.backoffice.config;

import cz.muni.fi.pa165.medystem.backoffice.connector.UserConnector;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authorization.AuthorizationDecision;
import org.springframework.security.authorization.AuthorizationManager;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.access.intercept.RequestAuthorizationContext;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.csrf.CsrfTokenRequestAttributeHandler;

@Configuration
@EnableWebSecurity
public class SecurityConfig {

  @Autowired
  private UserConnector userConnector;

  @Bean
  public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception {
    httpSecurity
        .csrf(AbstractHttpConfigurer::disable)
        .authorizeHttpRequests(authorizeRequests ->
            authorizeRequests
                .requestMatchers("/login/oauth2/code/muni").authenticated()
                .requestMatchers("/api/backoffice/appointments/**").access(authorizationManager(Set.of("DOCTOR", "NURSE")))
                .requestMatchers("/api/backoffice/doctors/add-patient/{doctorId}/{patientId}").access(authorizationManager(Set.of("DOCTOR", "NURSE")))
                .requestMatchers("/api/backoffice/doctors/**").access(authorizationManager(Set.of("ADMIN")))
                .requestMatchers("/api/backoffice/medical-records/**").access(authorizationManager(Set.of("DOCTOR", "NURSE")))
                .requestMatchers("/api/backoffice/patient/**").access(authorizationManager(Set.of("DOCTOR", "NURSE")))
                .requestMatchers("/management/**").permitAll()
                .anyRequest().authenticated()
        )
        .logout(logout ->
            logout
                .logoutUrl("/logout")
                .logoutSuccessUrl("/login")
        )
        .oauth2Login(Customizer.withDefaults())
        .anonymous(AbstractHttpConfigurer::disable);
    return httpSecurity.build();
  }

  public AuthorizationManager<RequestAuthorizationContext> authorizationManager(Set<String> roles) {
    return (authentication, context) -> new AuthorizationDecision(hasAnyRole((OAuth2AuthenticationToken) (authentication.get()), roles));
  }

  public boolean hasAnyRole(OAuth2AuthenticationToken token, Set<String> roles) {
    var userRoles = userConnector.getUserByUsername(token.getName()).getRoles();
    return userRoles.stream().anyMatch(roles::contains);
  }
}
