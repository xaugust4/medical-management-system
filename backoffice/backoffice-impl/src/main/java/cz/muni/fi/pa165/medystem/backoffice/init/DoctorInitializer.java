package cz.muni.fi.pa165.medystem.backoffice.init;

import cz.muni.fi.pa165.medystem.backoffice.entity.Doctor;
import cz.muni.fi.pa165.medystem.backoffice.entity.DoctorDailyAvailable;
import cz.muni.fi.pa165.medystem.backoffice.init.framework.data.DataPicker;
import cz.muni.fi.pa165.medystem.backoffice.init.framework.InitOrderConstants;
import cz.muni.fi.pa165.medystem.backoffice.init.framework.entity.AbstractInitializer;
import cz.muni.fi.pa165.medystem.backoffice.repository.DoctorRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@Component
@Order(InitOrderConstants.DOCTOR_ORDER)
@ConditionalOnExpression("${init.doctor.enabled:false} || ${init.enabled-all:false}")
public class DoctorInitializer extends AbstractInitializer<Long, Doctor, DoctorRepository> {

    public static final int ORDER = InitOrderConstants.DOCTOR_ORDER;

    @Value("${init.doctor.size:100}")
    private long seedSize;

    @Value("${init.doctor.userIdStart:1}")
    private long docUserIdStart;

    @Value("${init.doctor.doctorIdStart:1000}")
    private long doctorIdStart;


    @Autowired
    public DoctorInitializer(DoctorRepository repository) {
        super(repository, ORDER);
    }

    @Override
    protected List<Doctor> initializeEntities() {
        List<Doctor> entities = new ArrayList<>();

        for (int i = 1; i <= seedSize; i++) {
            entities.add(initializeEntity());
        }
        return entities;
    }

    private Doctor initializeEntity() {
        var doc = new Doctor();
        doc.setId(doctorIdStart++);
        doc.setSpecialization(DataPicker.RandomGenerator.randomSpecialization());

        Set<DoctorDailyAvailable> dailyAvailableHours = new HashSet<>();
        for (int i = 1; i <= 5; i++) {
            dailyAvailableHours.add(DataPicker.RandomGenerator.randomDailyAvailableHours());
        }
        doc.setDailyAvailableHours(dailyAvailableHours);
        doc.setUserId(docUserIdStart++);

        return doc;
    }
}
