package cz.muni.fi.pa165.medystem.backoffice.service;

import cz.muni.fi.pa165.medystem.backoffice.entity.MedicalRecord;
import cz.muni.fi.pa165.medystem.backoffice.repository.MedicalRecordRepository;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Getter
@RequiredArgsConstructor
@Service
public class MedicalRecordService extends CrudService<
        MedicalRecord,
        MedicalRecordRepository> {

    private final MedicalRecordRepository repository;

    public List<MedicalRecord> getMedicalRecordsByPatientId(Long patientId) {
        return repository.findAllByPatientId(patientId);
    }
}
