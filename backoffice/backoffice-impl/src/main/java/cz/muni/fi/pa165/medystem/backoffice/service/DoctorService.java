package cz.muni.fi.pa165.medystem.backoffice.service;

import cz.muni.fi.pa165.medystem.backoffice.entity.Doctor;
import cz.muni.fi.pa165.medystem.backoffice.repository.DoctorRepository;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Getter
@RequiredArgsConstructor
@Service
public class DoctorService extends CrudService<Doctor, DoctorRepository>{

    private final DoctorRepository repository;

}
