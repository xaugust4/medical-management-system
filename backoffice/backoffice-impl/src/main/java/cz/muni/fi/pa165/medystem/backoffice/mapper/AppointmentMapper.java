package cz.muni.fi.pa165.medystem.backoffice.mapper;

import cz.muni.fi.pa165.medystem.backoffice.entity.Appointment;
import dto.appointment.AppointmentViewResponseDto;
import dto.appointment.CreateAppointmentRequestDto;
import dto.appointment.UpdateAppointmentRequestDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface AppointmentMapper extends AbstractMapper<
        Appointment,
        CreateAppointmentRequestDto,
        AppointmentViewResponseDto,
        UpdateAppointmentRequestDto>{

    @Mapping(target = "patientId", source = "patient.id")
    @Mapping(target = "doctorId", source = "doctor.id")
    @Override
    CreateAppointmentRequestDto entityToCreateDto(Appointment entity);

    @Mapping(target = "patientId", source = "patient.id")
    @Mapping(target = "doctorId", source = "doctor.id")
    @Override
    AppointmentViewResponseDto entityToReadDto(Appointment entity);

    @Override
    Appointment createDtoToEntity(CreateAppointmentRequestDto read);

    @Override
    Appointment readDtoToEntity(AppointmentViewResponseDto read);

    @Override
    Appointment updateDtoToEntity(UpdateAppointmentRequestDto read);

//    Appointment updateEntityFromEntity(Appointment from, Appointment to);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "patient", ignore = true)
    @Override
    Appointment entityFromUpdateEntity(@MappingTarget Appointment entity, Appointment read);

}
