package cz.muni.fi.pa165.medystem.backoffice.facade;

import cz.muni.fi.pa165.medystem.backoffice.mapper.AbstractMapper;
import cz.muni.fi.pa165.medystem.backoffice.service.CrudService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@RequiredArgsConstructor
@Service
public abstract class CrudFacade<
        ENTITY,
        CREATE,
        READ,
        UPDATE,
        REPOSITORY extends JpaRepository<ENTITY, Long>,
        SERVICE extends CrudService<ENTITY, REPOSITORY>,
        MAPPER extends AbstractMapper<ENTITY, CREATE, READ, UPDATE>> {

    public READ create(CREATE createDto) {
        ENTITY toCreate = getMapper().createDtoToEntity(createDto);
        ENTITY created = getService().create(toCreate);
        return getMapper().entityToReadDto(created);
    }

    public READ get(Long id) {
        Optional<ENTITY> resultOptional = getService().get(id);
        ENTITY entity = resultOptional.orElseThrow(
                () -> new IllegalArgumentException("Entity with id " + id + " not found")
        );
        return getMapper().entityToReadDto(entity);
    }

    public Page<READ> getAll(Pageable pageable) {
        return getService().getAll(pageable)
                .map(getMapper()::entityToReadDto);
    }

    public READ update(Long id, UPDATE updateDto) {
        ENTITY toUpdate = getService().get(id).orElseThrow(
                () -> new IllegalArgumentException("Entity with id " + id + " not found")
        );
        getMapper().updateDtoToEntity(updateDto);

        toUpdate = getMapper().entityFromUpdateEntity(toUpdate, getMapper().updateDtoToEntity(updateDto));

        ENTITY result = getService().update(toUpdate);
        return getMapper().entityToReadDto(result);
    }

    public void delete(Long id) {
        getService().delete(id);
    }

    public abstract SERVICE getService();

    public abstract MAPPER getMapper();
}
