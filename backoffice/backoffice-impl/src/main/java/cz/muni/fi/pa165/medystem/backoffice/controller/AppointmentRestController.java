package cz.muni.fi.pa165.medystem.backoffice.controller;

import cz.muni.fi.pa165.medystem.backoffice.entity.Appointment;
import cz.muni.fi.pa165.medystem.backoffice.facade.AppointmentFacade;
import cz.muni.fi.pa165.medystem.backoffice.mapper.AppointmentMapper;
import cz.muni.fi.pa165.medystem.backoffice.repository.AppointmentRepository;
import cz.muni.fi.pa165.medystem.backoffice.service.AppointmentService;
import dto.appointment.AppointmentViewResponseDto;
import dto.appointment.CreateAppointmentRequestDto;
import dto.appointment.UpdateAppointmentRequestDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Max;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/backoffice/appointments")
@Tag(name = "Appointment", description = "Operations related to Appointments")
public class AppointmentRestController implements CrudController<
        Appointment,
        CreateAppointmentRequestDto,
        AppointmentViewResponseDto,
        UpdateAppointmentRequestDto,
        AppointmentRepository,
        AppointmentService,
        AppointmentMapper,
        AppointmentFacade> {

    private final AppointmentFacade facade;

    @GetMapping("/list/doctorId={doctorId}&dayOfWeek={dayOfWeek}")
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Get all Appointments of doctor of specific week of day", description = "Retrieves details of all Appointments")
    @ApiResponse(responseCode = "200", description = "Appointments found",
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = AppointmentViewResponseDto.class)))
    public List<AppointmentViewResponseDto> listAppointmentsOfWeek(
            @PathVariable Long doctorId,
            @PathVariable LocalDateTime dayOfWeek) {
        return getFacade().listAppointmentsOfWeek(doctorId, dayOfWeek);
    }

    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(summary = "Create a new Appointment ", description = "Creates a new Appointment with the given details")
    @ApiResponse(responseCode = "201", description = "Appointment created",
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = AppointmentViewResponseDto.class)))
    @Override
    public AppointmentViewResponseDto create(
            @RequestBody @Valid CreateAppointmentRequestDto createDto) {
        return getFacade().create(createDto);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Get a Appointment by ID", description = "Retrieves details of a specific Appointment by ID")
    @ApiResponse(responseCode = "200", description = "Appointment found",
            content = @Content(mediaType = "application/json",
                    schema = @Schema(implementation = AppointmentViewResponseDto.class)))
    @Override
    public AppointmentViewResponseDto get(@PathVariable Long id) {
        return getFacade().get(id);
    }

    @GetMapping(value = "/all", params = {"page", "size"})
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Get all Appointments", description = "Retrieves details of all Appointments")
    @ApiResponse(responseCode = "200", description = "Appointments found",
            content = @Content(mediaType = "application/json",
                    schema = @Schema(implementation = Page.class)))
    @Override
    public Page<AppointmentViewResponseDto> getAll(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "10") @Max(value = 50) int size,
            @RequestParam(value = "sort", defaultValue = "id, asc", required = false) String[] sortingParams
    ) {
        String field = sortingParams[0];
        String sortingDirection = sortingParams[1];
        Sort.Direction direction = sortingDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC;
        Pageable pageObj = PageRequest.of(page, size, Sort.by(direction, field));

        return getFacade().getAll(pageObj);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Update an Appointment", description = "Updates the details of an existing Appointment specified by ID")
    @ApiResponse(responseCode = "200", description = "Appointment  updated",
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = AppointmentViewResponseDto.class)))
    @Override
    public AppointmentViewResponseDto update(
            @PathVariable Long id,
            @RequestBody @Valid UpdateAppointmentRequestDto updateDto) {
        return getFacade().update(id, updateDto);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Operation(summary = "Delete an Appointment", description = "Deletes an Appointment specified by ID")
    @ApiResponse(responseCode = "204", description = "Appointment deleted")
    @Override
    public void delete(@PathVariable Long id) {
        getFacade().delete(id);
    }

}
