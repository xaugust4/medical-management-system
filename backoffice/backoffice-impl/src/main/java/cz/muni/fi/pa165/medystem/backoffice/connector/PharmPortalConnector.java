package cz.muni.fi.pa165.medystem.backoffice.connector;

import cz.muni.fi.pa165.medystem.pharmportalapi.dto.ViewPrescriptionResponseDto;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.List;

@Getter
@Setter
@Component
public class PharmPortalConnector {

    WebClient client = WebClient.create("http://pharmportal:8080");

    public List<ViewPrescriptionResponseDto> listPrescriptionsByPatientId(Long patientId) {

        Mono<ViewPrescriptionResponseDto[]> result = client
                .get()
                .uri("/api/pharmportal/prescriptions/patient/" + patientId + "/prescriptions")
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(ViewPrescriptionResponseDto[].class);

        return Arrays
                .stream(result.blockOptional()
                        .orElseThrow(() -> new IllegalArgumentException("Nothing found for patientId: " + patientId)))
                .toList();
    }

}
