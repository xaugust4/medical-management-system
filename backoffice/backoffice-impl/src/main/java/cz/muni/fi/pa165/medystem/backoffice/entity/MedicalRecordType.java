package cz.muni.fi.pa165.medystem.backoffice.entity;

public enum MedicalRecordType {
    UNCATEGORIZED,
    TREATMENT,
    ALLERGY,
    HOSPITALIZATION,
    CHECKUP
}
