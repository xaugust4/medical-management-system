package cz.muni.fi.pa165.medystem.backoffice.init.framework.entity;

public interface DataInitializer {

    void initialize();

    int getOrder();

}
