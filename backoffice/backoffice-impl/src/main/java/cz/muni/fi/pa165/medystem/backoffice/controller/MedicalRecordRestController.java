package cz.muni.fi.pa165.medystem.backoffice.controller;

import cz.muni.fi.pa165.medystem.backoffice.entity.MedicalRecord;
import cz.muni.fi.pa165.medystem.backoffice.facade.MedicalRecordFacade;
import cz.muni.fi.pa165.medystem.backoffice.mapper.MedicalRecordMapper;
import cz.muni.fi.pa165.medystem.backoffice.repository.MedicalRecordRepository;
import cz.muni.fi.pa165.medystem.backoffice.service.MedicalRecordService;
import dto.medrecord.CreateMedicalRecordRequestDto;
import dto.medrecord.MedicalRecordViewResponseDto;
import dto.medrecord.UpdateMedicalRecordRequestDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Max;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Getter
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/backoffice/medical-records")
@Tag(name = "MedicalRecord", description = "Operations related to MedicalRecords")
public class MedicalRecordRestController implements CrudController<
        MedicalRecord,
        CreateMedicalRecordRequestDto,
        MedicalRecordViewResponseDto,
        UpdateMedicalRecordRequestDto,
        MedicalRecordRepository,
        MedicalRecordService,
        MedicalRecordMapper,
        MedicalRecordFacade> {

    private final MedicalRecordFacade facade;

    @GetMapping("/list/by-patient-id/{patientId}")
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Get all MedicalRecords by patiendId", description = "Retrieves details of all MedicalRecords by patientId")
    @ApiResponse(responseCode = "200", description = "MedicalRecords found",
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = MedicalRecordViewResponseDto.class)))
    public List<MedicalRecordViewResponseDto> getMedicalRecordsByPatientId(@PathVariable Long patientId) {
        return getFacade().getMedicalRecordsByPatientId(patientId);
    }

    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(summary = "Create a new MedicalRecord ", description = "Creates a new MedicalRecord with the given details")
    @ApiResponse(responseCode = "201", description = "MedicalRecord created",
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = MedicalRecordViewResponseDto.class)))
    @Override
    public MedicalRecordViewResponseDto create(
            @RequestBody @Valid CreateMedicalRecordRequestDto createDto) {
        return getFacade().create(createDto);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Get a MedicalRecord by ID", description = "Retrieves details of a specific MedicalRecord by ID")
    @ApiResponse(responseCode = "200", description = "MedicalRecord found",
            content = @Content(mediaType = "application/json",
                    schema = @Schema(implementation = MedicalRecordViewResponseDto.class)))
    @Override
    public MedicalRecordViewResponseDto get(@PathVariable Long id) {
        return getFacade().get(id);
    }

    @GetMapping(value = "/all", params = {"page", "size"})
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Get all MedicalRecords", description = "Retrieves details of all MedicalRecords")
    @ApiResponse(responseCode = "200", description = "MedicalRecords found",
            content = @Content(mediaType = "application/json",
                    schema = @Schema(implementation = Page.class)))
    @Override
    public Page<MedicalRecordViewResponseDto> getAll(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "10") @Max(value = 50) int size,
            @RequestParam(value = "sort", defaultValue = "id, asc", required = false) String[] sortingParams
    ) {
        String field = sortingParams[0];
        String sortingDirection = sortingParams[1];
        Sort.Direction direction = sortingDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC;
        Pageable pageObj = PageRequest.of(page, size, Sort.by(direction, field));

        return getFacade().getAll(pageObj);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Update a MedicalRecord", description = "Updates the details of an existing MedicalRecord specified by ID")
    @ApiResponse(responseCode = "200", description = "MedicalRecord  updated",
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = MedicalRecordViewResponseDto.class)))
    @Override
    public MedicalRecordViewResponseDto update(
            @PathVariable Long id,
            @RequestBody @Valid UpdateMedicalRecordRequestDto updateDto) {
        return getFacade().update(id, updateDto);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Operation(summary = "Delete an MedicalRecord", description = "Deletes an MedicalRecord specified by ID")
    @ApiResponse(responseCode = "204", description = "MedicalRecord deleted")
    @Override
    public void delete(@PathVariable Long id) {
        getFacade().delete(id);
    }

}
