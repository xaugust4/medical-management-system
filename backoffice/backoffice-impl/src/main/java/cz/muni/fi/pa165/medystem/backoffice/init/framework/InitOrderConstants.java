package cz.muni.fi.pa165.medystem.backoffice.init.framework;

public interface InitOrderConstants {

    int DOCTOR_ORDER            = 100;
    int PATIENT_ORDER           = DOCTOR_ORDER      + 1;
    int APPOINTMENT_ORDER       = PATIENT_ORDER     + 1;
    int MEDICAL_RECORD_ORDER    = APPOINTMENT_ORDER + 1;

    int DELEGATE_INITIALIZER    = 1000;
}
