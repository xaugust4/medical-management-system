package cz.muni.fi.pa165.medystem.backoffice.controller;

import cz.muni.fi.pa165.medystem.backoffice.entity.AbstractEntity;
import cz.muni.fi.pa165.medystem.backoffice.facade.CrudFacade;
import cz.muni.fi.pa165.medystem.backoffice.mapper.AbstractMapper;
import cz.muni.fi.pa165.medystem.backoffice.service.CrudService;
import jakarta.validation.Valid;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;


public interface CrudController<
        ENTITY extends AbstractEntity<Long>,
        CREATE,
        READ,
        UPDATE,
        REPOSITORY extends JpaRepository<ENTITY, Long>,
        SERVICE extends CrudService<ENTITY, REPOSITORY>,
        MAPPER extends AbstractMapper<ENTITY, CREATE, READ, UPDATE>,
        FACADE extends CrudFacade<ENTITY, CREATE, READ, UPDATE, REPOSITORY, SERVICE, MAPPER>> {

    READ create(@RequestBody @Valid CREATE createDto);

    READ get(@PathVariable(value = "id") Long id);

    Page<READ> getAll(
            int page,
            int size,
            String[] sortingParams
    );

    READ update(@PathVariable(value = "id") Long id, @RequestBody @Valid UPDATE updateDto);

    void delete(@PathVariable(value = "id") Long id);

    FACADE getFacade();
}
