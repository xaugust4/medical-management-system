package cz.muni.fi.pa165.medystem.backoffice.repository;

import cz.muni.fi.pa165.medystem.backoffice.entity.MedicalRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MedicalRecordRepository extends JpaRepository<MedicalRecord, Long> {

    List<MedicalRecord> findAllByPatientId(Long patientId);

}
