package cz.muni.fi.pa165.medystem.backoffice.controller;

import cz.muni.fi.pa165.medystem.backoffice.entity.Doctor;
import cz.muni.fi.pa165.medystem.backoffice.facade.DoctorFacade;
import cz.muni.fi.pa165.medystem.backoffice.mapper.DoctorMapper;
import cz.muni.fi.pa165.medystem.backoffice.repository.DoctorRepository;
import cz.muni.fi.pa165.medystem.backoffice.service.DoctorService;
import dto.doctor.CreateDoctorRequestDto;
import dto.doctor.DoctorViewResponseDto;
import dto.doctor.UpdateDoctorRequestDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Max;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@Getter
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/backoffice/doctors")
@Tag(name = "Doctor", description = "Operations related to Doctors")
public class DoctorRestController implements CrudController<
        Doctor,
        CreateDoctorRequestDto,
        DoctorViewResponseDto,
        UpdateDoctorRequestDto,
        DoctorRepository,
        DoctorService,
        DoctorMapper,
        DoctorFacade> {

    private final DoctorFacade facade;

    @PostMapping("/add-patient/{doctorId}/{patientId}")
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Adds patient to a doctor", description = "Adds patient to a doctor")
    @ApiResponse(responseCode = "200", description = "Doctor created",
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = DoctorViewResponseDto.class)))
    public DoctorViewResponseDto addPatient(
            @PathVariable Long doctorId,
            @PathVariable Long patientId) {
        return getFacade().addPatient(doctorId, patientId);
    }

    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(summary = "Create a new Doctor ", description = "Creates a new Doctor with the given details")
    @ApiResponse(responseCode = "201", description = "Doctor created",
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = DoctorViewResponseDto.class)))
    @Override
    public DoctorViewResponseDto create(
            @RequestBody @Valid CreateDoctorRequestDto createDto) {
        return getFacade().create(createDto);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Get a Doctor by ID", description = "Retrieves details of a specific Doctor by ID")
    @ApiResponse(responseCode = "200", description = "Doctor found",
            content = @Content(mediaType = "application/json",
                    schema = @Schema(implementation = DoctorViewResponseDto.class)))
    @Override
    public DoctorViewResponseDto get(@PathVariable Long id) {
        return getFacade().get(id);
    }

    @GetMapping(value = "/all", params = {"page", "size"})
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Get all Doctors", description = "Retrieves details of all Doctors")
    @ApiResponse(responseCode = "200", description = "Doctors found",
            content = @Content(mediaType = "application/json",
                    schema = @Schema(implementation = Page.class)))
    @Override
    public Page<DoctorViewResponseDto> getAll(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "10") @Max(value = 50) int size,
            @RequestParam(value = "sort", defaultValue = "id, asc", required = false) String[] sortingParams
    ) {
        String field = sortingParams[0];
        String sortingDirection = sortingParams[1];
        Sort.Direction direction = sortingDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC;
        Pageable pageObj = PageRequest.of(page, size, Sort.by(direction, field));

        return getFacade().getAll(pageObj);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Update a Doctor", description = "Updates the details of an existing Doctor specified by ID")
    @ApiResponse(responseCode = "200", description = "Doctor  updated",
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = DoctorViewResponseDto.class)))
    @Override
    public DoctorViewResponseDto update(
            @PathVariable Long id,
            @RequestBody @Valid UpdateDoctorRequestDto updateDto) {
        return getFacade().update(id, updateDto);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Operation(summary = "Delete an Doctor", description = "Deletes an Doctor specified by ID")
    @ApiResponse(responseCode = "204", description = "Doctor deleted")
    @Override
    public void delete(@PathVariable Long id) {
        getFacade().delete(id);
    }
}
