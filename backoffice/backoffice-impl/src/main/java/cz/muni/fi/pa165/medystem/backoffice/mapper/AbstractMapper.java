package cz.muni.fi.pa165.medystem.backoffice.mapper;

import org.mapstruct.MappingTarget;

public interface AbstractMapper<
        ENTITY,
        CREATE,
        READ,
        UPDATE> {

    CREATE entityToCreateDto(ENTITY entity);

    READ entityToReadDto(ENTITY entity);

    ENTITY createDtoToEntity(CREATE create);

    ENTITY readDtoToEntity(READ read);

    ENTITY updateDtoToEntity(UPDATE update);

    ENTITY entityFromUpdateEntity(@MappingTarget ENTITY entity, ENTITY update);

}
