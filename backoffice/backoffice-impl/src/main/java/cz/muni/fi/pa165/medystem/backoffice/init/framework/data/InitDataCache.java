package cz.muni.fi.pa165.medystem.backoffice.init.framework.data;

import cz.muni.fi.pa165.medystem.backoffice.entity.Appointment;
import cz.muni.fi.pa165.medystem.backoffice.entity.Doctor;
import cz.muni.fi.pa165.medystem.backoffice.entity.MedicalRecord;
import cz.muni.fi.pa165.medystem.backoffice.entity.Patient;
import cz.muni.fi.pa165.medystem.backoffice.repository.AppointmentRepository;
import cz.muni.fi.pa165.medystem.backoffice.repository.DoctorRepository;
import cz.muni.fi.pa165.medystem.backoffice.repository.MedicalRecordRepository;
import cz.muni.fi.pa165.medystem.backoffice.repository.PatientRepository;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.function.Function;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Component
public class InitDataCache {

    private Map<Long, Appointment> appointments;
    private Map<Long, Doctor> doctors;
    private Map<Long, Patient> patients;
    private Map<Long, MedicalRecord> medicalRecords;

    private final AppointmentRepository appointmentRepository;
    private final DoctorRepository doctorRepository;
    private final PatientRepository patientRepository;
    private final MedicalRecordRepository medicalRecordRepository;

    private final Random random = new Random();

    public Optional<Appointment> getAppointment(Long id) {
        if (appointments == null) {
            initializeAppointments();
        }
        return Optional.ofNullable(appointments.get(id));
    }

    public Optional<Doctor> getDoctor(Long id) {
        if (doctors == null) {
            initializeDoctors();
        }
        return Optional.ofNullable(doctors.get(id));
    }

    public Optional<Patient> getPatient(Long id) {
        if (patients == null) {
            initializePatients();
        }
        return Optional.ofNullable(patients.get(id));
    }

    public Optional<MedicalRecord> getMedicalRecord(Long id) {
        if (medicalRecords == null) {
            initializeMedicalRecords();
        }
        return Optional.ofNullable(medicalRecords.get(id));
    }

    public Doctor pickRandomDoctor() {
        if (doctors == null) {
            initializeDoctors();
        }

        var doctorsList = doctors.values().stream().toList();
        return doctorsList.get(random.nextInt(doctorsList.size()));
    }

    public Patient pickRandomPatient() {
        if (patients == null) {
            initializePatients();
        }

        var patientsList = patients.values().stream().toList();
        return patientsList.get(random.nextInt(patientsList.size()));

    }

    public void reloadAll() {
        initializeAppointments();
        initializeDoctors();
        initializePatients();
        initializeMedicalRecords();
    }

    public void clearAllCache() {
        appointments = null;
        doctors = null;
        patients = null;
        medicalRecords = null;
    }


    private void initializeAppointments() {
        appointments = appointmentRepository.findAll().stream()
                .collect(
                        Collectors.toMap(Appointment::getId, Function.identity())
                );
    }

    private void initializeDoctors() {
        doctors = doctorRepository.findAll().stream()
                .collect(
                        Collectors.toMap(Doctor::getId, Function.identity())
                );
    }

    private void initializePatients() {
        patients = patientRepository.findAll().stream()
                .collect(
                        Collectors.toMap(Patient::getId, Function.identity())
                );
    }

    private void initializeMedicalRecords() {
        medicalRecords = medicalRecordRepository.findAll().stream()
                .collect(
                        Collectors.toMap(MedicalRecord::getId, Function.identity())
                );
    }
}
