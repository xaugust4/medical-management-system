package cz.muni.fi.pa165.medystem.backoffice.repository;

import cz.muni.fi.pa165.medystem.backoffice.entity.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PatientRepository extends JpaRepository<Patient, Long> {

    Optional<Patient> findByBirthNumber(String birthNumber);
}
