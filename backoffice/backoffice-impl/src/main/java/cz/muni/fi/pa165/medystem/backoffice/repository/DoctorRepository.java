package cz.muni.fi.pa165.medystem.backoffice.repository;

import cz.muni.fi.pa165.medystem.backoffice.entity.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DoctorRepository extends JpaRepository<Doctor, Long> {
}
