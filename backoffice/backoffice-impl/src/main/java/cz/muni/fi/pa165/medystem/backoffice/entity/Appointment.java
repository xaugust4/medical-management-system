package cz.muni.fi.pa165.medystem.backoffice.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "appointments")
@NoArgsConstructor
@Getter
@Setter
public class Appointment extends AbstractEntity<Long> {

    @NotNull
    @ManyToOne
    @JoinColumn(name = "patientId", nullable = false)
    private Patient patient;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "doctorId", nullable = false)
    private Doctor doctor;

    private LocalDateTime appointmentFrom;

    private LocalDateTime appointmentTo;

    @Enumerated(EnumType.STRING)
    private AppointmentStatus status = AppointmentStatus.PLANNED;

    public Appointment(Patient patient, Doctor doctor, LocalDateTime appointmentFrom, LocalDateTime appointmentTo) {
        this.patient = patient;
        this.doctor = doctor;
        this.appointmentFrom = appointmentFrom;
        this.appointmentTo = appointmentTo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Appointment that = (Appointment) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(patient, that.patient) &&
                Objects.equals(doctor, that.doctor) &&
                Objects.equals(appointmentFrom, that.appointmentFrom) &&
                status == that.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), patient, doctor, appointmentFrom, status);
    }
}
