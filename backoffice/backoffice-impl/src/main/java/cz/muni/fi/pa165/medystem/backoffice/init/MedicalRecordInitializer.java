package cz.muni.fi.pa165.medystem.backoffice.init;

import cz.muni.fi.pa165.medystem.backoffice.entity.MedicalRecord;
import cz.muni.fi.pa165.medystem.backoffice.init.framework.InitOrderConstants;
import cz.muni.fi.pa165.medystem.backoffice.init.framework.data.DataPicker;
import cz.muni.fi.pa165.medystem.backoffice.init.framework.data.InitDataCache;
import cz.muni.fi.pa165.medystem.backoffice.init.framework.entity.AbstractInitializer;
import cz.muni.fi.pa165.medystem.backoffice.repository.MedicalRecordRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Component
@Order(InitOrderConstants.MEDICAL_RECORD_ORDER)
@ConditionalOnExpression("${init.medical.enabled:false} || ${init.enabled-all:false}")
public class MedicalRecordInitializer extends AbstractInitializer<Long, MedicalRecord, MedicalRecordRepository> {

    public static final int ORDER = InitOrderConstants.MEDICAL_RECORD_ORDER;

    @Value("${init.medical.size:100}")
    private long seedSize;

    @Value("${init.medical.medicalIdStart:1000}")
    private long medicalIdStart;

    private final InitDataCache initDataCache;

    protected MedicalRecordInitializer(MedicalRecordRepository repository, InitDataCache initDataCache) {
        super(repository, ORDER);
        this.initDataCache = initDataCache;
    }

    @Override
    protected List<MedicalRecord> initializeEntities() {
        List<MedicalRecord> entities = new ArrayList<>();

        for (int i = 1; i <= seedSize; i++) {
            entities.add(initializeEntity());
        }
        return entities;
    }

    private MedicalRecord initializeEntity() {
        var medicalRecord = new MedicalRecord();
        medicalRecord.setId(medicalIdStart++);
        medicalRecord.setMedicalRecordType(DataPicker.RandomGenerator.randomMedicalRecordType());
        medicalRecord.setRecordDetails("DUMMY info");
        medicalRecord.setPatient(initDataCache.pickRandomPatient());
        return medicalRecord;
    }

    public int getOrder() {
        return ORDER;
    }
}
