package cz.muni.fi.pa165.medystem.backoffice.facade;

import cz.muni.fi.pa165.medystem.backoffice.entity.MedicalRecord;
import cz.muni.fi.pa165.medystem.backoffice.entity.Patient;
import cz.muni.fi.pa165.medystem.backoffice.mapper.MedicalRecordMapper;
import cz.muni.fi.pa165.medystem.backoffice.repository.MedicalRecordRepository;
import cz.muni.fi.pa165.medystem.backoffice.service.MedicalRecordService;
import cz.muni.fi.pa165.medystem.backoffice.service.PatientService;
import dto.medrecord.CreateMedicalRecordRequestDto;
import dto.medrecord.MedicalRecordViewResponseDto;
import dto.medrecord.UpdateMedicalRecordRequestDto;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Getter
@RequiredArgsConstructor
@Service
public class MedicalRecordFacade extends CrudFacade<
        MedicalRecord,
        CreateMedicalRecordRequestDto,
        MedicalRecordViewResponseDto,
        UpdateMedicalRecordRequestDto,
        MedicalRecordRepository,
        MedicalRecordService,
        MedicalRecordMapper> {

    private final MedicalRecordService service;
    private final PatientService patientService;
    private final MedicalRecordMapper mapper;

    public MedicalRecordViewResponseDto create(CreateMedicalRecordRequestDto dto) {
        Optional<Patient> medicalPatientOptional = patientService.get(dto.getPatientId());

        Patient medicalPatient = medicalPatientOptional.orElseThrow(
                () -> new IllegalArgumentException("Patient with id " + dto.getPatientId() + " does not exist")
        );

        MedicalRecord toCreate = mapper.createDtoToEntity(dto);
        toCreate.setPatient(medicalPatient);

        return mapper.entityToReadDto(service.create(toCreate));
    }

    public List<MedicalRecordViewResponseDto> getMedicalRecordsByPatientId(Long patientId) {
        Optional<Patient> patientOptional = patientService.get(patientId);
        patientOptional.orElseThrow(
                () -> new IllegalArgumentException("Patient with id " + patientId + " does not exist")
        );
        return service.getMedicalRecordsByPatientId(patientId).stream()
                .map(mapper::entityToReadDto)
                .toList();
    }
}
