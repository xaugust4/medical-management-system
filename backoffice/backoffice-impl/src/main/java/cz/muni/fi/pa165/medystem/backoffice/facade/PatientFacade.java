package cz.muni.fi.pa165.medystem.backoffice.facade;

import cz.muni.fi.pa165.medystem.backoffice.connector.PharmPortalConnector;
import cz.muni.fi.pa165.medystem.backoffice.connector.UserConnector;
import cz.muni.fi.pa165.medystem.backoffice.entity.Patient;
import cz.muni.fi.pa165.medystem.backoffice.mapper.PatientMapper;
import cz.muni.fi.pa165.medystem.backoffice.repository.PatientRepository;
import cz.muni.fi.pa165.medystem.backoffice.service.PatientService;
import cz.muni.fi.pa165.medystem.pharmportalapi.dto.ViewPrescriptionResponseDto;
import cz.muni.fi.pa165.medystem.usermanagement.dto.UserCreateRequestDto;
import dto.patient.CreatePatientRequestDto;
import dto.patient.PatientViewResponseDto;
import dto.patient.RegisterPatientRequestDto;
import dto.patient.UpdatePatientRequestDto;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;


@Getter
@RequiredArgsConstructor
@Service
public class PatientFacade extends CrudFacade<
        Patient,
        CreatePatientRequestDto,
        PatientViewResponseDto,
        UpdatePatientRequestDto,
        PatientRepository,
        PatientService,
        PatientMapper> {

    private final PatientService service;
    private final UserConnector userConnector;
    private final PharmPortalConnector pharmPortalConnector;
    private final PatientMapper mapper;

    public PatientViewResponseDto findPatientByBirthNumber(String birthNumber) {
        Patient patient = service.getPatientByBirthNumber(birthNumber);
        return mapper.entityToReadDto(patient);
    }

    public PatientViewResponseDto registerPatient(RegisterPatientRequestDto registerPatientRequestDto) {
        Patient foundPatient = service.get(registerPatientRequestDto.getPatientId())
                .orElseThrow(() -> new IllegalArgumentException("Patient not found for id: " + registerPatientRequestDto.getPatientId()));

        UserCreateRequestDto userToCreate = new UserCreateRequestDto();
        userToCreate.setFirstName(foundPatient.getFirstName());
        userToCreate.setLastName(foundPatient.getLastName());
        userToCreate.setUsername(registerPatientRequestDto.getUsername());

        var result = userConnector.createUser(userToCreate);
        foundPatient.setUserId(result.getId());
        return mapper.entityToReadDto(service.update(foundPatient));
    }

    public List<ViewPrescriptionResponseDto> listPrescriptionsByPatientId(Long patientId) {
        return pharmPortalConnector.listPrescriptionsByPatientId(patientId);
    }
}
