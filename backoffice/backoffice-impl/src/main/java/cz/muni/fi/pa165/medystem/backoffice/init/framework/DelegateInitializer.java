package cz.muni.fi.pa165.medystem.backoffice.init.framework;

import cz.muni.fi.pa165.medystem.backoffice.init.framework.entity.DataInitializer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Comparator;

@Slf4j
@Component
@Order(InitOrderConstants.DELEGATE_INITIALIZER)
public class DelegateInitializer implements ApplicationListener<ApplicationReadyEvent> {

    protected Collection<DataInitializer> initializers;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {

        var context = event.getApplicationContext();

        initializers = context.getBeansOfType(DataInitializer.class).values();

        initializers.stream().sorted(Comparator.comparing(DataInitializer::getOrder)).forEach(DataInitializer::initialize);

    }

}
