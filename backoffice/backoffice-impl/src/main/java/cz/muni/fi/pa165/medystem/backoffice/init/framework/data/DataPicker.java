package cz.muni.fi.pa165.medystem.backoffice.init.framework.data;

import cz.muni.fi.pa165.medystem.backoffice.entity.AppointmentStatus;
import cz.muni.fi.pa165.medystem.backoffice.entity.DoctorDailyAvailable;
import cz.muni.fi.pa165.medystem.backoffice.entity.MedicalRecordType;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Random;

public class DataPicker {

    private static final String[] FIRST_NAMES = {
            "John", "Jane", "Michael", "Jessica", "David", "Sarah", "Robert", "Emily", "William", "Ashley"
    };

    private static final String[] LAST_NAMES = {
            "Smith", "Johnson", "Williams", "Jones", "Brown", "Davis", "Miller", "Wilson", "Moore", "Taylor"
    };

    private static final String[] INSURANCE_COMPANIES = {
            "VZP", "ZP", "ZPMV", "VZP", "ZP", "ZPMV", "VZP", "ZP", "ZPMV", "VZP"
    };

    private static final String[] SPECIALIZATIONS = {
            "Cardiology", "Dermatology", "Endocrinology", "Gastroenterology", "Hematology", "Infectious diseases", "Nephrology", "Neurology", "Oncology", "Pulmonology"
    };


    public static class RandomGenerator {

        private static final Random random = new Random();

        public static LocalDate randomDateBetween(int startYear, int endYear) {
            return LocalDate.of(
                    random.nextInt(startYear, endYear),
                    random.nextInt(1, 13),
                    random.nextInt(1, 29)
            );
        }

        public static LocalDateTime randomDateTimeBetween(int startYear, int endYear) {
            return LocalDateTime.of(
                    random.nextInt(startYear, endYear),
                    random.nextInt(1, 13),
                    random.nextInt(1, 29),
                    random.nextInt(0, 24),
                    random.nextInt(0, 60),
                    random.nextInt(0, 60)
            );
        }

        public static String randomFirstName() {
            return FIRST_NAMES[random.nextInt(FIRST_NAMES.length)];
        }

        public static String randomLastName() {
            return LAST_NAMES[random.nextInt(LAST_NAMES.length)];
        }

        public static String randomInsuranceCompany() {
            return INSURANCE_COMPANIES[random.nextInt(INSURANCE_COMPANIES.length)];
        }

        public static String randomTelephoneNumber() {
            return String.valueOf(random.nextInt(100000000, 999999999));
        }

        public static String randomBirthNumber() {
            return random.nextInt(100000, 999999) + "/" + random.nextInt(1000, 9999);
        }

        public static String randomSpecialization() {
            return SPECIALIZATIONS[random.nextInt(SPECIALIZATIONS.length)];
        }

        public static DoctorDailyAvailable randomDailyAvailableHours() {
            DoctorDailyAvailable doctorDailyAvailable = new DoctorDailyAvailable();
            doctorDailyAvailable.setDayOfWeek(DayOfWeek.of(random.nextInt(1, 8)));

            int from = random.nextInt(0, 23);
            doctorDailyAvailable.setFromHour(random.nextInt(from, 24));
            doctorDailyAvailable.setToHour(random.nextInt(from, 24));
            doctorDailyAvailable.setFromMinute(random.nextInt(0, 59));
            doctorDailyAvailable.setToMinute(random.nextInt(0, 59));

            return doctorDailyAvailable;
        }

        public static AppointmentStatus randomAppointmentStatus() {
            return AppointmentStatus.values()[random.nextInt(AppointmentStatus.values().length)];
        }

        public static MedicalRecordType randomMedicalRecordType() {
            return MedicalRecordType.values()[random.nextInt(MedicalRecordType.values().length)];
        }
    }
}
