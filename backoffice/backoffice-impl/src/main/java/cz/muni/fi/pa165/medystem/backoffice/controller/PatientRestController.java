package cz.muni.fi.pa165.medystem.backoffice.controller;

import cz.muni.fi.pa165.medystem.backoffice.entity.Patient;
import cz.muni.fi.pa165.medystem.backoffice.facade.PatientFacade;
import cz.muni.fi.pa165.medystem.backoffice.mapper.PatientMapper;
import cz.muni.fi.pa165.medystem.backoffice.repository.PatientRepository;
import cz.muni.fi.pa165.medystem.backoffice.service.PatientService;
import cz.muni.fi.pa165.medystem.pharmportalapi.dto.ViewPrescriptionResponseDto;
import dto.patient.CreatePatientRequestDto;
import dto.patient.PatientViewResponseDto;
import dto.patient.RegisterPatientRequestDto;
import dto.patient.UpdatePatientRequestDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Max;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Getter
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/backoffice/patients")
@Tag(name = "Patient", description = "Operations related to Patients")
public class PatientRestController implements CrudController<
        Patient,
        CreatePatientRequestDto,
        PatientViewResponseDto,
        UpdatePatientRequestDto,
        PatientRepository,
        PatientService,
        PatientMapper,
        PatientFacade> {

    private final PatientFacade facade;

    @GetMapping("/search/by-birth-number/{birthNumber}")
    @ResponseStatus(HttpStatus.OK)
    public PatientViewResponseDto getPatientByBirthNumber(@PathVariable("birthNumber") String birthNumber) {
        return facade.findPatientByBirthNumber(birthNumber);
    }

    @PostMapping("/register")
    @ResponseStatus(HttpStatus.OK)
    public PatientViewResponseDto registerPatient(
            @RequestBody
            @Valid
            RegisterPatientRequestDto patientRegisterRequest) {
        return facade.registerPatient(patientRegisterRequest);
    }

    @GetMapping("{patientId}/presc/list")
    @ResponseStatus(HttpStatus.OK)
    public List<ViewPrescriptionResponseDto> listPrescriptionsByPatientId(@PathVariable Long patientId) {
        return facade.listPrescriptionsByPatientId(patientId);
    }


    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(summary = "Create a new Patient ", description = "Creates a new Patient with the given details")
    @ApiResponse(responseCode = "201", description = "Patient created",
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = PatientViewResponseDto.class)))
    @Override
    public PatientViewResponseDto create(
            @RequestBody @Valid CreatePatientRequestDto createDto) {
        return getFacade().create(createDto);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Get a Patient by ID", description = "Retrieves details of a specific Patient by ID")
    @ApiResponse(responseCode = "200", description = "Patient found",
            content = @Content(mediaType = "application/json",
                    schema = @Schema(implementation = PatientViewResponseDto.class)))
    @Override
    public PatientViewResponseDto get(@PathVariable Long id) {
        return getFacade().get(id);
    }

    @GetMapping(value = "/all", params = {"page", "size"})
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Get all Patients", description = "Retrieves details of all Patients")
    @ApiResponse(responseCode = "200", description = "Patients found",
            content = @Content(mediaType = "application/json",
                    schema = @Schema(implementation = Page.class)))
    @Override
    public Page<PatientViewResponseDto> getAll(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "10") @Max(value = 50) int size,
            @RequestParam(value = "sort", defaultValue = "id, asc", required = false) String[] sortingParams
    ) {
        String field = sortingParams[0];
        String sortingDirection = sortingParams[1];
        Sort.Direction direction = sortingDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC;
        Pageable pageObj = PageRequest.of(page, size, Sort.by(direction, field));

        return getFacade().getAll(pageObj);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Update a Patient", description = "Updates the details of an existing Patient specified by ID")
    @ApiResponse(responseCode = "200", description = "Patient  updated",
            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = PatientViewResponseDto.class)))
    @Override
    public PatientViewResponseDto update(
            @PathVariable Long id,
            @RequestBody @Valid UpdatePatientRequestDto updateDto) {
        return getFacade().update(id, updateDto);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Operation(summary = "Delete an Patient", description = "Deletes an Patient specified by ID")
    @ApiResponse(responseCode = "204", description = "Patient deleted")
    @Override
    public void delete(@PathVariable Long id) {
        getFacade().delete(id);
    }
}
