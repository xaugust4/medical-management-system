package cz.muni.fi.pa165.medystem.backoffice.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@RequiredArgsConstructor
@Service
public abstract class CrudService<
        ENTITY,
        REPOSITORY extends JpaRepository<ENTITY, Long>> {

    @Transactional
    public ENTITY create(ENTITY entity) {
        return getRepository().save(entity);
    }

    @Transactional(readOnly = true)
    public Optional<ENTITY> get(Long id) {
        return getRepository().findById(id);
    }

    @Transactional(readOnly = true)
    public Page<ENTITY> getAll(Pageable pageable) {
        return getRepository().findAll(pageable);
    }

    @Transactional
    public ENTITY update(ENTITY entity) {
        return getRepository().save(entity);
    }

    @Transactional
    public void delete(Long id) {
        getRepository().deleteById(id);
    }

    public abstract REPOSITORY getRepository();

}
