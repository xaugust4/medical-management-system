package cz.muni.fi.pa165.medystem.backoffice.init;

import cz.muni.fi.pa165.medystem.backoffice.entity.Appointment;
import cz.muni.fi.pa165.medystem.backoffice.init.framework.InitOrderConstants;
import cz.muni.fi.pa165.medystem.backoffice.init.framework.data.DataPicker;
import cz.muni.fi.pa165.medystem.backoffice.init.framework.data.InitDataCache;
import cz.muni.fi.pa165.medystem.backoffice.init.framework.entity.AbstractInitializer;
import cz.muni.fi.pa165.medystem.backoffice.repository.AppointmentRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Getter
@Setter
@Component
@Order(InitOrderConstants.APPOINTMENT_ORDER)
@ConditionalOnExpression("${init.appointment.enabled:false} || ${init.enabled-all:false}")
public class AppointmentInitializer extends AbstractInitializer<Long, Appointment, AppointmentRepository> {

    public static final int ORDER = InitOrderConstants.APPOINTMENT_ORDER;

    @Value("${init.appointment.size:100}")
    private long seedSize;

    @Value("${init.appointment.appointmentIdStart:1000}")
    private long appointmentIdStart;

    private final InitDataCache initDataCache;

    @Autowired
    protected AppointmentInitializer(AppointmentRepository repository, InitDataCache initDataCache) {
        super(repository, ORDER);
        this.initDataCache = initDataCache;
    }

    @Override
    protected List<Appointment> initializeEntities() {
        List<Appointment> entities = new ArrayList<>();

        for (int i = 1; i <= seedSize; i++) {
            entities.add(initializeEntity());
        }
        return entities;
    }

    private Appointment initializeEntity() {
        var appointment = new Appointment();

        appointment.setId(appointmentIdStart++);
        var from = DataPicker.RandomGenerator.randomDateTimeBetween(2022, 2023);
        var to = from.plusHours(2);
        appointment.setAppointmentFrom(from);
        appointment.setAppointmentTo(to);
        appointment.setStatus(DataPicker.RandomGenerator.randomAppointmentStatus());

        var doc = initDataCache.pickRandomDoctor();
        appointment.setDoctor(doc);
        appointment.setPatient(initDataCache.pickRandomPatient());

        return appointment;
    }
}
