package cz.muni.fi.pa165.medystem.backoffice.mapper;

import cz.muni.fi.pa165.medystem.backoffice.entity.Doctor;
import dto.doctor.CreateDoctorRequestDto;
import dto.doctor.DoctorViewResponseDto;
import dto.doctor.UpdateDoctorRequestDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface DoctorMapper extends AbstractMapper<
        Doctor,
        CreateDoctorRequestDto,
        DoctorViewResponseDto,
        UpdateDoctorRequestDto> {

    @Override
    CreateDoctorRequestDto entityToCreateDto(Doctor entity);

    @Override
    DoctorViewResponseDto entityToReadDto(Doctor entity);

    @Override
    Doctor createDtoToEntity(CreateDoctorRequestDto read);

    @Override
    Doctor readDtoToEntity(DoctorViewResponseDto read);

    @Override
    Doctor updateDtoToEntity(UpdateDoctorRequestDto read);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "userId", ignore = true)
    @Mapping(target = "appointments", ignore = true)
    @Mapping(target = "patients", ignore = true)
    @Override
    Doctor entityFromUpdateEntity(@MappingTarget Doctor entity, Doctor read);

}
