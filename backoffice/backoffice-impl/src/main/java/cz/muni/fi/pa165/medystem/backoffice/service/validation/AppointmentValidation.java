package cz.muni.fi.pa165.medystem.backoffice.service.validation;

import cz.muni.fi.pa165.medystem.backoffice.entity.Appointment;
import cz.muni.fi.pa165.medystem.backoffice.entity.AppointmentStatus;
import cz.muni.fi.pa165.medystem.backoffice.entity.Doctor;
import cz.muni.fi.pa165.medystem.backoffice.entity.DoctorDailyAvailable;
import cz.muni.fi.pa165.medystem.backoffice.service.AppointmentService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Set;

@RequiredArgsConstructor
@Service
public class AppointmentValidation {

    private final AppointmentService appointmentService;

    private boolean isIntervalValid(LocalDateTime from, LocalDateTime to) {
        return from.isBefore(to) && from.getYear() == to.getYear() && from.getDayOfYear() == to.getDayOfYear();
    }

    private boolean isWithinAvailableTime(LocalTime availableFrom, LocalTime availableTo, LocalTime appointmentFrom, LocalTime appointmentTo) {
        return !appointmentFrom.isBefore(availableFrom) && !appointmentTo.isAfter(availableTo);
    }

    public void isAppointmentValid(Appointment toCreate, Doctor doctor) {
        if (!isIntervalValid(toCreate.getAppointmentFrom(), toCreate.getAppointmentTo())) {
            throw new IllegalArgumentException("Invalid interval");
        }
        Set<DoctorDailyAvailable> doctorDailyAvailable = doctor.getDailyAvailableHours();
        List<Appointment> appointmentsForDoctor = appointmentService.findAllByDoctorId(doctor.getId());

        var anyTimeAvailable = doctorDailyAvailable.stream()
                .filter(available -> available.getDayOfWeek() == toCreate.getAppointmentFrom().getDayOfWeek())
                .filter(available -> {
                    LocalTime availableFrom = LocalTime.of(available.getFromHour(), available.getFromMinute());
                    LocalTime availableTo = LocalTime.of(available.getToHour(), available.getToMinute());
                    LocalTime appointmentFrom = toCreate.getAppointmentFrom().toLocalTime();
                    LocalTime appointmentTo = toCreate.getAppointmentTo().toLocalTime();
                    return isWithinAvailableTime(availableFrom, availableTo, appointmentFrom, appointmentTo);
                })
                .findAny();

        if (anyTimeAvailable.isEmpty()) {
            throw new IllegalArgumentException("Doctor is not available at this time");
        }

        boolean isTimeOverlapping = appointmentsForDoctor.stream()
                .filter(appointment -> appointment.getStatus() == AppointmentStatus.CONFIRMED || appointment.getStatus() == AppointmentStatus.PLANNED)
                .anyMatch(appointment ->
                        appointment.getAppointmentFrom().isBefore(toCreate.getAppointmentTo()) &&
                                appointment.getAppointmentTo().isAfter(toCreate.getAppointmentFrom())
                );

        if (isTimeOverlapping) {
            throw new IllegalArgumentException("There is already an appointment at this time");
        }
    }
}
