package cz.muni.fi.pa165.medystem.backoffice.mapper;

import cz.muni.fi.pa165.medystem.backoffice.entity.MedicalRecord;
import dto.medrecord.CreateMedicalRecordRequestDto;
import dto.medrecord.MedicalRecordViewResponseDto;
import dto.medrecord.UpdateMedicalRecordRequestDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface MedicalRecordMapper extends AbstractMapper<
        MedicalRecord,
        CreateMedicalRecordRequestDto,
        MedicalRecordViewResponseDto,
        UpdateMedicalRecordRequestDto> {

    @Override
    CreateMedicalRecordRequestDto entityToCreateDto(MedicalRecord entity);

    @Mapping(target = "patientId", source = "patient.id")
    @Override
    MedicalRecordViewResponseDto entityToReadDto(MedicalRecord entity);

    @Override
    MedicalRecord createDtoToEntity(CreateMedicalRecordRequestDto read);

    @Override
    MedicalRecord readDtoToEntity(MedicalRecordViewResponseDto read);

    @Override
    MedicalRecord updateDtoToEntity(UpdateMedicalRecordRequestDto read);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "patient", ignore = true)
    @Override
    MedicalRecord entityFromUpdateEntity(@MappingTarget MedicalRecord entity, MedicalRecord read);
}
