package cz.muni.fi.pa165.medystem.backoffice.connector;

import cz.muni.fi.pa165.medystem.usermanagement.dto.UserCreateRequestDto;
import cz.muni.fi.pa165.medystem.usermanagement.dto.UserViewResponseDto;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Getter
@Setter
@Component
public class UserConnector {

    WebClient client = WebClient.create("http://users:8080");

    public UserViewResponseDto createUser(UserCreateRequestDto toCreate) {
        Mono<UserViewResponseDto> result = client
                .post()
                .uri("/api/users/create")
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(toCreate), UserCreateRequestDto.class)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(UserViewResponseDto.class);

        return result.blockOptional()
                .orElseThrow(() -> new IllegalArgumentException("User not created"));

    }

    public UserViewResponseDto getUserByUsername(String username) {
        Mono<UserViewResponseDto> result = client
                .get()
                .uri("/api/users/username/" + username)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(UserViewResponseDto.class);

        return result.blockOptional()
                .orElseThrow(() -> new IllegalArgumentException("User not found"));
    }
}
