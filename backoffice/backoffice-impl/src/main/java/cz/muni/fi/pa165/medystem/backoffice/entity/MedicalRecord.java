package cz.muni.fi.pa165.medystem.backoffice.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;

@Entity
@Table(name = "medical_records")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class MedicalRecord extends AbstractEntity<Long> {

    @ManyToOne
    @JoinColumn(name = "patientId", nullable = false)
    private Patient patient;

    @Enumerated(EnumType.STRING)
    private MedicalRecordType medicalRecordType = MedicalRecordType.UNCATEGORIZED;

    @Column(length = 2048) // Not ideal for relational database, but...
    private String recordDetails;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        MedicalRecord that = (MedicalRecord) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(patient, that.patient) &&
                Objects.equals(recordDetails, that.recordDetails) &&
                medicalRecordType == that.getMedicalRecordType();
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), patient, recordDetails, medicalRecordType);
    }
}
