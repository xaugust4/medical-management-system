package cz.muni.fi.pa165.medystem.backoffice.entity;

public enum AppointmentStatus {
    PLANNED,
    CONFIRMED,
    FINISHED,
    MISSED,
    CANCELLED
}
