package cz.muni.fi.pa165.medystem.backoffice.entity;

import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.DayOfWeek;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class DoctorDailyAvailable {

    // TODO - Add tests for Minutes!

    private DayOfWeek dayOfWeek;

    private Integer fromHour;
    private Integer fromMinute;

    private Integer toHour;
    private Integer toMinute;
}
