package cz.muni.fi.pa165.medystem.backoffice.entity;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "patients")
@NoArgsConstructor
@Getter
@Setter
public class Patient extends AbstractEntity<Long> {

    private String firstName;

    private String lastName;

    private String telephoneNumber;

    private String insuranceCompany;

    private String birthNumber;

    private LocalDate birthDate;

    private Long userId;

    @OneToMany(mappedBy = "patient",
            cascade = {CascadeType.MERGE, CascadeType.PERSIST},
            fetch = FetchType.LAZY
    )
    private Set<MedicalRecord> medicalRecords = new HashSet<>();

    @OneToMany(mappedBy = "patient",
            cascade = {CascadeType.MERGE, CascadeType.PERSIST},
            fetch = FetchType.LAZY
    )
    private Set<Appointment> appointments = new HashSet<>();

    public Patient(String firstName, String lastName, String telephoneNumber, String insuranceCompany, String birthNumber, LocalDate birthDate) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.telephoneNumber = telephoneNumber;
        this.insuranceCompany = insuranceCompany;
        this.birthNumber = birthNumber;
        this.birthDate = birthDate;
    }

    public void addMedicalRecord(MedicalRecord medicalRecord) {
        medicalRecords.add(medicalRecord);
        medicalRecord.setPatient(this);
    }

    public void addAppointment(Appointment appointment) {
        appointments.add(appointment);
        appointment.setPatient(this);
    }

    public boolean isAnonymous() {
        return userId == null;
    }

    @Override
    public final boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Patient patient)) return false;
        if (!super.equals(o)) return false;

        return Objects.equals(firstName, patient.firstName) && Objects.equals(lastName, patient.lastName) && Objects.equals(telephoneNumber, patient.telephoneNumber) && Objects.equals(insuranceCompany, patient.insuranceCompany) && Objects.equals(birthNumber, patient.birthNumber) && Objects.equals(birthDate, patient.birthDate) && Objects.equals(userId, patient.userId) && Objects.equals(medicalRecords, patient.medicalRecords) && Objects.equals(appointments, patient.appointments);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + Objects.hashCode(firstName);
        result = 31 * result + Objects.hashCode(lastName);
        result = 31 * result + Objects.hashCode(telephoneNumber);
        result = 31 * result + Objects.hashCode(insuranceCompany);
        result = 31 * result + Objects.hashCode(birthNumber);
        result = 31 * result + Objects.hashCode(birthDate);
        result = 31 * result + Objects.hashCode(userId);
        result = 31 * result + Objects.hashCode(medicalRecords);
        result = 31 * result + Objects.hashCode(appointments);
        return result;
    }
}
