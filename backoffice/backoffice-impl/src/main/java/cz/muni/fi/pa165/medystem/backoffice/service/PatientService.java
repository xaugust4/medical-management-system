package cz.muni.fi.pa165.medystem.backoffice.service;

import cz.muni.fi.pa165.medystem.backoffice.entity.Patient;
import cz.muni.fi.pa165.medystem.backoffice.repository.PatientRepository;
import jakarta.persistence.EntityNotFoundException;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Getter
@RequiredArgsConstructor
@Service
public class PatientService extends CrudService<Patient, PatientRepository> {

    private final PatientRepository repository;


    @Transactional(readOnly = true)
    public Patient getPatientByBirthNumber(String birthNumber) {
        // TODO BirthNumber format parsing
        return repository.findByBirthNumber(birthNumber)
                .orElseThrow(() -> new EntityNotFoundException("Patient not found for this birth number :: " + birthNumber));
    }
}
