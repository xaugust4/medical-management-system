#!/bin/bash

user_exists() {
  psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" -tAc "SELECT 1 FROM pg_roles WHERE rolname='$1'" | grep -q 1
}

database_exists() {
  psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" -tAc "SELECT 1 FROM pg_database WHERE datname='$1'" | grep -q 1
}

# Create the user if it doesn't exist
if ! user_exists "$BACKOFFICE_DATABASE_USER"; then
  echo "Creating database user '$BACKOFFICE_DATABASE_USER' since it does not exist."
  psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
      CREATE USER "$BACKOFFICE_DATABASE_USER" WITH PASSWORD '$BACKOFFICE_DATABASE_PASSWORD';
EOSQL
else
  echo "User '$BACKOFFICE_DATABASE_USER' already exists, skipping creation."
fi

# Create the database if it doesn't exist
if ! database_exists "$BACKOFFICE_DATABASE_NAME"; then
  echo "Creating database $BACKOFFICE_DATABASE_NAME since it does not exist."
  psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
      CREATE DATABASE "$BACKOFFICE_DATABASE_NAME";
      GRANT ALL PRIVILEGES ON DATABASE "$BACKOFFICE_DATABASE_NAME" TO "$BACKOFFICE_DATABASE_USER";
EOSQL
else
  echo "Database '$BACKOFFICE_DATABASE_NAME' already exists, skipping creation."
fi
