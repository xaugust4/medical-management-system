package dto.medrecord;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Schema(description = "Response dto for viewing a medical record")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MedicalRecordViewResponseDto {

    @Schema(description = "Unique identifier of the patient", example = "1")
    private Long patientId;

    //TODO - add enum here?
    @Schema(description = "Type of the medical record", example = "ALLERGY")
    private MedicalRecordType medicalRecordType;

    @Schema(description = "Details of the medical record", example = "Peanut allergy")
    private String recordDetails;

}
