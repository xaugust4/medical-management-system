package dto.medrecord;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.*;

@Schema(description = "Request dto for updating a medical record")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UpdateMedicalRecordRequestDto {

    //TODO - add enum here?
    @Schema(description = "Type of the medical record", example = "ALLERGY")
    @Size(min = 2, max = 40)
    @NotNull
    private MedicalRecordType medicalRecordType;

    @Schema(description = "Details of the medical record", example = "Peanut allergy")
    private String recordDetails;

}
