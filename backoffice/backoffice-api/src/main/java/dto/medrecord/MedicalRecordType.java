package dto.medrecord;

public enum MedicalRecordType {
    UNCATEGORIZED,
    TREATMENT,
    ALLERGY,
    HOSPITALIZATION,
    CHECKUP
}
