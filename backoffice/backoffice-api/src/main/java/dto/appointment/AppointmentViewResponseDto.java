package dto.appointment;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Schema(description = "Response dto for viewing appointments")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class AppointmentViewResponseDto {

    @Schema(description = "Unique identifier of the appointment", example = "1")
    private Long id;

    @Schema(description = "Unique identifier of the patient", example = "1")
    private Long patientId;

    @Schema(description = "Unique identifier of the doctor", example = "1")
    private Long doctorId;

    @Schema(description = "Time of the start of the appointment", example = "2022-01-01T12:00:00")
    private LocalDateTime appointmentFrom;

    @Schema(description = "Time of the end of the appointment", example = "2022-01-01T13:00:00")
    private LocalDateTime appointmentTo;

    @Schema(description = "Status of the appointment", example = "CONFIRMED")
    private String status;

}
