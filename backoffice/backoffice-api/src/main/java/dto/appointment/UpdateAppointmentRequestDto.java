package dto.appointment;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Schema(description = "Request dto for updating an appointment")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class UpdateAppointmentRequestDto {

    @Schema(description = "Unique identifier of the doctor", example = "1")
    private Long doctorId;

    @Schema(description = "Time of the start of the appointment", example = "2022-01-01T12:00:00")
    private LocalDateTime appointmentFrom;

    @Schema(description = "Time of the end of the appointment", example = "2022-01-01T13:00:00")
    private LocalDateTime appointmentTo;

    @Schema(description = "Status of the appointment", example = "CONFIRMED")
    private String status;
}
