package dto.doctor;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Schema(description = "Response dto for viewing a doctor")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class DoctorViewResponseDto {

    @Schema(description = "Unique identifier of the doctor", example = "1")
    private Long id;

    @Schema(description = "Unique identifier of the user", example = "1")
    private String userId;

    @Schema(description = "Specialization of the doctor", example = "Cardiologist")
    private String specialization;
}