package dto.doctor;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Schema(description = "Request dto for creating a doctor")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class CreateDoctorRequestDto {

    @Schema(description = "Unique identifier of the user", example = "1")
    @NotBlank
    @NotNull
    private String userId;

    @Schema(description = "Specialization of the doctor", example = "Cardiologist")
    @Size(min = 2, max = 255)
    @NotNull
    private String specialization;

    @Schema(description = "Map of available hours for each day", example = "[{\"dayOfWeek\":\"MONDAY\",\"from\":8,\"to\":16},{\"dayOfWeek\":\"TUESDAY\",\"from\":9,\"to\":17}]")
    private Set<DoctorDailyAvailableDto> dailyAvailableHours;

}
