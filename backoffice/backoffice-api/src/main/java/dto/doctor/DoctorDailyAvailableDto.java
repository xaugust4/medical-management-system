package dto.doctor;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.DayOfWeek;

@Schema(description = "Dto for doctor's daily available hours")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DoctorDailyAvailableDto {

    @Schema(description = "Day of the week", example = "MONDAY")
    @NotNull
    private DayOfWeek dayOfWeek;

    @Schema(description = "From what hour the doctor is available", example = "8")
    @NotNull
    @Min(value = 0)
    @Max(value = 23)
    private Integer fromHour;

    @Schema(description = "From what hour the doctor is available", example = "8")
    @NotNull
    @Min(value = 0)
    @Max(value = 59)
    private Integer fromMinute;

    @Schema(description = "To what hour the doctor is available", example = "16")
    @NotNull
    @Min(value = 0)
    @Max(value = 23)
    private Integer toHour;

    @Schema(description = "To what hour the doctor is available", example = "16")
    @NotNull
    @Min(value = 0)
    @Max(value = 59)
    private Integer toMinute;
}
