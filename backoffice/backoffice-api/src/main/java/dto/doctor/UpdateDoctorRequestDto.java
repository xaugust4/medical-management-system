package dto.doctor;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Schema(description = "Request dto for updating a doctor")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UpdateDoctorRequestDto {

    @Schema(description = "Unique identifier of the doctor", example = "1")
    @Size(min = 2, max = 255)
    @NotNull
    private String specialization;

    @Schema(description = "Map of available hours for each day", example = "[{\"dayOfWeek\":\"MONDAY\",\"from\":8,\"to\":16},{\"dayOfWeek\":\"TUESDAY\",\"from\":9,\"to\":17}]")
    private Set<DoctorDailyAvailableDto> dailyAvailableHours;

}
