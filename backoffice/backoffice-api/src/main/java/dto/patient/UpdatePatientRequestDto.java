package dto.patient;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Schema(description = "Request dto for updating a patient")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class UpdatePatientRequestDto {

    @Schema(description = "First name of the patient", example = "John")
    @Size(min = 2, max = 255)
    @NotNull
    private String firstName;

    @Schema(description = "Last name of the patient", example = "Doe")
    @Size(min = 2, max = 255)
    @NotNull
    private String lastName;

    @Schema(description = "Telephone number of the patient", example = "123456789")
    @Size(min = 9, max = 12)
    @NotNull
    private String telephoneNumber;

    @Schema(description = "Insurance company of the patient", example = "ČPZP")
    @Size(min = 2, max = 255)
    private String insuranceCompany;
}
