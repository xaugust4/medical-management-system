package dto.patient;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Schema(description = "Response dto for viewing a patient")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class PatientViewResponseDto {

    @Schema(description = "Unique identifier of the patient", example = "1")
    private Long id;

    @Schema(description = "First name of the patient", example = "John")
    private String firstName;

    @Schema(description = "Last name of the patient", example = "Doe")
    private String lastName;

    @Schema(description = "Telephone number of the patient", example = "123456789")
    private String telephoneNumber;

    @Schema(description = "Insurance company of the patient", example = "ČPZP")
    private String insuranceCompany;

    @Schema(description = "Birth number of the patient", example = "120990/9809")
    private String birthNumber;

    @Schema(description = "Birth date of the patient", example = "1990-09-12")
    private LocalDate birthDate;
}
