package dto.patient;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Schema(description = "Request for registering unregistered patient to the system")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class RegisterPatientRequestDto {

    @Schema(description = "Id of the patient to register", example = "1")
    private Long patientId;

    @Schema(description = "Username for the patient", example = "panbrambora")
    private String username;

}
