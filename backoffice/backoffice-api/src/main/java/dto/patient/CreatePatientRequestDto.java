package dto.patient;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Schema(description = "Request dto for creating a patient")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class CreatePatientRequestDto {

    @Schema(description = "First name of the patient", example = "John")
    @Size(min = 2, max = 255)
    @NotNull
    private String firstName;

    @Schema(description = "Last name of the patient", example = "Doe")
    @Size(min = 2, max = 255)
    @NotNull
    private String lastName;

    @Schema(description = "Telephone number of the patient", example = "123456789")
    @Size(min = 9, max = 12)
    @NotNull
    private String telephoneNumber;

    @Schema(description = "Insurance company of the patient", example = "ČPZP")
    @Size(min = 2, max = 255)
    private String insuranceCompany;

    @Schema(description = "Birth number of the patient", example = "120990/9809")
    @Size(min = 5, max = 12)
    @NotNull
    private String birthNumber;

    @Schema(description = "Birth date of the patient", example = "1990-09-12")
    @NotNull
    private LocalDate birthDate;
}
