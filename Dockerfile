# Build stage for all microservices
FROM maven:3-eclipse-temurin-21-alpine AS build
WORKDIR /app
COPY pom.xml .
COPY users-and-groups/ users-and-groups/
COPY pharmportal/ pharmportal/
COPY backoffice/ backoffice/
RUN mvn clean package

# Production image for users-and-groups
FROM eclipse-temurin:21-jre-alpine AS users-and-groups
WORKDIR /app
COPY --from=build /app/users-and-groups/user-management-service-impl/target/*.jar /app/users-and-groups.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "users-and-groups.jar"]

# Production image for pharmportal
FROM eclipse-temurin:21-jre-alpine AS pharmportal
WORKDIR /app
COPY --from=build /app/pharmportal/pharmportal-impl/target/*.jar /app/pharmportal.jar
EXPOSE 8081
ENTRYPOINT ["java", "-jar", "pharmportal.jar"]

# Production image for backoffice
FROM eclipse-temurin:21-jre-alpine AS backoffice
WORKDIR /app
COPY --from=build /app/backoffice/backoffice-impl/target/*.jar /app/backoffice.jar
EXPOSE 8082
ENTRYPOINT ["java", "-jar", "backoffice.jar"]
