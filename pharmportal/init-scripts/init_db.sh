#!/bin/bash

user_exists() {
  psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" -tAc "SELECT 1 FROM pg_roles WHERE rolname='$1'" | grep -q 1
}

database_exists() {
  psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" -tAc "SELECT 1 FROM pg_database WHERE datname='$1'" | grep -q 1
}

# Create the user if it doesn't exist
if ! user_exists "$PHARMPORTAL_DATABASE_USER"; then
  echo "Creating database user '$PHARMPORTAL_DATABASE_USER' since it does not exist."
  psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
      CREATE USER "$PHARMPORTAL_DATABASE_USER" WITH PASSWORD '$PHARMPORTAL_DATABASE_PASSWORD';
EOSQL
else
  echo "User '$PHARMPORTAL_DATABASE_USER' already exists, skipping creation."
fi

# Create the database if it doesn't exist
if ! database_exists "$PHARMPORTAL_DATABASE_NAME"; then
  echo "Creating database $PHARMPORTAL_DATABASE_NAME since it does not exist."
  psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
      CREATE DATABASE "$PHARMPORTAL_DATABASE_NAME";
      GRANT ALL PRIVILEGES ON DATABASE "$PHARMPORTAL_DATABASE_NAME" TO "$PHARMPORTAL_DATABASE_USER";
EOSQL
else
  echo "Database '$PHARMPORTAL_DATABASE_NAME' already exists, skipping creation."
fi
