package cz.muni.fi.pa165.medystem.pharmportalapi.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Schema(description = "Data Transfer Object for calculating the costs of a patient.")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class CalculatePriceResponseDto {
  @Schema(description = "The sum of the costs of all prescriptions", example = "100.0")
  private double price;

  @Override
  public String toString() {
    return "CalculatePriceResponseDto{" +
        ", cost='" + price + '\'' +
        '}';
  }
}