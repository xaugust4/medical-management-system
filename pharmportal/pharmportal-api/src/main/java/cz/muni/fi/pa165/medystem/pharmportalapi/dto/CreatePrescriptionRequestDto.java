package cz.muni.fi.pa165.medystem.pharmportalapi.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Schema(description = "Data Transfer Object for creating a prescription.")
public class CreatePrescriptionRequestDto {

    @Schema(description = "ID of the patient", example = "1")
    private Long patientId;
    @Schema(description = "ID of the prescribing doctor", example = "1")
    private Long doctorId;
    @Schema(description = "Name of the medication", example = "Paralen")
    private String medicationName;
    @Schema(description = "Amount of medication in mg", example = "500")
    private double mgAmount;
    @Schema(description = "Price of the medication", example = "100")
    private double price;
    @Schema(description = "Frequency of taking the medication", example = "3x per day")
    private String frequency;
    @Schema(description = "Number of days the medication should be taken", example = "7")
    private int days;
    @Schema(description = "Additional note for the prescription", example = "Take with food")
    private String note;
    @Schema(description = "Expiration date of the prescription", example = "2022-12-31")
    private LocalDate expirationDate;

    @Override
    public String toString() {
        return "CreatePrescriptionDto{" +
                "patientId=" + patientId +
                ", doctorId=" + doctorId +
                ", medicationName='" + medicationName + '\'' +
                ", mgAmount=" + mgAmount +
                ", frequency='" + frequency + '\'' +
                ", days=" + days +
                ", days=" + days +
                ", note='" + note +
                ", expirationDate=" + expirationDate + '\'' +
                '}';
    }
}
