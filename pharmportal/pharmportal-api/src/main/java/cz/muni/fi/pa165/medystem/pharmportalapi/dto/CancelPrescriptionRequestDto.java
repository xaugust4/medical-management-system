package cz.muni.fi.pa165.medystem.pharmportalapi.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Schema(description = "Data Transfer Object for cancelling a prescription.")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class CancelPrescriptionRequestDto {

    @Schema(description = "A note for the cancelled prescription", example = "Accidentaly prescribed wrong medication")
    private String note;

    @Override
    public String toString() {
        return "InvalidatePrescriptionDto{" +
                ", note='" + note + '\'' +
                '}';
    }
}
