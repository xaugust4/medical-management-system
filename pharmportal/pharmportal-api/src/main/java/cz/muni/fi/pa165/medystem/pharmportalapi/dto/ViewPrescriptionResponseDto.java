package cz.muni.fi.pa165.medystem.pharmportalapi.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Schema(description = "Data Transfer Object for viewing a prescription.")
public class ViewPrescriptionResponseDto {

    @Schema(description = "ID of the prescription", example = "1")
    private Long id;
    @Schema(description = "ID of the patient", example = "1")
    private Long patientId;
    @Schema(description = "ID of the prescribing doctor", example = "1")
    private Long doctorId;
    @Schema(description = "Name of the medication", example = "Paralen")
    private String medicationName;
    @Schema(description = "Amount of medication in mg", example = "500")
    private double mgAmount;
    @Schema(description = "Frequency of taking the medication", example = "3x per day")
    private String frequency;
    @Schema(description = "Date until the medication should be taken", example = "2022-12-31")
    private LocalDate prescribedUntil;
    @Schema(description = "Expiration date of the prescription", example = "2022-12-31")
    private LocalDate expirationDate;
    @Schema(description = "Additional note for the prescription", example = "Take with food")
    private String note;
    @Schema(description = "Price of the prescription", example = "100.0")
    private double price;
    @Schema(description = "Date of creation of the prescription", example = "2022-12-31")
    private LocalDate createdAt;
    @Schema(description = "Status of the prescription", example = "CREATED")
    private String status;

    @Override
    public String toString() {
        return "ViewPrescriptionDto{" +
                "id=" + id +
                ", patientId=" + patientId +
                ", doctorId=" + doctorId +
                ", medicationName='" + medicationName + '\'' +
                ", mgAmount=" + mgAmount +
                ", frequency='" + frequency + '\'' +
                ", prescribedUntil=" + prescribedUntil +
                ", expirationDate=" + expirationDate +
                ", note='" + note + '\'' +
                ", createdAt=" + createdAt +
                ", status='" + status +
                '}';
    }


}
