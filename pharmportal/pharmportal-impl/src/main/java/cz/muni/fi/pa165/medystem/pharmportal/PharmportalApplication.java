package cz.muni.fi.pa165.medystem.pharmportal;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@OpenAPIDefinition(info = @Info(title = "Pharmportal API", version = "v1", description = "API for managing prescriptions"))
public class PharmportalApplication {

    public static void main(String[] args) {
        SpringApplication.run(PharmportalApplication.class, args);
    }

}
