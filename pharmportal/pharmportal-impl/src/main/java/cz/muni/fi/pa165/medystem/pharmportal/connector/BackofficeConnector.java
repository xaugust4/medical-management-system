package cz.muni.fi.pa165.medystem.pharmportal.connector;

import dto.doctor.DoctorViewResponseDto;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

@Component
public class BackofficeConnector {

    WebClient webClient = WebClient.create("http://backoffice:8081/api/backoffice");

    public DoctorViewResponseDto getDoctorById(Long id) {
        return webClient.get()
                .uri("/doctors/{id}", id)
                .retrieve()
                .bodyToMono(DoctorViewResponseDto.class)
                .block();
    }
}
