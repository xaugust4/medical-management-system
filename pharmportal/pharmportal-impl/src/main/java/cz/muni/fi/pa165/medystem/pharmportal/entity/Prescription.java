package cz.muni.fi.pa165.medystem.pharmportal.entity;

import cz.muni.fi.pa165.medystem.pharmportal.entity.enums.PrescriptionStatus;
import cz.muni.fi.pa165.medystem.pharmportal.exception.PrescriptionAlreadyDispensedException;
import cz.muni.fi.pa165.medystem.pharmportal.exception.PrescriptionExpiredException;
import cz.muni.fi.pa165.medystem.pharmportal.handler.ApiError;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Index;
import jakarta.persistence.PrePersist;
import jakarta.persistence.Table;
import java.time.LocalDate;
import java.util.Objects;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Entity
@Table(name = "prescriptions", indexes = @Index(columnList = "patient_id"))
@NoArgsConstructor
@Getter
@Setter
public class Prescription extends AbstractEntity<Long> {

  @Column(name = "medication_name", nullable = false)
  private String medicationName;

  @Column(name = "mg_amount", nullable = false)
  private double mgAmount;

  @Column(name = "price", nullable = false)
  private double price;

  @Column(name = "frequency", nullable = false)
  private String frequency;

  @Column(name = "prescribed_until")
  private LocalDate prescribedUntil;

  @Column(name = "expiration_date", nullable = false)
  private LocalDate expirationDate;

  @Column(name = "note")
  private String note;

  @Column(name = "patient_id", nullable = false)
  private Long patientId;

  @Column(name = "doctor_id", nullable = false)
  private Long doctorId;

  @Column(name = "created_at", nullable = false)
  private LocalDate createdAt;

  @Column(name = "status", nullable = false)
  @Enumerated(EnumType.STRING)
  private PrescriptionStatus status = PrescriptionStatus.CREATED;

  public Prescription(String medicationName, double mgAmount, String frequency, LocalDate prescribedUntil, LocalDate expirationDate, String note, Long patientId, Long doctorId, double price) {
    this.medicationName = medicationName;
    this.mgAmount = mgAmount;
    this.frequency = frequency;
    this.prescribedUntil = prescribedUntil;
    this.expirationDate = expirationDate;
    this.note = note;
    this.patientId = patientId;
    this.doctorId = doctorId;
    this.price = price;
    this.status = PrescriptionStatus.CREATED;

  }

  @PrePersist
  protected void onCreate() {
    createdAt = LocalDate.now();
  }


  private void setStatus(PrescriptionStatus status) {
    this.status = status;
  }

  public void cancel() {
    if (status == PrescriptionStatus.DISPENSED) {
      throw new PrescriptionAlreadyDispensedException(getId());
    }
    setStatus(PrescriptionStatus.CANCELLED);
  }

  public void dispense() {
    if (status == PrescriptionStatus.DISPENSED) {
      throw new PrescriptionAlreadyDispensedException(getId());
    }
    if (status == PrescriptionStatus.CANCELLED) {
      throw new PrescriptionAlreadyDispensedException(getId());
    }
    if (LocalDate.now().isAfter(expirationDate)) {
      throw new PrescriptionExpiredException(getId());
    }
    setStatus(PrescriptionStatus.DISPENSED);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    if (!super.equals(o)) {
      return false;
    }
    Prescription that = (Prescription) o;
    return Double.compare(that.mgAmount, mgAmount) == 0 &&
        Objects.equals(medicationName, that.medicationName) &&
        Objects.equals(frequency, that.frequency) &&
        Objects.equals(prescribedUntil, that.prescribedUntil) &&
        Objects.equals(expirationDate, that.expirationDate) &&
        Objects.equals(note, that.note) &&
        Objects.equals(patientId, that.patientId) &&
        Objects.equals(doctorId, that.doctorId) &&
        Objects.equals(createdAt, that.createdAt) &&
        price == that.price &&
        status == that.status;

  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), medicationName, mgAmount, frequency, prescribedUntil, expirationDate, note, patientId, doctorId, createdAt, status, price);
  }
}
