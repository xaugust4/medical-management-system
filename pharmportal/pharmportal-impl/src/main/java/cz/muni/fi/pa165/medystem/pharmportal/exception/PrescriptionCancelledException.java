package cz.muni.fi.pa165.medystem.pharmportal.exception;

public class PrescriptionCancelledException extends RuntimeException {
    public PrescriptionCancelledException(Long id) {
        super("Prescription with id " + id + " has been cancelled.");
    }
}
