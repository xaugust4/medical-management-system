package cz.muni.fi.pa165.medystem.pharmportal.service;

import cz.muni.fi.pa165.medystem.pharmportal.entity.Prescription;
import cz.muni.fi.pa165.medystem.pharmportal.entity.enums.PrescriptionStatus;
import cz.muni.fi.pa165.medystem.pharmportal.exception.PrescriptionNotFoundException;
import cz.muni.fi.pa165.medystem.pharmportal.repository.PrescriptionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PrescriptionService {

    private final PrescriptionRepository prescriptionRepository;

    @Autowired
    public PrescriptionService(PrescriptionRepository prescriptionRepository) {
        this.prescriptionRepository = prescriptionRepository;
    }

    @Transactional
    public Prescription createPrescription(Prescription prescription) {
        return prescriptionRepository.save(prescription);
    }

    @Transactional(readOnly = true)
    public Prescription findPrescriptionById(Long id) {
        return prescriptionRepository.findById(id)
                .orElseThrow(() -> new PrescriptionNotFoundException("Prescription not found with id: " + id));
    }

    @Transactional(readOnly = true)
    public Page<Prescription> listPrescriptionByPatientId(Long patientId, Pageable pageable) {
        return prescriptionRepository.findAllByPatientId(pageable, patientId);
    }

    @Transactional(readOnly = true)
    public Page<Prescription> listPrescriptionByPatientIdAndStatus(Long patientId, PrescriptionStatus status, Pageable pageable) {
        return prescriptionRepository.findAllByPatientIdAndStatus(pageable, patientId, status);
    }

    @Transactional(readOnly = true)
    public Page<Prescription> listPrescriptions(Pageable pageable) {
        return prescriptionRepository.findAll(pageable);
    }

    @Transactional
    public Prescription updatePrescription(Long prescriptionId, Prescription prescriptionDetails) {
        prescriptionRepository.findById(prescriptionId)
                .orElseThrow(() -> new PrescriptionNotFoundException("Prescription not found with id: " + prescriptionId));

        prescriptionDetails.setId(prescriptionId);
        return prescriptionRepository.save(prescriptionDetails);
    }

    @Transactional
    public Prescription cancelPrescription(Long prescriptionId, String note) {
        Prescription prescription = prescriptionRepository.findById(prescriptionId)
                .orElseThrow(() -> new PrescriptionNotFoundException("Prescription not found with id: " + prescriptionId));

        prescription.cancel();
        prescription.setNote(note);
        return prescriptionRepository.save(prescription);
    }

    @Transactional
    public Prescription dispensePrescription(Long prescriptionId) {
        Prescription prescription = prescriptionRepository.findById(prescriptionId)
                .orElseThrow(() -> new PrescriptionNotFoundException("Prescription not found with id: " + prescriptionId));
        prescription.dispense();
        return prescriptionRepository.save(prescription);
    }
}
