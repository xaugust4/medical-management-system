package cz.muni.fi.pa165.medystem.pharmportal.init.framework.data;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Random;

public class DataPicker {

    private static final String[] FIRST_NAMES = {
            "John", "Jane", "Michael", "Jessica", "David", "Sarah", "Robert", "Emily", "William", "Ashley"
    };

    private static final String[] LAST_NAMES = {
            "Smith", "Johnson", "Williams", "Jones", "Brown", "Davis", "Miller", "Wilson", "Moore", "Taylor"
    };

    private static final String[] INSURANCE_COMPANIES = {
            "VZP", "ZP", "ZPMV", "VZP", "ZP", "ZPMV", "VZP", "ZP", "ZPMV", "VZP"
    };

    private static final String[] SPECIALIZATIONS = {
            "Cardiology", "Dermatology", "Endocrinology", "Gastroenterology", "Hematology", "Infectious diseases", "Nephrology", "Neurology", "Oncology", "Pulmonology"
    };

    private static final String[] MEDICATION_NAMES = {
            "Aspirin", "Paracetamol", "Ibuprofen", "Diclofenac", "Naproxen", "Ketoprofen", "Indomethacin", "Meloxicam", "Celecoxib", "Piroxicam"
    };

    private static final String[] FREQUENCIES = {
            "Once a day", "Twice a day", "Three times a day", "Four times a day", "Once a week", "Once a month", "Once a year"
    };

    private static final String[] NOTES = {
            "Take with food", "Do not take with alcohol", "Do not take with milk", "Do not take with grapefruit", "Do not take with coffee", "Do not take with tea", "Do not take with water", "Do not take with juice", "Do not take with soda", "Do not take with energy drink"
    };


    public static class RandomGenerator {

        private static final Random random = new Random();

        public static LocalDate randomDateBetween(int startYear, int endYear) {
            return LocalDate.of(
                    random.nextInt(startYear, endYear),
                    random.nextInt(1, 13),
                    random.nextInt(1, 29)
            );
        }

        public static LocalDateTime randomDateTimeBetween(int startYear, int endYear) {
            return LocalDateTime.of(
                    random.nextInt(startYear, endYear),
                    random.nextInt(1, 13),
                    random.nextInt(1, 29),
                    random.nextInt(0, 24),
                    random.nextInt(0, 60),
                    random.nextInt(0, 60)
            );
        }


        public static int randomPrice() {
            return random.nextInt(100, 10000);
        }

        public static String randomMedicationName() {
            return MEDICATION_NAMES[random.nextInt(MEDICATION_NAMES.length)];
        }

        public static String randomFrequency() {
            return FREQUENCIES[random.nextInt(FREQUENCIES.length)];
        }

        public static String randomNote() {
            return NOTES[random.nextInt(NOTES.length)];
        }

        public static double randomMgAmount() {
            return random.nextDouble() * 1000;
        }
    }
}
