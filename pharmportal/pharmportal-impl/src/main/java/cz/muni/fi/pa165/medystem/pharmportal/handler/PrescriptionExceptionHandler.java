package cz.muni.fi.pa165.medystem.pharmportal.handler;

import cz.muni.fi.pa165.medystem.pharmportal.controller.PrescriptionRestController;
import cz.muni.fi.pa165.medystem.pharmportal.exception.PrescriptionAlreadyDispensedException;
import cz.muni.fi.pa165.medystem.pharmportal.exception.PrescriptionCancelledException;
import cz.muni.fi.pa165.medystem.pharmportal.exception.PrescriptionExpiredException;
import cz.muni.fi.pa165.medystem.pharmportal.exception.PrescriptionNotFoundException;
import jakarta.persistence.PersistenceException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice(assignableTypes = {PrescriptionRestController.class})
public class PrescriptionExceptionHandler extends GeneralExceptionHandler {

  @ExceptionHandler(value = PrescriptionNotFoundException.class)
  protected ResponseEntity<Object> handlePrescriptionNotFoundException(PrescriptionNotFoundException ex) {
    String error = "Prescription not found";
    return buildResponseEntity(new ApiError(HttpStatus.NOT_FOUND, error, ex));
  }

  @ExceptionHandler(value = PrescriptionExpiredException.class)
  protected ResponseEntity<Object> handlePrescriptionExpiredException(PrescriptionNotFoundException ex) {
    String error = "Prescription is expired";
    return buildResponseEntity(new ApiError(HttpStatus.BAD_REQUEST, error, ex));
  }

  @ExceptionHandler(value = PrescriptionAlreadyDispensedException.class)
  protected ResponseEntity<Object> handlePrescriptionAlreadyDispensedxception(PrescriptionAlreadyDispensedException ex) {
    String error = "Prescription has already been dispensed";
    return buildResponseEntity(new ApiError(HttpStatus.BAD_REQUEST, error, ex));
  }

  @ExceptionHandler(value = PrescriptionCancelledException.class)
  protected ResponseEntity<Object> handlePrescriptionCancelledException(PrescriptionCancelledException ex) {
    String error = "Prescription has been cancelled";
    return buildResponseEntity(new ApiError(HttpStatus.BAD_REQUEST, error, ex));
  }


  @ExceptionHandler(value = PersistenceException.class)
  protected ResponseEntity<Object> handlePersistenceException(PersistenceException ex) {
    if (ex.getMessage().contains("violates unique constraint")) {
      return buildResponseEntity(new ApiError(HttpStatus.CONFLICT, "Prescription with desired prescriptionname already exists", ex));
    }
    String error = "Error when persisting the prescription";
    return buildResponseEntity(new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, error, ex));
  }
}