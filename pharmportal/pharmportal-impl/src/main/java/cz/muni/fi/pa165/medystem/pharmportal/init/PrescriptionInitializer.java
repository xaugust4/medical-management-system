package cz.muni.fi.pa165.medystem.pharmportal.init;

import cz.muni.fi.pa165.medystem.pharmportal.entity.Prescription;
import cz.muni.fi.pa165.medystem.pharmportal.init.framework.data.DataPicker;
import cz.muni.fi.pa165.medystem.pharmportal.init.framework.entity.AbstractInitializer;
import cz.muni.fi.pa165.medystem.pharmportal.repository.PrescriptionRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.List;

@Getter
@Setter
@Component
@Order(1)
@ConditionalOnExpression("${init.prescription.enabled:false} || ${init.enabled-all:false}")
public class PrescriptionInitializer extends AbstractInitializer<Long, Prescription, PrescriptionRepository> {

    private static final int ORDER = 1;

    @Value("${init.prescription.doctor-ids:1000}")
    private long doctorIds;

    @Value("${init.prescription.patient-ids:1000}")
    private long patientIds;

    @Value("${init.prescription.size:1000}")
    private int seedSize;

    @Value("${init.prescription.start-id:1000}")
    private long startId;

    protected PrescriptionInitializer(PrescriptionRepository repository) {
        super(repository, ORDER);
    }

    @Override
    protected List<Prescription> initializeEntities() {
        List<Prescription> entities = new java.util.ArrayList<>();

        for (int i = 1; i <= seedSize; i++) {
            entities.add(initializeEntity());
        }
        return entities;
    }

    private Prescription initializeEntity() {
        var prescription = new Prescription();
        prescription.setId(startId++);
        prescription.setCreatedAt(DataPicker.RandomGenerator.randomDateBetween(2022, 2023));
        prescription.setPrice(DataPicker.RandomGenerator.randomPrice());
        prescription.setExpirationDate(DataPicker.RandomGenerator.randomDateBetween(2023, 2024));
        prescription.setNote(DataPicker.RandomGenerator.randomNote());
        prescription.setFrequency(DataPicker.RandomGenerator.randomFrequency());
        prescription.setMedicationName(DataPicker.RandomGenerator.randomMedicationName());
        prescription.setMgAmount(DataPicker.RandomGenerator.randomMgAmount());
        prescription.setPrescribedUntil(DataPicker.RandomGenerator.randomDateBetween(2023, 2024));
        prescription.setDoctorId(doctorIds++);
        prescription.setPatientId(patientIds++);

        return prescription;
    }
}
