package cz.muni.fi.pa165.medystem.pharmportal.exception;

import jakarta.persistence.PersistenceException;

public class PrescriptionNotFoundException extends PersistenceException {

  public PrescriptionNotFoundException(String message) {
    super(message);
  }

  public PrescriptionNotFoundException(String message, Exception cause) {
    super(message, cause);
  }
}
