package cz.muni.fi.pa165.medystem.pharmportal.connector;

import cz.muni.fi.pa165.medystem.usermanagement.dto.UserViewResponseDto;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Component
public class UsersConnector {

    WebClient webClient = WebClient.create("http://localhost:8080/api/users");

    public UserViewResponseDto getUserById(Long id) {
        return webClient.get()
                .uri("/{id}", id)
                .retrieve()
                .bodyToMono(UserViewResponseDto.class)
                .block();
    }

    public UserViewResponseDto getUserByUsername(String username) {
        Mono<UserViewResponseDto> result = webClient
                .get()
                .uri("/username/" + username)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(UserViewResponseDto.class);

        return result.blockOptional()
                .orElseThrow(() -> new IllegalArgumentException("User not found"));
    }
}
