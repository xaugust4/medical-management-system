package cz.muni.fi.pa165.medystem.pharmportal.exception;

public class PrescriptionExpiredException extends RuntimeException {
    public PrescriptionExpiredException(Long id) {
        super("Prescription with id " + id + " is expired.");
    }
}
