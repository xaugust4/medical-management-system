package cz.muni.fi.pa165.medystem.pharmportal.entity.enums;

public enum PrescriptionStatus {
    CREATED,
    DISPENSED,
    CANCELLED,
}
