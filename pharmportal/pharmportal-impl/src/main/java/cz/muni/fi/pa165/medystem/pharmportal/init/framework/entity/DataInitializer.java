package cz.muni.fi.pa165.medystem.pharmportal.init.framework.entity;

public interface DataInitializer {

    void initialize();

    int getOrder();

}
