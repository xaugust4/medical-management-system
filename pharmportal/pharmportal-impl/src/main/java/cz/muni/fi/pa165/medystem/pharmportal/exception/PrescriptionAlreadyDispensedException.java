package cz.muni.fi.pa165.medystem.pharmportal.exception;

import jakarta.persistence.PersistenceException;

public class PrescriptionAlreadyDispensedException extends RuntimeException {
    public PrescriptionAlreadyDispensedException(Long id) {
        super("Prescription with id " + id + " has already been dispensed.");
    }
}
