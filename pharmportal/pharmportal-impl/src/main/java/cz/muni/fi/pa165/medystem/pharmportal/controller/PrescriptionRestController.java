package cz.muni.fi.pa165.medystem.pharmportal.controller;

import cz.muni.fi.pa165.medystem.pharmportal.facade.PrescriptionFacade;
import cz.muni.fi.pa165.medystem.pharmportalapi.dto.CalculatePriceResponseDto;
import cz.muni.fi.pa165.medystem.pharmportalapi.dto.CancelPrescriptionRequestDto;
import cz.muni.fi.pa165.medystem.pharmportalapi.dto.CreatePrescriptionRequestDto;
import cz.muni.fi.pa165.medystem.pharmportalapi.dto.ViewPrescriptionResponseDto;
import cz.muni.fi.pa165.medystem.usermanagement.dto.UserViewResponseDto;
import dto.doctor.DoctorViewResponseDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Max;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/pharmportal/prescriptions")
@Tag(name = "Prescription", description = "Operations related to Prescriptions")
public class PrescriptionRestController {

    private final PrescriptionFacade prescriptionFacade;

    @Autowired
    public PrescriptionRestController(PrescriptionFacade prescriptionFacade) {
        this.prescriptionFacade = prescriptionFacade;
    }

    @Operation(summary = "Create a new prescription", description = "Creates a new prescription with given params")
    @ApiResponse(responseCode = "201", description = "Prescription created",
            content = @Content(mediaType = "application/json",
                    schema = @Schema(implementation = ViewPrescriptionResponseDto.class)))
    @ApiResponse(responseCode = "500", description = "Internal server error occured", content = @Content(schema = @Schema(hidden = true)))
    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    public ViewPrescriptionResponseDto createPrescription(@Valid @RequestBody CreatePrescriptionRequestDto dtoIn) {
        return prescriptionFacade.createPrescription(dtoIn);
    }

    @Operation(summary = "Get prescription by ID", description = "Returns a prescription by id")
    @ApiResponse(responseCode = "200", description = "Prescription found",
            content = @Content(mediaType = "application/json",
                    schema = @Schema(implementation = ViewPrescriptionResponseDto.class)))
    @ApiResponse(responseCode = "500", description = "Internal server error occured", content = @Content(schema = @Schema(hidden = true)))
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ViewPrescriptionResponseDto getPrescriptionById(@PathVariable Long id) {
        return prescriptionFacade.findPrescriptionById(id);
    }

    @Operation(summary = "List all prescriptions in the system", description = "Returns all prescriptions in the system")
    @ApiResponse(responseCode = "200", description = "Prescriptions found",
            content = @Content(mediaType = "application/json",
                    schema = @Schema(implementation = Page.class)))
    @ApiResponse(responseCode = "500", description = "Internal server error occured", content = @Content(schema = @Schema(hidden = true)))
    @GetMapping(value = "/all", params = {"page", "size"})
    @ResponseStatus(HttpStatus.OK)
    public Page<ViewPrescriptionResponseDto> getAllPrescriptions(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "10") @Max(value = 50) int size,
            @RequestParam(value = "sort", defaultValue = "id, asc", required = false) String[] sortingParams
    ) {
        String field = sortingParams[0];
        String sortingDirection = sortingParams[1];
        Sort.Direction direction = sortingDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC;
        Pageable pageObj = PageRequest.of(page, size, Sort.by(direction, field));

        return prescriptionFacade.listAllPrescriptions(pageObj);
    }

    @Operation(summary = "List patient's prescription", description = "Returns a list of prescriptions based on patient ID")
    @ApiResponse(responseCode = "200", description = "Prescriptions found",
            content = @Content(mediaType = "application/json",
                    schema = @Schema(implementation = ViewPrescriptionResponseDto.class)))
    @ApiResponse(responseCode = "500", description = "Internal server error occured", content = @Content(schema = @Schema(hidden = true)))
    @ApiResponse(responseCode = "404", description = "Patient not found", content = @Content(schema = @Schema(hidden = true)))
    @GetMapping(value = "/patient/{patientId}/prescriptions", params = {"page", "size"})
    @ResponseStatus(HttpStatus.OK)
    public Page<ViewPrescriptionResponseDto> getPatientPrescriptions(
            @PathVariable Long patientId,
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "10") @Max(value = 50) int size,
            @RequestParam(value = "sort", defaultValue = "id, asc", required = false) String[] sortingParams
    ) {
        String field = sortingParams[0];
        String sortingDirection = sortingParams[1];
        Sort.Direction direction = sortingDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC;
        Pageable pageObj = PageRequest.of(page, size, Sort.by(direction, field));
        return prescriptionFacade.listPrescriptionsByPatientId(patientId, pageObj);
    }

    @Operation(summary = "List patient's active medication", description = "Returns a list of medications currently prescribed to the patient")
    @ApiResponse(responseCode = "200", description = "Prescriptions found",
            content = @Content(mediaType = "application/json",
                    schema = @Schema(implementation = ViewPrescriptionResponseDto.class)))
    @ApiResponse(responseCode = "500", description = "Internal server error occured", content = @Content(schema = @Schema(hidden = true)))
    @ApiResponse(responseCode = "404", description = "Patient not found", content = @Content(schema = @Schema(hidden = true)))
    @GetMapping(value = "/patient/{patientId}/activeMedication", params = {"page", "size"})
    @ResponseStatus(HttpStatus.OK)
    public Page<ViewPrescriptionResponseDto> getPatientActiveMedication(
            @PathVariable Long patientId,
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "10") @Max(value = 50) int size,
            @RequestParam(value = "sort", defaultValue = "id, asc", required = false) String[] sortingParams) {
        String field = sortingParams[0];
        String sortingDirection = sortingParams[1];
        Sort.Direction direction = sortingDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC;
        Pageable pageObj = PageRequest.of(page, size, Sort.by(direction, field));
        return prescriptionFacade.listActivePrescriptionsByPatientId(patientId, pageObj);
    }


    @Operation(summary = "List unresolved prescriptions", description = "Returns a list of prescriptions based on patient ID that have not been dispensed")
    @ApiResponse(responseCode = "200", description = "Prescriptions found",
            content = @Content(mediaType = "application/json",
                    schema = @Schema(implementation = ViewPrescriptionResponseDto.class)))
    @ApiResponse(responseCode = "500", description = "Internal server error occured", content = @Content(schema = @Schema(hidden = true)))
    @ApiResponse(responseCode = "404", description = "Patient not found", content = @Content(schema = @Schema(hidden = true)))
    @GetMapping(value = "/patient/{patientId}/unresolvedPrescriptions", params = {"page", "size"})
    @ResponseStatus(HttpStatus.OK)
    public Page<ViewPrescriptionResponseDto> getPatientUnresolvedPrescriptions(
            @PathVariable Long patientId,
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "10") @Max(value = 50) int size,
            @RequestParam(value = "sort", defaultValue = "id, asc", required = false) String[] sortingParams) {
        String field = sortingParams[0];
        String sortingDirection = sortingParams[1];
        Sort.Direction direction = sortingDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC;
        Pageable pageObj = PageRequest.of(page, size, Sort.by(direction, field));
        return prescriptionFacade.listUnresolvedPatientPrescriptions(patientId, pageObj);
    }

    @Operation(summary = "Cancel prescription", description = "Cancels a prescription by ID with a note. Aleready dispensed prescriptions cannot be cancelled.")
    @ApiResponse(responseCode = "200", description = "Prescription cancelled",
            content = @Content(mediaType = "application/json",
                    schema = @Schema(implementation = ViewPrescriptionResponseDto.class)))
    @ApiResponse(responseCode = "500", description = "Internal server error occured", content = @Content(schema = @Schema(hidden = true)))
    @ApiResponse(responseCode = "404", description = "Prescription not found", content = @Content(schema = @Schema(hidden = true)))
    @ApiResponse(responseCode = "400", description = "Attempted to cancel already dispensed medication", content = @Content(schema = @Schema(hidden = true)))
    @PatchMapping("/{id}/cancel")
    @ResponseStatus(HttpStatus.OK)
    public ViewPrescriptionResponseDto cancelPrescription(@PathVariable Long id,
                                                          @Valid @RequestBody CancelPrescriptionRequestDto dtoIn) {
        return prescriptionFacade.cancelPrescription(id, dtoIn);
    }

    @Operation(summary = "Dispense prescription", description = "Changes the status of a prescription to dispensed by ID. Cancelled or already dispensed prescriptions cannot be dispensed.")
    @ApiResponse(responseCode = "200", description = "Prescriptions found",
            content = @Content(mediaType = "application/json",
                    schema = @Schema(implementation = ViewPrescriptionResponseDto.class)))
    @ApiResponse(responseCode = "500", description = "Internal server error occured", content = @Content(schema = @Schema(hidden = true)))
    @ApiResponse(responseCode = "404", description = "Prescription not found", content = @Content(schema = @Schema(hidden = true)))
    @ApiResponse(responseCode = "400", description = "Attempting to dispense expired, cancelled or already dispensed medication", content = @Content(schema = @Schema(hidden = true)))
    @PatchMapping("/{id}/dispense")
    @ResponseStatus(HttpStatus.OK)
    public ViewPrescriptionResponseDto dispensePrescription(@PathVariable Long id) {
        return prescriptionFacade.dispensePrescription(id);
    }


    @Operation(summary = "Get prescribing doctor", description = "Returns information about the prescribing doctor.")
    @ApiResponse(responseCode = "200", description = "Doctor found",
            content = @Content(mediaType = "application/json",
                    schema = @Schema(implementation = DoctorViewResponseDto.class)))
    @ApiResponse(responseCode = "500", description = "Internal server error occured", content = @Content(schema = @Schema(hidden = true)))
    @ApiResponse(responseCode = "404", description = "Doctor not found", content = @Content(schema = @Schema(hidden = true)))
    @PatchMapping("/{id}/doctor")
    @ResponseStatus(HttpStatus.OK)
    public UserViewResponseDto getPrescribingDoctor(@PathVariable Long id) {
        return prescriptionFacade.getPrescribingDoctor(id);
    }


    @Operation(summary = "Calculates the total cost of the patient", description = "Returns the total costs of the patients medication.")
    @ApiResponse(responseCode = "200", description = "Cost calculated",
            content = @Content(mediaType = "application/json",
                    schema = @Schema(implementation = CalculatePriceResponseDto.class)))
    @ApiResponse(responseCode = "500", description = "Internal server error occured", content = @Content(schema = @Schema(hidden = true)))
    @ApiResponse(responseCode = "404", description = "Patient not found", content = @Content(schema = @Schema(hidden = true)))
    @GetMapping("/{patientId}/costs")
    @ResponseStatus(HttpStatus.OK)
    public CalculatePriceResponseDto calculatePatientCost(@PathVariable Long patientId) {
        return prescriptionFacade.calculatePatientCosts(patientId);
    }
}
