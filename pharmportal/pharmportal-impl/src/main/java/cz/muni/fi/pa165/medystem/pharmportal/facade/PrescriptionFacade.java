package cz.muni.fi.pa165.medystem.pharmportal.facade;

import cz.muni.fi.pa165.medystem.pharmportal.connector.BackofficeConnector;
import cz.muni.fi.pa165.medystem.pharmportal.connector.UsersConnector;
import cz.muni.fi.pa165.medystem.pharmportal.entity.Prescription;
import cz.muni.fi.pa165.medystem.pharmportal.entity.enums.PrescriptionStatus;
import cz.muni.fi.pa165.medystem.pharmportal.mapper.PrescriptionMapper;
import cz.muni.fi.pa165.medystem.pharmportal.service.PrescriptionService;
import cz.muni.fi.pa165.medystem.pharmportalapi.dto.CalculatePriceResponseDto;
import cz.muni.fi.pa165.medystem.pharmportalapi.dto.CancelPrescriptionRequestDto;
import cz.muni.fi.pa165.medystem.pharmportalapi.dto.CreatePrescriptionRequestDto;
import cz.muni.fi.pa165.medystem.pharmportalapi.dto.ViewPrescriptionResponseDto;
import cz.muni.fi.pa165.medystem.usermanagement.dto.UserViewResponseDto;
import dto.doctor.DoctorViewResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Service
public class PrescriptionFacade {
    private final PrescriptionService prescriptionService;
    private final PrescriptionMapper prescriptionMapper;
    private final BackofficeConnector backofficeConnector;
    private final UsersConnector usersConnector;

    @Autowired
    public PrescriptionFacade(PrescriptionService prescriptionService, PrescriptionMapper prescriptionMapper, BackofficeConnector backofficeConnector, UsersConnector usersConnector) {
        this.prescriptionService = prescriptionService;
        this.prescriptionMapper = prescriptionMapper;
        this.backofficeConnector = backofficeConnector;
        this.usersConnector = usersConnector;
    }

    public ViewPrescriptionResponseDto createPrescription(CreatePrescriptionRequestDto dtoIn) {
        Prescription prescription = prescriptionMapper.prescriptionCreateDtoToPrescription(dtoIn);
        prescription.setPrescribedUntil(LocalDate.now().plusDays(dtoIn.getDays()));
        Prescription createdPrescription = prescriptionService.createPrescription(prescription);
        return prescriptionMapper.prescriptionToPrescriptionViewDto(createdPrescription);
    }

    public ViewPrescriptionResponseDto findPrescriptionById(Long id) {
        Prescription prescription = prescriptionService.findPrescriptionById(id);
        return prescriptionMapper.prescriptionToPrescriptionViewDto(prescription);
    }

    public Page<ViewPrescriptionResponseDto> listPrescriptionsByPatientId(Long id, Pageable pageable) {
        Page<Prescription> prescriptions = prescriptionService.listPrescriptionByPatientId(id, pageable);
        return prescriptions.map(prescriptionMapper::prescriptionToPrescriptionViewDto);
    }

    public Page<ViewPrescriptionResponseDto> listActivePrescriptionsByPatientId(Long id, Pageable pageable) {
        Page<Prescription> prescriptions = prescriptionService.listPrescriptionByPatientIdAndStatus(id, PrescriptionStatus.DISPENSED, pageable);
        List<ViewPrescriptionResponseDto> filteredPrescriptions = prescriptions
                .stream()
                .filter(prescription -> prescription.getPrescribedUntil().isAfter(LocalDate.now()))
                .map(prescriptionMapper::prescriptionToPrescriptionViewDto)
                .toList();

        return new PageImpl<>(filteredPrescriptions, pageable, filteredPrescriptions.size());
    }

    public Page<ViewPrescriptionResponseDto> listUnresolvedPatientPrescriptions(Long id, Pageable pageable) {
        Page<Prescription> prescriptions = prescriptionService.listPrescriptionByPatientIdAndStatus(id, PrescriptionStatus.CREATED, pageable);
        return prescriptions.map(prescriptionMapper::prescriptionToPrescriptionViewDto);
    }

    public Page<ViewPrescriptionResponseDto> listAllPrescriptions(Pageable pageable) {
        Page<Prescription> prescriptions = prescriptionService.listPrescriptions(pageable);
        return prescriptions.map(prescriptionMapper::prescriptionToPrescriptionViewDto);
    }


    public ViewPrescriptionResponseDto cancelPrescription(Long prescriptionId, CancelPrescriptionRequestDto dtoIn) {
        Prescription prescription = prescriptionService.cancelPrescription(prescriptionId, dtoIn.getNote());
        return prescriptionMapper.prescriptionToPrescriptionViewDto(prescription);
    }

    public ViewPrescriptionResponseDto dispensePrescription(Long prescriptionId) {
        Prescription updatedPrescription = prescriptionService.dispensePrescription(prescriptionId);
        return prescriptionMapper.prescriptionToPrescriptionViewDto(updatedPrescription);
    }

    public UserViewResponseDto getPrescribingDoctor(Long id) {
        Prescription prescription = prescriptionService.findPrescriptionById(id);
        Long doctorId = prescription.getDoctorId();
        DoctorViewResponseDto doctor = backofficeConnector.getDoctorById(doctorId);
        return usersConnector.getUserById(Long.valueOf(doctor.getUserId()));
    }

    public CalculatePriceResponseDto calculatePatientCosts(Long patientId) {
        Pageable pageable = PageRequest.of(0, 10);
        double totalCost = 0.0;
        Page<Prescription> page;

        do {
            page = prescriptionService.listPrescriptionByPatientId(patientId, pageable);
            double pageCost = page.getContent()
                    .stream()
                    .filter(prescription -> prescription.getStatus() == PrescriptionStatus.DISPENSED)
                    .mapToDouble(Prescription::getPrice).sum();
            totalCost += pageCost;
            pageable = page.nextPageable();
        } while (page.hasNext());

        return new CalculatePriceResponseDto(totalCost);
    }

}
