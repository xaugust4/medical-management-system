package cz.muni.fi.pa165.medystem.pharmportal.config;

import cz.muni.fi.pa165.medystem.pharmportal.connector.UsersConnector;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authorization.AuthorizationDecision;
import org.springframework.security.authorization.AuthorizationManager;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.access.intercept.RequestAuthorizationContext;

@Configuration
@EnableWebSecurity
public class SecurityConfig {

  @Autowired
  private UsersConnector userConnector;

  @Bean
  public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception {
    httpSecurity
        .csrf(AbstractHttpConfigurer::disable)
        .authorizeHttpRequests(authorizeRequests ->
            authorizeRequests
                .requestMatchers("/login/oauth2/code/muni").authenticated()
                .requestMatchers("/api/pharmportal/prescriptions/{id}/dispense").access(authorizationManager(Set.of("PHARMACIST")))
                .requestMatchers("/api/pharmportal/prescriptions/**").access(authorizationManager(Set.of("DOCTOR")))
                .requestMatchers("/management/**").permitAll()
                .anyRequest().authenticated()
        )
        .logout(logout ->
            logout
                .logoutUrl("/logout")
                .logoutSuccessUrl("/login")
        )
        .oauth2Login(Customizer.withDefaults())
        .anonymous(AbstractHttpConfigurer::disable);
    return httpSecurity.build();
  }

  public AuthorizationManager<RequestAuthorizationContext> authorizationManager(Set<String> roles) {
    return (authentication, context) -> new AuthorizationDecision(hasAnyRole((OAuth2AuthenticationToken) (authentication.get()), roles));
  }

  public boolean hasAnyRole(OAuth2AuthenticationToken token, Set<String> roles) {
    var userRoles = userConnector.getUserByUsername(token.getName()).getRoles();
    return userRoles.stream().anyMatch(roles::contains);
  }
}
