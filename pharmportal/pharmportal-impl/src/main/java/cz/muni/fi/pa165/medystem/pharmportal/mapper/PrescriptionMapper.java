package cz.muni.fi.pa165.medystem.pharmportal.mapper;

import cz.muni.fi.pa165.medystem.pharmportal.entity.Prescription;
import cz.muni.fi.pa165.medystem.pharmportalapi.dto.CreatePrescriptionRequestDto;
import cz.muni.fi.pa165.medystem.pharmportalapi.dto.ViewPrescriptionResponseDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public abstract class PrescriptionMapper {

  public abstract ViewPrescriptionResponseDto prescriptionToPrescriptionViewDto(Prescription prescription);

  @Mapping(target = "status", ignore = true)
  public abstract Prescription prescriptionCreateDtoToPrescription(CreatePrescriptionRequestDto dto);


}
