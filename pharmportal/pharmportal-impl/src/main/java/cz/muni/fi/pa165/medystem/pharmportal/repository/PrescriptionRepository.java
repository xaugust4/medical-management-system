package cz.muni.fi.pa165.medystem.pharmportal.repository;

import cz.muni.fi.pa165.medystem.pharmportal.entity.Prescription;
import cz.muni.fi.pa165.medystem.pharmportal.entity.enums.PrescriptionStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PrescriptionRepository extends JpaRepository<Prescription, Long> {

    Page<Prescription> findAllByPatientId(Pageable pageable, Long patientId);

    Page<Prescription> findAllByPatientIdAndStatus(Pageable pageable, Long patientId, PrescriptionStatus status);

}
