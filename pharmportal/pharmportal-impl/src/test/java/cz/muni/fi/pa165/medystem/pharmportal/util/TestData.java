package cz.muni.fi.pa165.medystem.pharmportal.util;

import cz.muni.fi.pa165.medystem.pharmportal.entity.Prescription;
import cz.muni.fi.pa165.medystem.pharmportalapi.dto.CalculatePriceResponseDto;
import cz.muni.fi.pa165.medystem.pharmportalapi.dto.CancelPrescriptionRequestDto;
import cz.muni.fi.pa165.medystem.pharmportalapi.dto.CreatePrescriptionRequestDto;
import cz.muni.fi.pa165.medystem.pharmportalapi.dto.ViewPrescriptionResponseDto;

import java.time.LocalDate;

public final class TestData {

    public static CalculatePriceResponseDto getCalculatePriceResponseDto() {
        CalculatePriceResponseDto dto = new CalculatePriceResponseDto();
        dto.setPrice(100.0);
        return dto;
    }

    public static CancelPrescriptionRequestDto getCancelPrescriptionRequestDto() {
        CancelPrescriptionRequestDto dto = new CancelPrescriptionRequestDto();
        dto.setNote("Accidentally prescribed wrong medication");
        return dto;
    }

    public static CreatePrescriptionRequestDto getCreatePrescriptionRequestDto() {
        CreatePrescriptionRequestDto dto = new CreatePrescriptionRequestDto();
        dto.setPatientId(1L);
        dto.setDoctorId(1L);
        dto.setMedicationName("Paralen");
        dto.setMgAmount(500);
        dto.setPrice(100);
        dto.setFrequency("3x per day");
        dto.setDays(7);
        dto.setNote("Take with food");
        dto.setExpirationDate(LocalDate.now().plusDays(7));
        return dto;
    }

    public static ViewPrescriptionResponseDto getViewPrescriptionResponseDto() {
        ViewPrescriptionResponseDto dto = new ViewPrescriptionResponseDto();
        dto.setId(1L);
        dto.setPatientId(1L);
        dto.setDoctorId(1L);
        dto.setMedicationName("Paralen");
        dto.setMgAmount(500);
        dto.setFrequency("3x per day");
        dto.setPrescribedUntil(LocalDate.now().plusDays(7));
        dto.setExpirationDate(LocalDate.now().plusDays(7));
        dto.setNote("Take with food");
        dto.setPrice(100.0);
        dto.setCreatedAt(LocalDate.now());
        dto.setStatus("CREATED");
        return dto;
    }

    public static Prescription getPrescription() {
        Prescription prescription = new Prescription();
        prescription.setMedicationName("Paralen");
        prescription.setMgAmount(500);
        prescription.setPrice(100.0);
        prescription.setFrequency("3x per day");
        prescription.setPrescribedUntil(LocalDate.now().plusDays(7));
        prescription.setExpirationDate(LocalDate.now().plusDays(7));
        prescription.setNote("Take with food");
        prescription.setPatientId(1L);
        prescription.setDoctorId(1L);
        prescription.setCreatedAt(LocalDate.now());
        return prescription;
    }
}
