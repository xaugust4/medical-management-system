package cz.muni.fi.pa165.medystem.pharmportal.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import cz.muni.fi.pa165.medystem.pharmportal.facade.PrescriptionFacade;
import cz.muni.fi.pa165.medystem.pharmportal.util.TestData;
import cz.muni.fi.pa165.medystem.pharmportalapi.dto.CalculatePriceResponseDto;
import cz.muni.fi.pa165.medystem.pharmportalapi.dto.CancelPrescriptionRequestDto;
import cz.muni.fi.pa165.medystem.pharmportalapi.dto.CreatePrescriptionRequestDto;
import cz.muni.fi.pa165.medystem.pharmportalapi.dto.ViewPrescriptionResponseDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;

@AutoConfigureMockMvc
@SpringBootTest
class PrescriptionRestControllerTest {

    private static final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private PrescriptionFacade prescriptionFacade;
    private ObjectMapper objectMapper;

    @BeforeEach
    public void setup() {
        objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
    }

    @Test
    void createPrescription_validPrescription_prescriptionSaved() throws Exception {
        CreatePrescriptionRequestDto requestDto = TestData.getCreatePrescriptionRequestDto();
        ViewPrescriptionResponseDto responseDto = TestData.getViewPrescriptionResponseDto();

        responseDto.setId(1L);

        when(prescriptionFacade.createPrescription(any())).thenReturn(responseDto);

        mockMvc.perform(post("/api/pharmportal/prescriptions")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(requestDto)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value(1L));
    }

    @Test
    void getPrescriptionById_validId_prescriptionReturned() throws Exception {
        ViewPrescriptionResponseDto responseDto = TestData.getViewPrescriptionResponseDto();

        when(prescriptionFacade.findPrescriptionById(responseDto.getId())).thenReturn(responseDto);

        mockMvc.perform(get("/api/pharmportal/prescriptions/{id}", responseDto.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(responseDto.getId()));
    }

    @Test
    void getAllPrescriptions_prescriptionsExist_prescriptionsReturned() throws Exception {
        Pageable pageable = PageRequest.of(0, 10, Sort.by(Sort.Direction.ASC, "id"));
        Page<ViewPrescriptionResponseDto> emptyPage = new PageImpl<>(Collections.emptyList(), pageable, 0);

        when(prescriptionFacade.listAllPrescriptions(pageable)).thenReturn(emptyPage);

        mockMvc.perform(get("/api/pharmportal/prescriptions/all")
                        .param("page", "0")
                        .param("size", "10"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content").isEmpty())
                .andExpect(jsonPath("$.pageable.pageNumber").value(0))
                .andExpect(jsonPath("$.pageable.pageSize").value(10))
                .andExpect(jsonPath("$.totalPages").value(0))
                .andExpect(jsonPath("$.totalElements").value(0))
                .andExpect(jsonPath("$.numberOfElements").value(0));

        verify(prescriptionFacade).listAllPrescriptions(pageable);
    }

    @Test
    void cancelPrescription_validRequest_prescriptionCancelled() throws Exception {
        CancelPrescriptionRequestDto requestDto = TestData.getCancelPrescriptionRequestDto();
        ViewPrescriptionResponseDto responseDto = TestData.getViewPrescriptionResponseDto();
        responseDto.setStatus("CANCELLED");

        when(prescriptionFacade.cancelPrescription(eq(responseDto.getId()), any())).thenReturn(responseDto);

        mockMvc.perform(patch("/api/pharmportal/prescriptions/{id}/cancel", responseDto.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(requestDto)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("CANCELLED"));
    }

    @Test
    void getPatientPrescriptions_validPatientId_prescriptionsReturned() throws Exception {
        Long patientId = 1L;
        ViewPrescriptionResponseDto responseDto = TestData.getViewPrescriptionResponseDto();
        List<ViewPrescriptionResponseDto> responseDtos = Collections.singletonList(responseDto);
        Page<ViewPrescriptionResponseDto> pageResponse = new PageImpl<>(responseDtos, PageRequest.of(0, 10), 1);

        when(prescriptionFacade.listPrescriptionsByPatientId(eq(patientId), any(Pageable.class))).thenReturn(pageResponse);

        mockMvc.perform(get("/api/pharmportal/prescriptions/patient/{patientId}/prescriptions", patientId)
                        .param("page", "0")
                        .param("size", "10")
                        .param("sort", "id,asc"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content[0].patientId").value(patientId));
    }

    @Test
    void getPatientActiveMedication_validPatientId_activeMedicationsReturned() throws Exception {
        Long patientId = 1L;
        ViewPrescriptionResponseDto responseDto = TestData.getViewPrescriptionResponseDto();
        List<ViewPrescriptionResponseDto> responseDtos = Collections.singletonList(responseDto);
        Page<ViewPrescriptionResponseDto> pageResponse = new PageImpl<>(responseDtos, PageRequest.of(0, 10), 1);

        when(prescriptionFacade.listActivePrescriptionsByPatientId(eq(patientId), any(Pageable.class))).thenReturn(pageResponse);

        mockMvc.perform(get("/api/pharmportal/prescriptions/patient/{patientId}/activeMedication", patientId)
                        .param("page", "0")
                        .param("size", "10")
                        .param("sort", "id,asc"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content[0].patientId").value(patientId));
    }

    @Test
    void getPatientUnresolvedPrescriptions_validPatientId_unresolvedPrescriptionsReturned() throws Exception {
        Long patientId = 1L;
        ViewPrescriptionResponseDto responseDto = TestData.getViewPrescriptionResponseDto();
        List<ViewPrescriptionResponseDto> responseDtos = Collections.singletonList(responseDto);
        Page<ViewPrescriptionResponseDto> pageResponse = new PageImpl<>(responseDtos, PageRequest.of(0, 10), 1);

        when(prescriptionFacade.listUnresolvedPatientPrescriptions(eq(patientId), any(Pageable.class))).thenReturn(pageResponse);

        mockMvc.perform(get("/api/pharmportal/prescriptions/patient/{patientId}/unresolvedPrescriptions", patientId)
                        .param("page", "0")
                        .param("size", "10")
                        .param("sort", "id,asc"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content[0].patientId").value(patientId));
    }

    @Test
    void dispensePrescription_validId_prescriptionDispensed() throws Exception {
        Long id = 1L;
        ViewPrescriptionResponseDto responseDto = TestData.getViewPrescriptionResponseDto();
        responseDto.setStatus("DISPENSED");

        when(prescriptionFacade.dispensePrescription(id)).thenReturn(responseDto);

        mockMvc.perform(patch("/api/pharmportal/prescriptions/{id}/dispense", id))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(id))
                .andExpect(jsonPath("$.status").value("DISPENSED"));
    }

    @Test
    void calculatePatientCost_validPatientId_costCalculated() throws Exception {
        Long patientId = 1L;
        CalculatePriceResponseDto responseDto = TestData.getCalculatePriceResponseDto();

        when(prescriptionFacade.calculatePatientCosts(patientId)).thenReturn(responseDto);

        mockMvc.perform(get("/api/pharmportal/prescriptions/{patientId}/costs", patientId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.price").value(responseDto.getPrice()));
    }
}

