package cz.muni.fi.pa165.medystem.pharmportal.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import cz.muni.fi.pa165.medystem.pharmportal.exception.PrescriptionAlreadyDispensedException;
import cz.muni.fi.pa165.medystem.pharmportal.exception.PrescriptionCancelledException;
import cz.muni.fi.pa165.medystem.pharmportal.exception.PrescriptionNotFoundException;
import cz.muni.fi.pa165.medystem.pharmportal.facade.PrescriptionFacade;
import cz.muni.fi.pa165.medystem.pharmportal.util.TestData;
import cz.muni.fi.pa165.medystem.pharmportalapi.dto.CancelPrescriptionRequestDto;
import cz.muni.fi.pa165.medystem.pharmportalapi.dto.CreatePrescriptionRequestDto;
import jakarta.persistence.PersistenceException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@SpringBootTest
public class PrescriptionRestControllerExceptionHandlerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PrescriptionFacade prescriptionFacade;

    private ObjectMapper objectMapper;

    @BeforeEach
    public void setupSuite() {
        objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
    }

    @Test
    void createPrescription_malformedJson_badRequest() throws Exception {
        mockMvc.perform(post("/api/pharmportal/prescriptions")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("invalid json"))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("Malformed JSON request"));
    }

    @Test
    void getPrescriptionById_notFound_prescriptionNotFound() throws Exception {
        String errorMessage = "Prescription with given ID does not exist";
        when(prescriptionFacade.findPrescriptionById(any(Long.class))).thenThrow(new PrescriptionNotFoundException(errorMessage));

        mockMvc.perform(get("/api/pharmportal/prescriptions/{id}", 1L))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message").value("Prescription not found"));
    }

    @Test
    void cancelPrescription_alreadyDispensed_badRequest() throws Exception {
        when(prescriptionFacade.cancelPrescription(any(Long.class), any(CancelPrescriptionRequestDto.class)))
                .thenThrow(new PrescriptionAlreadyDispensedException(1L));

        mockMvc.perform(patch("/api/pharmportal/prescriptions/{id}/cancel", 1L)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"note\":\"Accidentally prescribed wrong medication\"}"))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("Prescription has already been dispensed"));
    }

    @Test
    void dispensePrescription_cancelledPrescription_badRequest() throws Exception {
        when(prescriptionFacade.dispensePrescription(any(Long.class))).thenThrow(new PrescriptionCancelledException(1L));

        mockMvc.perform(patch("/api/pharmportal/prescriptions/{id}/dispense", 1L))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("Prescription has been cancelled"));
    }

    @Test
    void createPrescription_uniqueConstraintViolation_conflict() throws Exception {
        CreatePrescriptionRequestDto requestDto = TestData.getCreatePrescriptionRequestDto();
        when(prescriptionFacade.createPrescription(any(CreatePrescriptionRequestDto.class)))
                .thenThrow(new PersistenceException("violates unique constraint"));

        mockMvc.perform(post("/api/pharmportal/prescriptions")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(requestDto)))
                .andExpect(status().isConflict())
                .andExpect(jsonPath("$.message").value("Prescription with desired prescriptionname already exists"));
    }

}
