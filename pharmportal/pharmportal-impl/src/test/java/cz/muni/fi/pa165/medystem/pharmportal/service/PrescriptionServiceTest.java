package cz.muni.fi.pa165.medystem.pharmportal.service;

import cz.muni.fi.pa165.medystem.pharmportal.entity.Prescription;
import cz.muni.fi.pa165.medystem.pharmportal.entity.enums.PrescriptionStatus;
import cz.muni.fi.pa165.medystem.pharmportal.exception.PrescriptionNotFoundException;
import cz.muni.fi.pa165.medystem.pharmportal.repository.PrescriptionRepository;
import cz.muni.fi.pa165.medystem.pharmportal.util.TestData;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
class PrescriptionServiceTest {

    @Mock
    private PrescriptionRepository prescriptionRepository;
    @InjectMocks
    private PrescriptionService prescriptionService;

    @Test
    void createPrescription_validPrescription_prescriptionSaved() {
        Prescription testPrescription = TestData.getPrescription();
        when(prescriptionRepository.save(any(Prescription.class))).thenReturn(testPrescription);

        Prescription result = prescriptionService.createPrescription(testPrescription);

        assertThat(result).isEqualTo(testPrescription);
        verify(prescriptionRepository).save(testPrescription);
    }

    @Test
    void findPrescriptionById_validId_prescriptionFound() {
        Prescription testPrescription = TestData.getPrescription();
        when(prescriptionRepository.findById(anyLong())).thenReturn(Optional.of(testPrescription));

        Prescription result = prescriptionService.findPrescriptionById(1L);

        assertThat(result).isEqualTo(testPrescription);
        verify(prescriptionRepository).findById(1L);
    }

    @Test
    void findPrescriptionById_invalidId_throwsException() {
        when(prescriptionRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThrows(PrescriptionNotFoundException.class, () -> prescriptionService.findPrescriptionById(99L));

        verify(prescriptionRepository).findById(99L);
    }

    @Test
    void listPrescriptions_noArguments_prescriptionsListed() {
        Prescription prescription = TestData.getPrescription();
        Pageable pageable = PageRequest.of(0, 10, Sort.by(Sort.Direction.ASC, "id"));
        Page<Prescription> pageEntity = new PageImpl<>(List.of(prescription), pageable, 1);
        when(prescriptionRepository.findAll(pageable)).thenReturn(pageEntity);

        Page<Prescription> result = prescriptionService.listPrescriptions(pageable);

        assertThat(result).isEqualTo(pageEntity);
        verify(prescriptionRepository).findAll(pageable);
    }

    @Test
    void listPrescriptionByPatientId_validPatientId_prescriptionsFound() {
        Prescription prescription = TestData.getPrescription();
        Pageable pageable = PageRequest.of(0, 10, Sort.by(Sort.Direction.ASC, "id"));
        Page<Prescription> pageEntity = new PageImpl<>(List.of(prescription), pageable, 1);
        when(prescriptionRepository.findAllByPatientId(pageable, 1L)).thenReturn(pageEntity);

        Page<Prescription> result = prescriptionService.listPrescriptionByPatientId(1L, pageable);

        assertThat(result).isEqualTo(pageEntity);
        verify(prescriptionRepository).findAllByPatientId(pageable, 1L);
    }

    @Test
    void listPrescriptionByPatientIdAndStatus_validArguments_prescriptionsFound() {
        Prescription prescription = TestData.getPrescription();
        Pageable pageable = PageRequest.of(0, 10, Sort.by(Sort.Direction.ASC, "id"));
        Page<Prescription> pageEntity = new PageImpl<>(List.of(prescription), pageable, 1);
        when(prescriptionRepository.findAllByPatientIdAndStatus(pageable, 1L, PrescriptionStatus.CREATED)).thenReturn(pageEntity);

        Page<Prescription> result = prescriptionService.listPrescriptionByPatientIdAndStatus(1L, PrescriptionStatus.CREATED, pageable);

        assertThat(result).isEqualTo(pageEntity);
        verify(prescriptionRepository).findAllByPatientIdAndStatus(pageable, 1L, PrescriptionStatus.CREATED);
    }

    @Test
    void updatePrescription_validArguments_prescriptionUpdated() {
        Prescription originalPrescription = TestData.getPrescription();
        Prescription updatedDetails = TestData.getPrescription();
        updatedDetails.setNote("Updated note");
        when(prescriptionRepository.findById(anyLong())).thenReturn(Optional.of(originalPrescription));
        when(prescriptionRepository.save(any(Prescription.class))).thenReturn(updatedDetails);

        Prescription result = prescriptionService.updatePrescription(1L, updatedDetails);

        assertThat(result).isEqualTo(updatedDetails);
        verify(prescriptionRepository).findById(1L);
        verify(prescriptionRepository).save(updatedDetails);
    }

    @Test
    void updatePrescription_invalidPrescriptionId_throwsException() {
        Prescription updatedDetails = TestData.getPrescription();
        when(prescriptionRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThrows(PrescriptionNotFoundException.class, () -> prescriptionService.updatePrescription(99L, updatedDetails));

        verify(prescriptionRepository).findById(99L);
    }

    @Test
    void cancelPrescription_validArguments_prescriptionCanceled() {
        Prescription testPrescription = TestData.getPrescription();
        when(prescriptionRepository.findById(anyLong())).thenReturn(Optional.of(testPrescription));
        when(prescriptionRepository.save(any(Prescription.class))).thenReturn(testPrescription);

        Prescription result = prescriptionService.cancelPrescription(1L, "Cancellation note");

        assertThat(result).isNotNull();
        assertThat(result.getNote()).isEqualTo("Cancellation note");
        verify(prescriptionRepository).findById(1L);
        verify(prescriptionRepository).save(testPrescription);
    }

    @Test
    void cancelPrescription_invalidPrescriptionId_throwsException() {
        when(prescriptionRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThrows(PrescriptionNotFoundException.class, () -> prescriptionService.cancelPrescription(99L, "Cancellation note"));

        verify(prescriptionRepository).findById(99L);
    }

    @Test
    void dispensePrescription_validId_prescriptionDispensed() {
        Prescription testPrescription = TestData.getPrescription();
        when(prescriptionRepository.findById(anyLong())).thenReturn(Optional.of(testPrescription));
        when(prescriptionRepository.save(any(Prescription.class))).thenReturn(testPrescription);

        Prescription result = prescriptionService.dispensePrescription(1L);

        assertThat(result).isNotNull();
        verify(prescriptionRepository).findById(1L);
        verify(prescriptionRepository).save(testPrescription);
    }

    @Test
    void dispensePrescription_invalidPrescriptionId_throwsException() {
        when(prescriptionRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThrows(PrescriptionNotFoundException.class, () -> prescriptionService.dispensePrescription(99L));

        verify(prescriptionRepository).findById(99L);
    }
}