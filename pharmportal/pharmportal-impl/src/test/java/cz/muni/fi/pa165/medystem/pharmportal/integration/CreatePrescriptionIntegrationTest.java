package cz.muni.fi.pa165.medystem.pharmportal.integration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import cz.muni.fi.pa165.medystem.pharmportal.entity.Prescription;
import cz.muni.fi.pa165.medystem.pharmportal.repository.PrescriptionRepository;
import cz.muni.fi.pa165.medystem.pharmportal.util.TestData;
import cz.muni.fi.pa165.medystem.pharmportalapi.dto.CreatePrescriptionRequestDto;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
@AutoConfigureMockMvc
public class CreatePrescriptionIntegrationTest {

  private static ObjectMapper objectMapper = new ObjectMapper();
  @Autowired
  private MockMvc mockMvc;
  @Autowired
  private PrescriptionRepository prescriptionRepository;

  @BeforeAll
  public static void setupMapper() {
    objectMapper = new ObjectMapper();
    objectMapper.findAndRegisterModules();
    objectMapper.registerModule(new JavaTimeModule());
  }

  @Test
  @Transactional
  public void createPrescription_validPrescription_returnCreatedPrescription() throws Exception {
    CreatePrescriptionRequestDto prescriptionCreateRequestDto = TestData.getCreatePrescriptionRequestDto();
    String prescriptionCreateRequestDtoPayload = objectMapper.writeValueAsString(prescriptionCreateRequestDto);

    var payload = mockMvc.perform(post("/api/pharmportal/prescriptions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(prescriptionCreateRequestDtoPayload))
        .andExpect(status().isCreated())
        .andExpect(jsonPath("$.doctorId").value(prescriptionCreateRequestDto.getDoctorId()))
        .andExpect(jsonPath("$.patientId").value(prescriptionCreateRequestDto.getPatientId()))
        .andExpect(jsonPath("$.price").value(prescriptionCreateRequestDto.getPrice()))
        .andExpect(jsonPath("$.note").value(prescriptionCreateRequestDto.getNote()))
        .andExpect(jsonPath("$.frequency").value(prescriptionCreateRequestDto.getFrequency()))
        .andExpect(jsonPath("$.medicationName").value(prescriptionCreateRequestDto.getMedicationName()))
        .andReturn()
        .getResponse()
        .getContentAsString();

    Prescription retrievedPrescriptionFromHttp = objectMapper.readValue(payload, Prescription.class);
    Prescription retrievedPrescription = prescriptionRepository.getReferenceById(retrievedPrescriptionFromHttp.getId());

    assertNotNull(retrievedPrescription);
    assertEquals(retrievedPrescriptionFromHttp.getId(), retrievedPrescription.getId());
  }

}
