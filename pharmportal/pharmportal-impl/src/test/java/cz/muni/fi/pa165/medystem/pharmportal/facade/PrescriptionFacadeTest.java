package cz.muni.fi.pa165.medystem.pharmportal.facade;

import cz.muni.fi.pa165.medystem.pharmportal.entity.Prescription;
import cz.muni.fi.pa165.medystem.pharmportal.entity.enums.PrescriptionStatus;
import cz.muni.fi.pa165.medystem.pharmportal.mapper.PrescriptionMapper;
import cz.muni.fi.pa165.medystem.pharmportal.service.PrescriptionService;
import cz.muni.fi.pa165.medystem.pharmportal.util.TestData;
import cz.muni.fi.pa165.medystem.pharmportalapi.dto.CalculatePriceResponseDto;
import cz.muni.fi.pa165.medystem.pharmportalapi.dto.CancelPrescriptionRequestDto;
import cz.muni.fi.pa165.medystem.pharmportalapi.dto.CreatePrescriptionRequestDto;
import cz.muni.fi.pa165.medystem.pharmportalapi.dto.ViewPrescriptionResponseDto;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class PrescriptionFacadeTest {

    @Mock
    private PrescriptionService prescriptionService;

    @Mock
    private PrescriptionMapper prescriptionMapper;

    @InjectMocks
    private PrescriptionFacade prescriptionFacade;

    @Test
    void createPrescription_validPrescription_prescriptionSaved() {
        CreatePrescriptionRequestDto dtoIn = TestData.getCreatePrescriptionRequestDto();
        Prescription prescription = TestData.getPrescription();
        ViewPrescriptionResponseDto expectedDto = TestData.getViewPrescriptionResponseDto();

        when(prescriptionMapper.prescriptionCreateDtoToPrescription(dtoIn)).thenReturn(prescription);
        when(prescriptionService.createPrescription(prescription)).thenReturn(prescription);
        when(prescriptionMapper.prescriptionToPrescriptionViewDto(prescription)).thenReturn(expectedDto);

        ViewPrescriptionResponseDto result = prescriptionFacade.createPrescription(dtoIn);

        assertThat(result).isEqualTo(expectedDto);
    }

    @Test
    void findPrescriptionById_validId_prescriptionFound() {
        Prescription prescription = TestData.getPrescription();
        ViewPrescriptionResponseDto expectedDto = TestData.getViewPrescriptionResponseDto();

        when(prescriptionService.findPrescriptionById(1L)).thenReturn(prescription);
        when(prescriptionMapper.prescriptionToPrescriptionViewDto(prescription)).thenReturn(expectedDto);

        ViewPrescriptionResponseDto result = prescriptionFacade.findPrescriptionById(1L);

        assertThat(result).isEqualTo(expectedDto);
    }

    @Test
    void listPrescriptionsByPatientId_validId_prescriptionsListed() {
        long patientId = 1L;
        Prescription prescription = TestData.getPrescription();
        ViewPrescriptionResponseDto dto = TestData.getViewPrescriptionResponseDto();
        List<Prescription> prescriptions = Arrays.asList(prescription);
        Page<Prescription> prescriptionPage = new PageImpl<>(prescriptions, PageRequest.of(0, 10), prescriptions.size());

        when(prescriptionService.listPrescriptionByPatientId(eq(patientId), any(Pageable.class))).thenReturn(prescriptionPage);
        when(prescriptionMapper.prescriptionToPrescriptionViewDto(any(Prescription.class))).thenReturn(dto);

        Page<ViewPrescriptionResponseDto> results = prescriptionFacade.listPrescriptionsByPatientId(patientId, PageRequest.of(0, 10));

        assertThat(results.getContent()).containsExactly(dto);
    }

    @Test
    void listActivePrescriptionsByPatientId_validId_activePrescriptionsListed() {
        long patientId = 1L;
        Prescription prescription = TestData.getPrescription();
        prescription.setPrescribedUntil(LocalDate.now().plusDays(1)); // Assume it is active
        ViewPrescriptionResponseDto dto = TestData.getViewPrescriptionResponseDto();
        List<Prescription> prescriptions = Arrays.asList(prescription);
        Page<Prescription> prescriptionPage = new PageImpl<>(prescriptions, PageRequest.of(0, 10), prescriptions.size());

        when(prescriptionService.listPrescriptionByPatientIdAndStatus(eq(patientId), eq(PrescriptionStatus.DISPENSED), any(Pageable.class))).thenReturn(prescriptionPage);
        when(prescriptionMapper.prescriptionToPrescriptionViewDto(any(Prescription.class))).thenReturn(dto);

        Page<ViewPrescriptionResponseDto> results = prescriptionFacade.listActivePrescriptionsByPatientId(patientId, PageRequest.of(0, 10));

        assertThat(results.getContent()).hasSize(1);
        assertThat(results.getContent().get(0)).isEqualTo(dto);
    }

    @Test
    void cancelPrescription_validRequest_prescriptionCancelled() {
        CancelPrescriptionRequestDto dtoIn = TestData.getCancelPrescriptionRequestDto();
        Prescription prescription = TestData.getPrescription();
        ViewPrescriptionResponseDto expectedDto = TestData.getViewPrescriptionResponseDto();

        when(prescriptionService.cancelPrescription(1L, dtoIn.getNote())).thenReturn(prescription);
        when(prescriptionMapper.prescriptionToPrescriptionViewDto(prescription)).thenReturn(expectedDto);

        ViewPrescriptionResponseDto result = prescriptionFacade.cancelPrescription(1L, dtoIn);

        assertThat(result).isEqualTo(expectedDto);
    }

    @Test
    void dispensePrescription_validId_prescriptionDispensed() {
        Prescription prescription = TestData.getPrescription();
        ViewPrescriptionResponseDto expectedDto = TestData.getViewPrescriptionResponseDto();

        when(prescriptionService.dispensePrescription(1L)).thenReturn(prescription);
        when(prescriptionMapper.prescriptionToPrescriptionViewDto(prescription)).thenReturn(expectedDto);

        ViewPrescriptionResponseDto result = prescriptionFacade.dispensePrescription(1L);

        assertThat(result).isEqualTo(expectedDto);
    }

    @Test
    void calculatePatientCosts_validId_costsCalculated() {
        long patientId = 1L;
        Prescription prescription = TestData.getPrescription();
        prescription.setPrice(100.0);
        prescription.dispense();
        Pageable pageable = PageRequest.of(0, 10);
        Page<Prescription> prescriptionPage = new PageImpl<>(List.of(prescription), pageable, 1);
        CalculatePriceResponseDto expectedDto = new CalculatePriceResponseDto(100.0);

        when(prescriptionService.listPrescriptionByPatientId(patientId, pageable))
                .thenReturn(prescriptionPage);

        CalculatePriceResponseDto actualDto = prescriptionFacade.calculatePatientCosts(patientId);

        assertEquals(expectedDto.getPrice(), actualDto.getPrice());
        verify(prescriptionService, times(1)).listPrescriptionByPatientId(patientId, pageable);
    }

    @Test
    void listUnresolvedPatientPrescriptions_validId_unresolvedPrescriptionsReturned() {
        long patientId = 1L;
        Prescription prescription = TestData.getPrescription();
        ViewPrescriptionResponseDto dto = TestData.getViewPrescriptionResponseDto();
        List<Prescription> prescriptions = Arrays.asList(prescription);
        Page<Prescription> prescriptionPage = new PageImpl<>(prescriptions, PageRequest.of(0, 10), prescriptions.size());

        when(prescriptionService.listPrescriptionByPatientIdAndStatus(eq(patientId), eq(PrescriptionStatus.CREATED), any(Pageable.class))).thenReturn(prescriptionPage);
        when(prescriptionMapper.prescriptionToPrescriptionViewDto(any(Prescription.class))).thenReturn(dto);

        Page<ViewPrescriptionResponseDto> result = prescriptionFacade.listUnresolvedPatientPrescriptions(patientId, PageRequest.of(0, 10));

        assertThat(result).hasSize(1);
        assertThat(result.get().toList()).isEqualTo(List.of(dto));
    }

    @Test
    void listAllPrescriptions_prescriptionsExist_allPrescriptionsReturned() {
        Prescription prescription = TestData.getPrescription();
        ViewPrescriptionResponseDto dto = TestData.getViewPrescriptionResponseDto();
        Pageable pageable = PageRequest.of(0, 10, Sort.by(Sort.Direction.ASC, "id"));
        Page<Prescription> pageEntity = new PageImpl<>(List.of(prescription), pageable, 1);

        when(prescriptionService.listPrescriptions(pageable)).thenReturn(pageEntity);
        when(prescriptionMapper.prescriptionToPrescriptionViewDto(prescription)).thenReturn(dto);

        Page<ViewPrescriptionResponseDto> result = prescriptionFacade.listAllPrescriptions(pageable);

        assertThat(result).hasSize(1);
        assertThat(result.get().toList()).isEqualTo(List.of(dto));
    }
}
