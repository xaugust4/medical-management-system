package cz.muni.fi.pa165.medystem.pharmportal.facade;

import cz.muni.fi.pa165.medystem.pharmportal.entity.Prescription;
import cz.muni.fi.pa165.medystem.pharmportal.mapper.PrescriptionMapper;
import cz.muni.fi.pa165.medystem.pharmportal.service.PrescriptionService;
import cz.muni.fi.pa165.medystem.pharmportalapi.dto.CalculatePriceResponseDto;
import cz.muni.fi.pa165.medystem.pharmportalapi.dto.CancelPrescriptionRequestDto;
import cz.muni.fi.pa165.medystem.pharmportalapi.dto.CreatePrescriptionRequestDto;
import cz.muni.fi.pa165.medystem.pharmportalapi.dto.ViewPrescriptionResponseDto;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.Collections;
import java.util.NoSuchElementException;

@ExtendWith(MockitoExtension.class)
public class PrescriptionFacadeErrorHandlingTest {

    @Mock
    private PrescriptionService prescriptionService;

    @Mock
    private PrescriptionMapper prescriptionMapper;

    @InjectMocks
    private PrescriptionFacade prescriptionFacade;

    @Test
    void createPrescription_invalidRequest_throwsException() {
        CreatePrescriptionRequestDto dtoIn = new CreatePrescriptionRequestDto();
        when(prescriptionMapper.prescriptionCreateDtoToPrescription(dtoIn)).thenThrow(IllegalArgumentException.class);

        assertThatThrownBy(() -> prescriptionFacade.createPrescription(dtoIn))
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void findPrescriptionById_invalidId_throwsException() {
        long invalidId = -1L;
        when(prescriptionService.findPrescriptionById(invalidId)).thenThrow(NoSuchElementException.class);

        assertThatThrownBy(() -> prescriptionFacade.findPrescriptionById(invalidId))
                .isInstanceOf(NoSuchElementException.class);
    }

    @Test
    void listPrescriptionsByPatientId_noPrescriptionsFound_returnsEmptyPage() {
        long patientId = 1L;
        Pageable pageable = PageRequest.of(0, 10);
        Page<Prescription> emptyPage = new PageImpl<>(Collections.emptyList(), pageable, 0);

        when(prescriptionService.listPrescriptionByPatientId(patientId, pageable)).thenReturn(emptyPage);

        Page<ViewPrescriptionResponseDto> results = prescriptionFacade.listPrescriptionsByPatientId(patientId, pageable);

        assertThat(results).isEmpty();
    }

    @Test
    void cancelPrescription_nonExistentPrescription_throwsException() {
        long nonExistentPrescriptionId = -1L;
        CancelPrescriptionRequestDto dtoIn = new CancelPrescriptionRequestDto();
        when(prescriptionService.cancelPrescription(nonExistentPrescriptionId, dtoIn.getNote())).thenThrow(NoSuchElementException.class);

        assertThatThrownBy(() -> prescriptionFacade.cancelPrescription(nonExistentPrescriptionId, dtoIn))
                .isInstanceOf(NoSuchElementException.class);
    }

    @Test
    void dispensePrescription_invalidPrescriptionId_throwsException() {
        long invalidPrescriptionId = -1L;
        when(prescriptionService.dispensePrescription(invalidPrescriptionId)).thenThrow(NoSuchElementException.class);

        assertThatThrownBy(() -> prescriptionFacade.dispensePrescription(invalidPrescriptionId))
                .isInstanceOf(NoSuchElementException.class);
    }

    @Test
    void calculatePatientCosts_noDispensedPrescriptions_returnsZeroCost() {
        long patientId = 1L;
        Pageable pageable = PageRequest.of(0, 10);
        Page<Prescription> emptyPage = new PageImpl<>(Collections.emptyList(), pageable, 0);

        when(prescriptionService.listPrescriptionByPatientId(eq(patientId), any(Pageable.class))).thenReturn(emptyPage);

        CalculatePriceResponseDto result = prescriptionFacade.calculatePatientCosts(patientId);

        assertThat(result.getPrice()).isZero();
        verify(prescriptionService).listPrescriptionByPatientId(eq(patientId), any(Pageable.class));
    }
}
