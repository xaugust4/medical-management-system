
user_exists() {
  PGPASSWORD="$POSTGRES_PASSWORD" psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname postgres -h postgres -p 5432 -tAc "SELECT 1 FROM pg_roles WHERE rolname='$1'" | grep -q 1
}

database_exists() {
  PGPASSWORD="$POSTGRES_PASSWORD" psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname postgres -h postgres -p 5432 -tAc "SELECT 1 FROM pg_database WHERE datname='$1'" | grep -q 1
}

create_db() {
DATABASE_NAME="$1"
DATABASE_USER="$2"
# Create the database if it doesn't exist
if ! database_exists "$DATABASE_NAME"; then
    echo "Creating database $DATABASE_NAME since it does not exist."
    PGPASSWORD="$POSTGRES_PASSWORD" psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" -h postgres -p 5432 <<-EOSQL
        CREATE DATABASE "$DATABASE_NAME";
        GRANT ALL PRIVILEGES ON DATABASE "$DATABASE_NAME" TO "$DATABASE_USER";
EOSQL
    # Now, grant rights on the public schema to DATABASE_USER
    PGPASSWORD="$POSTGRES_PASSWORD" psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$DATABASE_NAME" -h postgres -p 5432 <<-EOSQL
        GRANT ALL PRIVILEGES ON SCHEMA public TO "$DATABASE_USER";
        GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO "$DATABASE_USER";
        ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT ALL PRIVILEGES ON TABLES TO "$DATABASE_USER";
EOSQL
else
    echo "Database '$DATABASE_NAME' already exists, skipping creation."
fi
}

create_user() {
DATABASE_USER="$1"
DATABASE_PASSWORD="$2"
# Create the user if it doesn't exist
if ! user_exists "$DATABASE_USER"; then
  echo "Creating database user '$DATABASE_USER' since it does not exist."
  PGPASSWORD="$POSTGRES_PASSWORD" psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" -h postgres -p 5432 <<-EOSQL
      CREATE USER "$DATABASE_USER" WITH PASSWORD '$DATABASE_PASSWORD';
EOSQL
else
  echo "User '$DATABASE_USER' already exists, skipping creation."
fi
}

export PGPASSWORD="$POSTGRES_PASSWORD"

create_user $USERS_DATABASE_USER $USERS_DATABASE_PASSWORD
create_user $BACKOFFICE_DATABASE_USER $BACKOFFICE_DATABASE_PASSWORD
create_user $PHARMPORTAL_DATABASE_USER $PHARMPORTAL_DATABASE_PASSWORD

create_db $USERS_DATABASE_NAME $USERS_DATABASE_USER
create_db $BACKOFFICE_DATABASE_NAME $BACKOFFICE_DATABASE_USER
create_db $PHARMPORTAL_DATABASE_NAME $PHARMPORTAL_DATABASE_USER
