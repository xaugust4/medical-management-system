# Medical Management System

## Description

This is a simple medical management system that allows users to manage patients, prescriptions and appointments. The
system is built using SpringBoot, Hibernate and Docker. We use PostgreSQL database as our primary choice.

## Modules

The system is divided into three modules:

1. **Pharmportal**: This module allows doctors to prescribe medication and pharmacists to dispense it. Users can add,
   list, dispense and cancel and view prescriptions.
2. **Backoffice**: This module allows users to manage patients, their medical records and appointments. Users can add,
   list, cancel and view patients and appointments.
3. **Users and groups**: This module allows users to manage users and groups. Users can add, list, update and delete
   users and groups. This module will be used to authenticate users and manage their roles.

## Use Case Diagram

![uc.png](docs/uc.png)

## DTO diagrams

Pharmportal:
![dto-pharmportal .png](docs/dto-pharmportal.png)

Backoffice:
![img.png](docs/dto-backoffice.png)

Users and groups:
![img.png](docs/dto-users-and-groups.png)

## Setup

Our deployment supports Windows (10 or higher), Rocky Linux (8 or higher) and Ubuntu (22 or higher) deployment. Others
platforms may work, but it is not guaranteed.

### Prerequisites

- Docker (Desktop) installed, e.g. from [here](https://www.docker.com/products/docker-desktop).

### Running the whole application

- Navigate to the root directory of the project (dir where this `README.md` resides).
- Firstly, you need obtain `docker-compose.yml`. The easiest way is to copy and rename `docker-compose-prod.yml`
  to `docker-compose.yml`.
- Secondly, you need to define configuration environmental variables. The easiest way is to copy and
  rename `.env.sample` to `.env`. In this file you can change all settings to your liking.
- After you have completed previous steps, run `docker compose up -d --build` to run the application. Depending on your
  system, you might need to run the command as `sudo`.
- Application will now run on ports defined by the `.env` file.
- When you are done, run `docker compose down` to shut down the deployment. If you wish to also remove all persistent
  volumes, run `docker compose down -v` instead.

### Running singular microservice

- Navigate to the root directory of the desired microservice (`backoffice`/`users-and-groups`/`pharmportal`).
- Firstly, you need obtain `docker-compose.yml`. The easiest way is to copy and rename `docker-compose-dev.yml`
  to `docker-compose.yml`.
- Secondly, you need to define configuration environmental variables. The easiest way is to copy and
  rename `.env.sample` to `.env`. In this file you can change all settings to your liking.
- After you have completed previous steps, run `docker compose up -d --build` to run the application. Depending on your
  system, you might need to run the command as `sudo`.
- Application will now run on ports defined by the `.env` file.
- When you are done, run `docker compose down` to shut down the deployment. If you wish to also remove all persistent
  volumes, run `docker compose down -v` instead.

### Running the development database for all microservices

- Same as [Running singular microservice](#running-singular-microservice), just use `docker-compose-dev.yml` as a
  template for getting `docker-compose.yml`.

## Known issues

- The database init container *may* end with an error. Please, assign execute permissions to every script
  in `init-scripts` directory and try again.

## Authors

- [Josef Augustin](mailto:505673@mail.muni.cz) - Backoffice Backend, Team Lead
- [Lukas Batora](mailto:514528@mail.muni.cz) - Users and Groups Backend, DevOps
- [Marek Dlouhy](mailto:514557@mail.muni.cz) - Pharmportal Backend
