#!/bin/bash

user_exists() {
  psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" -tAc "SELECT 1 FROM pg_roles WHERE rolname='$1'" | grep -q 1
}

database_exists() {
  psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" -tAc "SELECT 1 FROM pg_database WHERE datname='$1'" | grep -q 1
}

# Create the user if it doesn't exist
if ! user_exists "$USERS_DATABASE_USER"; then
  echo "Creating database user '$USERS_DATABASE_USER' since it does not exist."
  psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
      CREATE USER "$USERS_DATABASE_USER" WITH PASSWORD '$USERS_DATABASE_PASSWORD';
EOSQL
else
  echo "User '$USERS_DATABASE_USER' already exists, skipping creation."
fi

# Create the database if it doesn't exist
if ! database_exists "$USERS_DATABASE_NAME"; then
  echo "Creating database $USERS_DATABASE_NAME since it does not exist."
  psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
      CREATE DATABASE "$USERS_DATABASE_NAME";
      GRANT ALL PRIVILEGES ON DATABASE "$USERS_DATABASE_NAME" TO "$USERS_DATABASE_USER";
EOSQL
else
  echo "Database '$USERS_DATABASE_NAME' already exists, skipping creation."
fi
