package util;

import cz.muni.fi.pa165.medystem.usermanagement.dto.RoleViewResponseDto;
import cz.muni.fi.pa165.medystem.usermanagement.dto.UserCreateRequestDto;
import cz.muni.fi.pa165.medystem.usermanagement.dto.UserUpdateRequestDto;
import cz.muni.fi.pa165.medystem.usermanagement.dto.UserViewResponseDto;
import cz.muni.fi.pa165.medystem.usermanagement.entity.Role;
import cz.muni.fi.pa165.medystem.usermanagement.entity.User;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class TestData {

    public static RoleViewResponseDto createAdminRoleResponse() {
        return new RoleViewResponseDto(
                1L,
                "ADMIN",
                "Administrator role with full permissions."
        );
    }

    public static UserCreateRequestDto createUserRequest() {
        return new UserCreateRequestDto(
                "john_doe",
                "John",
                "Doe"
        );
    }

    public static UserUpdateRequestDto updateUserRequest() {
        return new UserUpdateRequestDto(
                "johnny_d",
                "Johnny",
                "Doe"
        );
    }

    public static UserViewResponseDto createUserResponse() {
        Set<String> roles = new HashSet<>(Arrays.asList("USER", "ADMIN"));
        return new UserViewResponseDto(
                1L,
                "john_doe",
                "John",
                "Doe",
                "2023-04-01T12:00:00Z",
                roles
        );
    }

    public static Role createAdminRole() {
        Role adminRole = new Role();
        adminRole.setRoleName("ADMIN");
        adminRole.setDescription("Administrator role with full permissions.");
        return adminRole;
    }

    public static User createUser() {
        User johnDoe = new User();
        johnDoe.setUsername("john_doe");
        johnDoe.setFirstName("John");
        johnDoe.setLastName("Doe");
        johnDoe.setRegistrationDate(LocalDate.of(2023, 4, 1));
        johnDoe.setRoles(Set.of(
                        new Role("Admin", "System administrator")
                )
        );

        return johnDoe;
    }
}
