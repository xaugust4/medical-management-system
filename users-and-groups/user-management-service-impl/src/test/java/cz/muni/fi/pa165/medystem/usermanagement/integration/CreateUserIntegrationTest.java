package cz.muni.fi.pa165.medystem.usermanagement.integration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import cz.muni.fi.pa165.medystem.usermanagement.dto.UserCreateRequestDto;
import cz.muni.fi.pa165.medystem.usermanagement.entity.User;
import cz.muni.fi.pa165.medystem.usermanagement.repository.UserRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import util.TestData;

@SpringBootTest
@AutoConfigureMockMvc()
public class CreateUserIntegrationTest {

  private static ObjectMapper objectMapper = new ObjectMapper();
  @Autowired
  private MockMvc mockMvc;
  @Autowired
  private UserRepository userRepository;

  @BeforeAll
  public static void setupMapper() {
    objectMapper = new ObjectMapper();
    objectMapper.registerModule(new JavaTimeModule());
  }

  @Test
  @Transactional
  public void createUser_validUser_returnCreatedUser() throws Exception {
    UserCreateRequestDto userCreateRequestDto = TestData.createUserRequest();
    String userCreateRequestDtoPayload = new ObjectMapper().writeValueAsString(userCreateRequestDto);

    var payload = mockMvc.perform(post("/api/users/create")
            .contentType(MediaType.APPLICATION_JSON)
            .content(userCreateRequestDtoPayload))
        .andExpect(status().isCreated())
        .andExpect(jsonPath("$.username").value(userCreateRequestDto.getUsername()))
        .andExpect(jsonPath("$.firstName").value(userCreateRequestDto.getFirstName()))
        .andExpect(jsonPath("$.lastName").value(userCreateRequestDto.getLastName()))
        .andReturn()
        .getResponse()
        .getContentAsString();

    User retrievedUserFromHttp = objectMapper.readValue(payload, User.class);
    User retrievedUser = userRepository.getReferenceById(retrievedUserFromHttp.getId());

    assertNotNull(retrievedUser);
    assertEquals(retrievedUserFromHttp.getId(), retrievedUser.getId());
  }

}
