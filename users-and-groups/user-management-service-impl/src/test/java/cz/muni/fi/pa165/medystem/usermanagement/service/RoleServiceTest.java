package cz.muni.fi.pa165.medystem.usermanagement.service;

import cz.muni.fi.pa165.medystem.usermanagement.entity.Role;
import cz.muni.fi.pa165.medystem.usermanagement.exception.RoleNotFoundException;
import cz.muni.fi.pa165.medystem.usermanagement.repository.RoleRepository;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.mockito.ArgumentMatchers.any;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.when;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import util.TestData;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
class RoleServiceTest {

    @Mock
    private RoleRepository roleRepository;

    @InjectMocks
    private RoleService roleService;

    @Test
    void createRole_roleCreated_returnRole() {
        // Arrange
        Role role = TestData.createAdminRole();
        when(roleRepository.save(role)).thenReturn(role);

        // Act
        Role success = roleService.createRole(role);

        // Assert
        assertEquals(success, role);
    }

    @Test
    void createRoleWithoutName_roleNotCreated_throwException() {
        // Arrange
        Role role = TestData.createAdminRole();
        role.setRoleName(null);
        when(roleRepository.save(role)).thenThrow(new IllegalArgumentException("Role name cannot be null"));

        // Act
        assertThrows(IllegalArgumentException.class, () -> roleService.createRole(role));
    }

    @Test
    void updateRole_roleUpdated_setRoleName() {
        // Arrange
        Role originalRole = TestData.createAdminRole();
        Role updatedRole = TestData.createAdminRole();
        updatedRole.setRoleName("newRoleName");

        when(roleRepository.findById(1L)).thenReturn(Optional.of(originalRole));
        when(roleRepository.save(updatedRole)).thenReturn(updatedRole);

        // Act
        Role success = roleService.updateRole(1L, updatedRole);

        // Assert
        assertEquals(success.getRoleName(), "newRoleName");
    }

    @Test
    void updateRole_roleNotFound_throwException() {
        // Arrange
        Role roleDetails = TestData.createAdminRole();
        roleDetails.setRoleName("newRoleName");

        when(roleRepository.findById(1L)).thenReturn(Optional.empty());

        // Assert
        assertThrows(RoleNotFoundException.class, () -> roleService.updateRole(1L, roleDetails));
    }

    @Test
    void deleteRole_roleFound_roleNotPresent() {
        // Arrange
        Role role = TestData.createAdminRole();
        when(roleRepository.findById(1L)).thenReturn(Optional.of(role));

        // Act
        roleService.deleteRole(1L);

        // Assert
        Mockito.verify(roleRepository).delete(role);
    }

    @Test
    void deleteRole_roleNotFound_throwException() {
        // Arrange
        when(roleRepository.findById(1L)).thenReturn(Optional.empty());

        // Assert
        assertThrows(RoleNotFoundException.class, () -> roleService.deleteRole(1L));
    }

    @Test
    void getRoleById_roleFound_returnRole() {
        // Arrange
        Role role = TestData.createAdminRole();
        when(roleRepository.findById(1L)).thenReturn(Optional.of(role));

        // Act
        Role success = roleService.getRoleById(1L);

        // Assert
        assertEquals(success, role);
    }

    @Test
    void getRoleById_roleNotFound_throwException() {
        // Arrange
        when(roleRepository.findById(1L)).thenReturn(Optional.empty());

        // Assert
        assertThrows(RoleNotFoundException.class, () -> roleService.getRoleById(1L));
    }

    @Test
    void getAllRoles_foundOneRole_returnOneRole() {
        // Arrange
        Role role = TestData.createAdminRole();
        Pageable pageable = PageRequest.of(0, 10);
        Page<Role> rolePage = new PageImpl<>(List.of(role), pageable, 1);

        when(roleRepository.findAll(any(Pageable.class))).thenReturn(rolePage);

        // Act
        Page<Role> result = roleService.getAllRoles(pageable);

        // Assert
        assertThat(result.getContent()).isEqualTo(List.of(role));
    }

    @Test
    void getAllRoles_noRoles_returnEmptyList() {
        // Arrange
        Pageable pageable = PageRequest.of(0, 10);
        Page<Role> emptyRolePage = new PageImpl<>(Collections.emptyList(), pageable, 0);

        when(roleRepository.findAll(any(Pageable.class))).thenReturn(emptyRolePage);

        // Act
        Page<Role> result = roleService.getAllRoles(pageable);

        // Assert
        assertThat(result.getContent()).isEqualTo(List.of());
    }
}