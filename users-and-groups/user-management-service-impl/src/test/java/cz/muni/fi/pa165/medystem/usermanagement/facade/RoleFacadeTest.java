package cz.muni.fi.pa165.medystem.usermanagement.facade;

import cz.muni.fi.pa165.medystem.usermanagement.dto.RoleViewResponseDto;
import cz.muni.fi.pa165.medystem.usermanagement.entity.Role;
import cz.muni.fi.pa165.medystem.usermanagement.exception.RoleNotFoundException;
import cz.muni.fi.pa165.medystem.usermanagement.mapper.RoleMapper;
import cz.muni.fi.pa165.medystem.usermanagement.service.RoleService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.mockito.ArgumentMatchers.any;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.when;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import util.TestData;

import java.util.Collections;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class RoleFacadeTest {

    @Mock
    private RoleService roleService;

    @Mock
    private RoleMapper roleMapper;

    @InjectMocks
    private RoleFacade roleFacade;

    @Test
    void createRole_roleCreated_returnRole() {
        // Arrange
        RoleViewResponseDto roleViewResponseDto = TestData.createAdminRoleResponse();
        Role role = TestData.createAdminRole();
        when(roleMapper.roleViewDtoToRole(roleViewResponseDto)).thenReturn(role);
        when(roleService.createRole(role)).thenReturn(role);
        when(roleMapper.roleToRoleViewDto(role)).thenReturn(roleViewResponseDto);

        // Act
        RoleViewResponseDto success = roleFacade.createRole(roleViewResponseDto);

        // Assert
        assertThat(success).isEqualTo(roleViewResponseDto);
    }

    @Test
    void createRoleWithoutName_roleNotCreated_throwException() {
        // Arrange
        RoleViewResponseDto roleViewResponseDto = TestData.createAdminRoleResponse();
        roleViewResponseDto.setRoleName(null);

        Role role = TestData.createAdminRole();
        role.setRoleName(null);

        when(roleMapper.roleViewDtoToRole(roleViewResponseDto)).thenReturn(role);
        when(roleService.createRole(role)).thenThrow(new IllegalArgumentException("Role name cannot be null"));

        // Act
        assertThrows(IllegalArgumentException.class, () -> roleFacade.createRole(roleViewResponseDto));
    }

    @Test
    void updateRole_roleUpdated_setRoleUpdated() {
        // Arrange
        RoleViewResponseDto toUpdateDto = TestData.createAdminRoleResponse();
        toUpdateDto.setDescription("new description");

        Role toUpdateEntity = TestData.createAdminRole();
        toUpdateEntity.setDescription("new description");

        when(roleMapper.roleViewDtoToRole(toUpdateDto)).thenReturn(toUpdateEntity);
        when(roleService.updateRole(1L, toUpdateEntity)).thenReturn(toUpdateEntity);
        when(roleMapper.roleToRoleViewDto(toUpdateEntity)).thenReturn(toUpdateDto);

        // Act
        RoleViewResponseDto success = roleFacade.updateRole(1L, toUpdateDto);

        // Assert
        assertThat(success).isEqualTo(toUpdateDto);
    }

    @Test
    void updateRoleNotExisting_roleNotUpdated_throwException() {
        // Arrange
        RoleViewResponseDto toUpdateDto = TestData.createAdminRoleResponse();
        toUpdateDto.setDescription("new description");

        Role toUpdateEntity = TestData.createAdminRole();
        toUpdateEntity.setDescription("new description");

        when(roleMapper.roleViewDtoToRole(toUpdateDto)).thenReturn(toUpdateEntity);
        when(roleService.updateRole(1L, toUpdateEntity)).thenThrow(new RoleNotFoundException("Role with id 1 not found"));

        // Act
        assertThrows(RoleNotFoundException.class, () -> roleFacade.updateRole(1L, toUpdateDto));
    }

    @Test
    void deleteRole_roleDeleted_roleNotPresent() {
        // Arrange
        Mockito.doNothing().when(roleService).deleteRole(1L);

        // Act
        roleFacade.deleteRole(1L);

        // Assert
        Mockito.verify(roleService, Mockito.times(1)).deleteRole(1L);
    }

    @Test
    void deleteRole_roleNotFound_throwException() {
        // Arrange
        Mockito.doThrow(new RoleNotFoundException("Role with id 1 not found")).when(roleService).deleteRole(1L);

        // Act
        assertThrows(RoleNotFoundException.class, () -> roleFacade.deleteRole(1L));
    }

    @Test
    void getRoleById_roleFound_returnRole() {
        // Arrange
        Role role = TestData.createAdminRole();
        RoleViewResponseDto roleViewResponseDto = TestData.createAdminRoleResponse();
        when(roleService.getRoleById(1L)).thenReturn(role);
        when(roleMapper.roleToRoleViewDto(role)).thenReturn(roleViewResponseDto);

        // Act
        RoleViewResponseDto found = roleFacade.getRoleById(1L);

        // Assert
        assertThat(found).isEqualTo(roleViewResponseDto);
    }

    @Test
    void getRoleById_roleNotFound_throwException() {
        // Arrange
        when(roleService.getRoleById(1L)).thenThrow(new RoleNotFoundException("Role with id 1 not found"));

        // Act
        assertThrows(RoleNotFoundException.class, () -> roleFacade.getRoleById(1L));
    }

    @Test
    void getAllRoles_foundOneRole_returnRoles() {
        // Arrange
        Role role = TestData.createAdminRole();
        RoleViewResponseDto roleViewResponseDto = TestData.createAdminRoleResponse();
        Pageable pageable = PageRequest.of(0, 10);
        Page<Role> rolePage = new PageImpl<>(List.of(role), pageable, 1);

        when(roleService.getAllRoles(any(Pageable.class))).thenReturn(rolePage);
        when(roleMapper.roleToRoleViewDto(role)).thenReturn(roleViewResponseDto);

        // Act
        Page<RoleViewResponseDto> found = roleFacade.getAllRoles(pageable);

        // Assert
        assertThat(found.getContent()).containsExactly(roleViewResponseDto);
    }

    @Test
    void getAllRoles_noRoles_returnEmptyList() {
        // Arrange
        Pageable pageable = PageRequest.of(0, 10);
        Page<Role> emptyRolePage = new PageImpl<>(Collections.emptyList(), pageable, 0);

        when(roleService.getAllRoles(any(Pageable.class))).thenReturn(emptyRolePage);

        // Act
        Page<RoleViewResponseDto> found = roleFacade.getAllRoles(pageable);

        // Assert
        assertThat(found.getContent()).isEmpty();
    }
}
