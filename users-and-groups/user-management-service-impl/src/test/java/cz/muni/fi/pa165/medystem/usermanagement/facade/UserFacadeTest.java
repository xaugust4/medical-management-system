package cz.muni.fi.pa165.medystem.usermanagement.facade;

import cz.muni.fi.pa165.medystem.usermanagement.dto.UserCreateRequestDto;
import cz.muni.fi.pa165.medystem.usermanagement.dto.UserUpdateRequestDto;
import cz.muni.fi.pa165.medystem.usermanagement.dto.UserViewResponseDto;
import cz.muni.fi.pa165.medystem.usermanagement.entity.User;
import cz.muni.fi.pa165.medystem.usermanagement.exception.UserNotFoundException;
import cz.muni.fi.pa165.medystem.usermanagement.mapper.UserMapper;
import cz.muni.fi.pa165.medystem.usermanagement.service.UserService;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.mockito.ArgumentMatchers.any;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.when;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import util.TestData;

import java.util.List;

@ExtendWith(MockitoExtension.class)
class UserFacadeTest {

    @Mock
    private UserService userService;

    @Mock
    private UserMapper userMapper;

    @InjectMocks
    private UserFacade userFacade;

    @Test
    void createUser_userCreated_returnUser() {
        // Arrange
        UserCreateRequestDto userCreateRequestDto = TestData.createUserRequest();
        User user = TestData.createUser();
        UserViewResponseDto userViewResponseDto = TestData.createUserResponse();

        when(userMapper.userCreateDtoToUser(userCreateRequestDto)).thenReturn(user);
        when(userService.createUser(user)).thenReturn(user);
        when(userMapper.userToUserViewDto(user)).thenReturn(userViewResponseDto);

        // Act
        UserViewResponseDto success = userFacade.createUser(userCreateRequestDto);

        // Assert
        assertThat(success).isEqualTo(userViewResponseDto);
    }

    @Test
    void createUserWithoutFirstName_userNotCreated_throwException() {
        // Arrange
        UserCreateRequestDto userCreateRequestDto = TestData.createUserRequest();
        userCreateRequestDto.setFirstName(null);

        User user = TestData.createUser();
        user.setFirstName(null);

        when(userMapper.userCreateDtoToUser(userCreateRequestDto)).thenReturn(user);
        when(userService.createUser(user)).thenThrow(new IllegalArgumentException("User name cannot be null"));

        // Act
        assertThrows(IllegalArgumentException.class, () -> userFacade.createUser(userCreateRequestDto));
    }

    @Test
    void getUserById_userFound_returnUser() {
        // Arrange
        User user = TestData.createUser();
        UserViewResponseDto userViewResponseDto = TestData.createUserResponse();

        when(userService.findUserById(1L)).thenReturn(user);
        when(userMapper.userToUserViewDto(user)).thenReturn(userViewResponseDto);

        // Act
        UserViewResponseDto success = userFacade.getUserById(1L);

        // Assert
        assertThat(success).isEqualTo(userViewResponseDto);
    }

    @Test
    void getUserById_userNotFound_throwException() {
        // Arrange
        when(userService.findUserById(1L)).thenThrow(new UserNotFoundException("User not found with id: " + 1L));

        // Act
        assertThrows(UserNotFoundException.class, () -> userFacade.getUserById(1L));
    }

    @Test
    void getUserByUsername_userFound_returnUser() {
        // Arrange
        User user = TestData.createUser();
        UserViewResponseDto userViewResponseDto = TestData.createUserResponse();

        when(userService.findUserByUsername("john_doe")).thenReturn(user);
        when(userMapper.userToUserViewDto(user)).thenReturn(userViewResponseDto);

        // Act
        UserViewResponseDto success = userFacade.getUserByUsername("john_doe");

        // Assert
        assertThat(success).isEqualTo(userViewResponseDto);
    }

    @Test
    void getUserByUsername_userNotFound_throwException() {
        // Arrange
        when(userService.findUserByUsername("john_doe")).thenThrow(new UserNotFoundException("User not found with username: john_doe"));

        // Act
        assertThrows(UserNotFoundException.class, () -> userFacade.getUserByUsername("john_doe"));
    }

    @Test
    void getAllUsers_foundOneUser_returnOneUser() {
        // Arrange
        User user = TestData.createUser();
        UserViewResponseDto userViewResponseDto = TestData.createUserResponse();
        Pageable pageable = PageRequest.of(0, 10);
        Page<User> userPage = new PageImpl<>(List.of(user), pageable, 1);

        when(userService.findAllUsers(any(Pageable.class))).thenReturn(userPage);
        when(userMapper.userToUserViewDto(user)).thenReturn(userViewResponseDto);

        // Act
        Page<UserViewResponseDto> result = userFacade.getAllUsers(pageable);

        // Assert
        assertThat(result.getContent()).isEqualTo(List.of(userViewResponseDto));
    }

    @Test
    void updateUser_userFound_setFirstName() {
        // Arrange
        UserUpdateRequestDto toUpdateDto = TestData.updateUserRequest();
        toUpdateDto.setFirstName("Johnny");

        User toUpdateEntity = TestData.createUser();
        toUpdateEntity.setFirstName("Johnny");

        UserViewResponseDto toUpdateResponseDto = TestData.createUserResponse();
        toUpdateResponseDto.setFirstName("Johnny");

        when(userMapper.userUpdateDtoToUser(toUpdateDto)).thenReturn(toUpdateEntity);
        when(userService.updateUser(1L, toUpdateEntity)).thenReturn(toUpdateEntity);
        when(userMapper.userToUserViewDto(toUpdateEntity)).thenReturn(toUpdateResponseDto);

        // Act
        UserViewResponseDto success = userFacade.updateUser(1L, toUpdateDto);

        // Assert
        assertThat(success).isEqualTo(toUpdateResponseDto);
    }

    @Test
    void updateUser_userNotFound_throwException() {
        // Arrange
        UserUpdateRequestDto toUpdateDto = TestData.updateUserRequest();
        toUpdateDto.setFirstName("Johnny");

        User toUpdateEntity = TestData.createUser();
        toUpdateEntity.setFirstName("Johnny");

        when(userMapper.userUpdateDtoToUser(toUpdateDto)).thenReturn(toUpdateEntity);
        when(userService.updateUser(1L, toUpdateEntity)).thenThrow(new UserNotFoundException("User not found with id: 1"));

        // Act
        assertThrows(UserNotFoundException.class, () -> userFacade.updateUser(1L, toUpdateDto));
    }

    @Test
    void deleteUser_userFound_userNotPresent() {
        // Arrange
        Mockito.doNothing().when(userService).deleteUser(1L);

        // Act
        userFacade.deleteUser(1L);

        // Assert
        Mockito.verify(userService, Mockito.times(1)).deleteUser(1L);
    }

    @Test
    void deleteUser_userNotFound_throwException() {
        // Arrange
        Mockito.doThrow(new UserNotFoundException("User not found with id: 1")).when(userService).deleteUser(1L);

        // Act
        assertThrows(UserNotFoundException.class, () -> userFacade.deleteUser(1L));
    }
}
