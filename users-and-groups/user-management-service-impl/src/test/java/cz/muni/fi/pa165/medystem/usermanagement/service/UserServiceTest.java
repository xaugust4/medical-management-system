package cz.muni.fi.pa165.medystem.usermanagement.service;

import cz.muni.fi.pa165.medystem.usermanagement.entity.User;
import cz.muni.fi.pa165.medystem.usermanagement.exception.UserNotFoundException;
import cz.muni.fi.pa165.medystem.usermanagement.repository.UserRepository;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.mockito.ArgumentMatchers.any;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.when;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import util.TestData;

import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserService userService;

    @Test
    void createUser_userCreated_returnUser() {
        // Arrange
        User user = TestData.createUser();
        when(userRepository.save(user)).thenReturn(user);

        // Act
        User success = userService.createUser(user);

        // Assert
        assertEquals(success, user);
    }

    @Test
    void createInvalidUser_userNotCreated_throwException() {
        // Arrange
        User user = TestData.createUser();
        user.setUsername(null);
        when(userRepository.save(user)).thenThrow(new IllegalArgumentException("User name cannot be null"));

        // Act
        assertThrows(IllegalArgumentException.class, () -> userService.createUser(user));
    }

    @Test
    void findUserById_userFound_returnUser() {
        // Arrange
        User user = TestData.createUser();
        when(userRepository.findById(1L)).thenReturn(Optional.of(user));

        // Act
        User foundUser = userService.findUserById(1L);

        // Assert
        assertEquals(foundUser, user);
    }

    @Test
    void findUserById_userNotFound_throwException() {
        // Arrange
        when(userRepository.findById(1L)).thenReturn(Optional.empty());

        // Act
        assertThrows(UserNotFoundException.class, () -> userService.findUserById(1L));
    }

    @Test
    void findUserByUsername_userFound_returnUser() {
        // Arrange
        User user = TestData.createUser();
        when(userRepository.findByUsername("john_doe")).thenReturn(Optional.of(user));

        // Act
        User foundUser = userService.findUserByUsername("john_doe");

        // Assert
        assertEquals(foundUser, user);
    }

    @Test
    void findUserByUserName_userNotFound_throwException() {
        // Arrange
        when(userRepository.findByUsername("john_doe")).thenReturn(Optional.empty());

        // Act
        assertThrows(UserNotFoundException.class, () -> userService.findUserByUsername("john_doe"));
    }

    @Test
    void findAllUsers_foundOneUser_returnOneUser() {
        // Arrange
        User user = TestData.createUser();
        Pageable pageable = PageRequest.of(0, 10);
        Page<User> userPage = new PageImpl<>(List.of(user), pageable, 1);

        when(userRepository.findAll(any(Pageable.class))).thenReturn(userPage);

        // Act
        Page<User> result = userService.findAllUsers(pageable);

        // Assert
        assertThat(result.getContent()).containsExactly(user);
    }

    @Test
    void findAllUsers_noUserFound_returnEmptyList() {
        // Arrange
        Pageable pageable = PageRequest.of(0, 10);
        Page<User> emptyUserPage = new PageImpl<>(List.of(), pageable, 0);

        when(userRepository.findAll(any(Pageable.class))).thenReturn(emptyUserPage);

        // Act
        Page<User> result = userService.findAllUsers(pageable);

        // Assert
        assertThat(result.getContent()).isEmpty();
    }

    @Test
    void updateUser_userUpdated_setFirstName() {
        // Arrange
        User user = TestData.createUser();
        user.setFirstName("John");
        when(userRepository.findById(1L)).thenReturn(Optional.of(user));
        when(userRepository.save(user)).thenReturn(user);

        // Act
        User success = userService.updateUser(1L, user);

        // Assert
        assertEquals(success.getFirstName(), "John");
    }

    @Test
    void updateUser_userNotFound_throwException() {
        // Arrange
        when(userRepository.findById(1L)).thenReturn(Optional.empty());

        // Act
        assertThrows(UserNotFoundException.class, () -> userService.updateUser(1L, TestData.createUser()));
    }

    @Test
    void deleteUser_userFound_userDeleted() {
        // Arrange
        when(userRepository.existsById(1L)).thenReturn(true);

        // Act
        userService.deleteUser(1L);

        // Assert
        Mockito.verify(userRepository).deleteById(1L);
    }

    @Test
    void deleteUser_userNotFound_throwException() {
        // Arrange
        when(userRepository.existsById(1L)).thenReturn(false);

        // Act
        assertThrows(UserNotFoundException.class, () -> userService.deleteUser(1L));
    }
}
