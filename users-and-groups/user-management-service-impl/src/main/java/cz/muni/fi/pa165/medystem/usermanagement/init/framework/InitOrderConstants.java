package cz.muni.fi.pa165.medystem.usermanagement.init.framework;

public interface InitOrderConstants {

    int ROLE_ORDER            = 100;
    int USER_ORDER            = ROLE_ORDER + 1;


    int DELEGATE_INITIALIZER    = 1000;
}
