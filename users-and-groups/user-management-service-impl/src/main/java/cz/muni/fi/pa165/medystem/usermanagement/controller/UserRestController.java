package cz.muni.fi.pa165.medystem.usermanagement.controller;

import cz.muni.fi.pa165.medystem.usermanagement.dto.UserCreateRequestDto;
import cz.muni.fi.pa165.medystem.usermanagement.dto.UserUpdateRequestDto;
import cz.muni.fi.pa165.medystem.usermanagement.dto.UserViewResponseDto;
import cz.muni.fi.pa165.medystem.usermanagement.facade.UserFacade;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Max;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/users")
@Tag(name = "User", description = "Operations related to Users")
public class UserRestController {

    private final UserFacade userFacade;

    @Autowired
    public UserRestController(UserFacade userFacade) {
        this.userFacade = userFacade;
    }

    @PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(summary = "Create a new user", description = "Creates a new user with the given details")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "User created",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = UserViewResponseDto.class))),
            @ApiResponse(responseCode = "409", description = "User with desired username already exists", content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = "500", description = "Error when persisting the user", content = @Content(schema = @Schema(hidden = true)))
    })
    public UserViewResponseDto createUser(@Valid @RequestBody @Parameter(description = "User creation data") UserCreateRequestDto userCreateRequestDto) {
        return userFacade.createUser(userCreateRequestDto);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Get a user by ID", description = "Retrieves details of a specific user by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User found",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = UserViewResponseDto.class))),
            @ApiResponse(responseCode = "404", description = "User not found", content = @Content(schema = @Schema(hidden = true)))
    })
    public UserViewResponseDto getUserById(@PathVariable @Parameter(description = "The ID of the user to retrieve") Long id) {
        return userFacade.getUserById(id);
    }

    @GetMapping("/username/{username}")
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Get a user by username", description = "Retrieves details of a specific user by username")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User found",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = UserViewResponseDto.class)))
    })
    public UserViewResponseDto getUserByUsername(@PathVariable @Parameter(description = "The username of the user to retrieve") String username) {
        return userFacade.getUserByUsername(username);
    }

    @GetMapping(value = "/all", params = {"page", "size"})
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Get all users", description = "Retrieves details of all users")
    @ApiResponse(responseCode = "200", description = "Users found",
            content = @Content(mediaType = "application/json",
                    schema = @Schema(implementation = Page.class)))
    public Page<UserViewResponseDto> getAllUsers(@RequestParam(value = "page", defaultValue = "0") int page,
                                                 @RequestParam(value = "size", defaultValue = "10") @Max(value = 50) int size,
                                                 @RequestParam(value = "sort", defaultValue = "id, asc", required = false) String[] sortingParams
    ) {
        String field = sortingParams[0];
        String sortingDirection = sortingParams[1];
        Sort.Direction direction = sortingDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC;
        Pageable pageObj = PageRequest.of(page, size, Sort.by(direction, field));
        return userFacade.getAllUsers(pageObj);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Update a user", description = "Updates the details of an existing user specified by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User updated",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = UserViewResponseDto.class))),
            @ApiResponse(responseCode = "404", description = "User not found", content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = "500", description = "Error when persisting the user", content = @Content(schema = @Schema(hidden = true)))

    })
    public UserViewResponseDto updateUser(@PathVariable @Parameter(description = "The ID of the user to update") Long id,
                                          @Valid @RequestBody @Parameter(description = "User update data") UserUpdateRequestDto userUpdateRequestDto) {
        return userFacade.updateUser(id, userUpdateRequestDto);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Operation(summary = "Delete a user", description = "Deletes a user specified by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "User deleted"),
            @ApiResponse(responseCode = "404", description = "User not found", content = @Content(schema = @Schema(hidden = true)))
    })
    public void deleteUser(@PathVariable @Parameter(description = "The ID of the user to delete") Long id) {
        userFacade.deleteUser(id);
    }
}
