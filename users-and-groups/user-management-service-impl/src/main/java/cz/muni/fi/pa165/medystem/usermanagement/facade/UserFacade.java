package cz.muni.fi.pa165.medystem.usermanagement.facade;

import cz.muni.fi.pa165.medystem.usermanagement.dto.UserCreateRequestDto;
import cz.muni.fi.pa165.medystem.usermanagement.dto.UserUpdateRequestDto;
import cz.muni.fi.pa165.medystem.usermanagement.dto.UserViewResponseDto;
import cz.muni.fi.pa165.medystem.usermanagement.entity.User;
import cz.muni.fi.pa165.medystem.usermanagement.mapper.UserMapper;
import cz.muni.fi.pa165.medystem.usermanagement.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class UserFacade {

    private final UserService userService;
    private final UserMapper userMapper;

    @Autowired
    public UserFacade(UserService userService, UserMapper userMapper) {
        this.userService = userService;
        this.userMapper = userMapper;
    }

    public UserViewResponseDto createUser(UserCreateRequestDto userCreateRequestDto) {
        User user = userMapper.userCreateDtoToUser(userCreateRequestDto);
        user = userService.createUser(user);
        return userMapper.userToUserViewDto(user);
    }

    public UserViewResponseDto getUserById(Long id) {
        User user = userService.findUserById(id);
        return userMapper.userToUserViewDto(user);
    }

    public UserViewResponseDto getUserByUsername(String username) {
        User user = userService.findUserByUsername(username);
        return userMapper.userToUserViewDto(user);
    }

    public Page<UserViewResponseDto> getAllUsers(Pageable pageable) {
        Page<User> users = userService.findAllUsers(pageable);
        return users.map(userMapper::userToUserViewDto);
    }

    public UserViewResponseDto updateUser(Long id, UserUpdateRequestDto userUpdateRequestDto) {
        User user = userService.updateUser(id, userMapper.userUpdateDtoToUser(userUpdateRequestDto));
        return userMapper.userToUserViewDto(user);
    }

    public void deleteUser(Long id) {
        userService.deleteUser(id);
    }
}
