package cz.muni.fi.pa165.medystem.usermanagement.controller;

import cz.muni.fi.pa165.medystem.usermanagement.dto.RoleViewResponseDto;
import cz.muni.fi.pa165.medystem.usermanagement.facade.RoleFacade;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Max;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/users/roles")
@Tag(name = "Role", description = "Operations related to Roles")
public class RoleRestController {

    private final RoleFacade roleFacade;

    @Autowired
    public RoleRestController(RoleFacade roleFacade) {
        this.roleFacade = roleFacade;
    }

    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(summary = "Create a new role", description = "Creates a new role with the specified details")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Role created",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = RoleViewResponseDto.class))),
            @ApiResponse(responseCode = "409", description = "Role with desired details already exists", content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = "500", description = "Internal server error due to persistence issue", content = @Content(schema = @Schema(hidden = true)))
    })
    public RoleViewResponseDto createRole(@Valid @RequestBody @Parameter(description = "Role creation data") RoleViewResponseDto roleViewResponseDto) {
        return roleFacade.createRole(roleViewResponseDto);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Update an existing role", description = "Updates the details of an existing role specified by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Role updated",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = RoleViewResponseDto.class))),
            @ApiResponse(responseCode = "404", description = "Role not found", content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content(schema = @Schema(hidden = true)))
    })
    public RoleViewResponseDto updateRole(@PathVariable @Parameter(description = "The ID of the role to update") Long id,
                                          @Valid @RequestBody @Parameter(description = "Role update data") RoleViewResponseDto roleViewResponseDto) {
        return roleFacade.updateRole(id, roleViewResponseDto);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Operation(summary = "Delete a role", description = "Deletes a role specified by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Role deleted", content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = "404", description = "Role not found", content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content(schema = @Schema(hidden = true)))
    })
    public void deleteRole(@PathVariable @Parameter(description = "The ID of the role to delete") Long id) {
        roleFacade.deleteRole(id);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Get a role by ID", description = "Retrieves details of a specific role by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Role found",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = RoleViewResponseDto.class))),
            @ApiResponse(responseCode = "404", description = "Role not found", content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content(schema = @Schema(hidden = true)))
    })
    public RoleViewResponseDto getRoleById(@PathVariable @Parameter(description = "The ID of the role to retrieve") Long id) {
        return roleFacade.getRoleById(id);
    }

    @GetMapping(value = "/all", params = {"page", "size"})
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Get all roles", description = "Retrieves details of all roles")
    @ApiResponse(responseCode = "200", description = "Roles found",
            content = @Content(mediaType = "application/json",
                    schema = @Schema(implementation = Page.class)))
    public Page<RoleViewResponseDto> getAllRoles(@RequestParam(value = "page", defaultValue = "0") int page,
                                                 @RequestParam(value = "size", defaultValue = "10") @Max(value = 50) int size,
                                                 @RequestParam(value = "sort", defaultValue = "id, asc", required = false) String[] sortingParams
    ) {
        String field = sortingParams[0];
        String sortingDirection = sortingParams[1];
        Sort.Direction direction = sortingDirection.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC;
        Pageable pageObj = PageRequest.of(page, size, Sort.by(direction, field));
        return roleFacade.getAllRoles(pageObj);
    }
}
