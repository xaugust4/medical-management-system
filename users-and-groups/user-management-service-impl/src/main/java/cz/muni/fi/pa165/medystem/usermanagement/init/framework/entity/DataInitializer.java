package cz.muni.fi.pa165.medystem.usermanagement.init.framework.entity;

public interface DataInitializer {

    void initialize();

    int getOrder();

}
