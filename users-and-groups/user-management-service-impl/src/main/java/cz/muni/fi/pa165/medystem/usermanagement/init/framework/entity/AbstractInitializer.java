package cz.muni.fi.pa165.medystem.usermanagement.init.framework.entity;

import cz.muni.fi.pa165.medystem.usermanagement.entity.AbstractEntity;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.repository.JpaRepository;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Slf4j
@Getter
public abstract class AbstractInitializer<
        ID extends Serializable,
        ENTITY extends AbstractEntity<ID>,
        REPOSITORY extends JpaRepository<ENTITY, ID>> implements DataInitializer {

    private final int order;

    protected REPOSITORY repository;

    protected List<ENTITY> entities = new ArrayList<>();

    protected AbstractInitializer(REPOSITORY repository, int order) {
        this.repository = repository;
        this.order = order;
    }

    protected void persist(Collection<? extends ENTITY> entities) {
        repository.saveAll(entities);
    }

    public void initialize() {
        entities = initializeEntities();
        if (!entities.isEmpty()) {
            log.info("Initializing {} {} entities...", entities.size(), entities.get(0).getClass().getSimpleName());
            persist(entities);
            log.info("Initialized {} {} entities.", entities.size(), entities.get(0).getClass().getSimpleName());
        }
    }

    protected abstract List<ENTITY> initializeEntities();

}
