package cz.muni.fi.pa165.medystem.usermanagement;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@OpenAPIDefinition(info = @Info(title = "Users and Roles API", version = "v1", description = "API for managing users and their roles"))
public class UsersAndGroupsApplication {

    public static void main(String[] args) {
        SpringApplication.run(UsersAndGroupsApplication.class, args);
    }

}
