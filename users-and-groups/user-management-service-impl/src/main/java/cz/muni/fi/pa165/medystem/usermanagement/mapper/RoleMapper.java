package cz.muni.fi.pa165.medystem.usermanagement.mapper;

import cz.muni.fi.pa165.medystem.usermanagement.dto.RoleViewResponseDto;
import cz.muni.fi.pa165.medystem.usermanagement.entity.Role;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public abstract class RoleMapper {

    public abstract RoleViewResponseDto roleToRoleViewDto(Role role);

    @Mapping(target = "users", ignore = true)
    public abstract Role roleViewDtoToRole(RoleViewResponseDto roleViewResponseDto);
}
