package cz.muni.fi.pa165.medystem.usermanagement.mapper;

import cz.muni.fi.pa165.medystem.usermanagement.dto.UserCreateRequestDto;
import cz.muni.fi.pa165.medystem.usermanagement.dto.UserUpdateRequestDto;
import cz.muni.fi.pa165.medystem.usermanagement.dto.UserViewResponseDto;
import cz.muni.fi.pa165.medystem.usermanagement.entity.Role;
import cz.muni.fi.pa165.medystem.usermanagement.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class UserMapper {

    @Mapping(target = "roles", source = "roles", qualifiedByName = "mapRolesToNames")
    public abstract UserViewResponseDto userToUserViewDto(User user);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "registrationDate", ignore = true)
    @Mapping(target = "roles", ignore = true)
    public abstract User userCreateDtoToUser(UserCreateRequestDto user);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "registrationDate", ignore = true)
    @Mapping(target = "roles", ignore = true)
    public abstract User userUpdateDtoToUser(UserUpdateRequestDto userUpdateRequestDto);

    @Named("mapRolesToNames")
    Set<String> mapRolesToNames(Set<Role> roles) {
        return roles.stream().map(Role::getRoleName).collect(Collectors.toSet());
    }
}
