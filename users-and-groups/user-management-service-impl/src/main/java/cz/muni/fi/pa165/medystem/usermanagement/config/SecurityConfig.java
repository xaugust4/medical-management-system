package cz.muni.fi.pa165.medystem.usermanagement.config;

import cz.muni.fi.pa165.medystem.usermanagement.facade.UserFacade;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authorization.AuthorizationDecision;
import org.springframework.security.authorization.AuthorizationManager;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.access.intercept.RequestAuthorizationContext;
@Configuration
@EnableWebSecurity
public class SecurityConfig {

  @Autowired
  private UserFacade userFacade;
  @Bean
  public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception {
    httpSecurity
        .csrf(AbstractHttpConfigurer::disable)
        .authorizeHttpRequests(authorizeRequests ->
            authorizeRequests
                .requestMatchers("/login/oauth2/code/muni").authenticated()
                .requestMatchers("/api/users/username/{username}").permitAll()
                .requestMatchers("/api/users/**").access(authorizationManager(Set.of("ADMIN")))
                .requestMatchers("/management/**").permitAll()
                .anyRequest().authenticated()
        )
        .logout(logout ->
            logout
                .logoutUrl("/logout")
                .logoutSuccessUrl("/login")
        )
        .oauth2Login(Customizer.withDefaults())
        .anonymous(AbstractHttpConfigurer::disable);
    return httpSecurity.build();
  }

  public AuthorizationManager<RequestAuthorizationContext> authorizationManager(Set<String> roles) {
    return (authentication, context) -> new AuthorizationDecision(hasAnyRole((authentication.get()), roles));
  }

  public boolean hasAnyRole(Authentication token, Set<String> roles) {
    var userRoles = userFacade.getUserByUsername(token.getName()).getRoles();
    return userRoles.stream().anyMatch(roles::contains);
  }
}
