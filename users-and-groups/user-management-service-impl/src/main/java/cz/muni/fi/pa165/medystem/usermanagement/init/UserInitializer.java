package cz.muni.fi.pa165.medystem.usermanagement.init;

import cz.muni.fi.pa165.medystem.usermanagement.entity.User;
import cz.muni.fi.pa165.medystem.usermanagement.init.framework.InitOrderConstants;
import cz.muni.fi.pa165.medystem.usermanagement.init.framework.data.DataPicker;
import cz.muni.fi.pa165.medystem.usermanagement.init.framework.entity.AbstractInitializer;
import cz.muni.fi.pa165.medystem.usermanagement.repository.RoleRepository;
import cz.muni.fi.pa165.medystem.usermanagement.repository.UserRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@Component
@ConditionalOnExpression("${init.user.enabled:false} || ${init.enabled-all:false}")
public class UserInitializer extends AbstractInitializer<Long, User, UserRepository> {

    private static final int ORDER = InitOrderConstants.USER_ORDER;

    @Value("${init.user.size:100}")
    private long seedSize;

    @Value("${init.user.patientUserIdStart:100}")
    private long patientUserIdStart;

    @Value("${init.user.doctorUserIdStart:1000}")
    private long doctorUserIdStart;

    private final RoleRepository roleRepository;


    protected UserInitializer(UserRepository repository, RoleRepository roleRepository) {
        super(repository, ORDER);
        this.roleRepository = roleRepository;
    }

    @Override
    protected List<User> initializeEntities() {
        List<User> entities = new ArrayList<>();

        for (int i = 0; i < seedSize; i++) {
            entities.add(initializeEntity(true));
        }
        for (int i = 0; i < seedSize; i++) {
            entities.add(initializeEntity(false));
        }

        return entities;
    }

    private User initializeEntity(boolean isPatient) {
        var user = new User();
        String lastName = DataPicker.RandomGenerator.randomLastName();

        if (isPatient) {
            user.setId(patientUserIdStart++);
            roleRepository.findByRoleName(RoleInitializer.PATIENT)
                    .ifPresent((role) -> user.setRoles(Set.of(role)));
            user.setUsername("PATIENT: " + lastName + user.getId());
        } else {
            user.setId(doctorUserIdStart++);
            roleRepository.findByRoleName(RoleInitializer.DOCTOR)
                    .ifPresent((role) -> user.setRoles(Set.of(role)));
            user.setUsername("DOCTOR: " + lastName + user.getId());
        }
        user.setFirstName(DataPicker.RandomGenerator.randomFirstName());
        user.setLastName(lastName);
        user.setRegistrationDate(DataPicker.RandomGenerator.randomDateBetween(2010, 2020));
        return user;
    }



}
