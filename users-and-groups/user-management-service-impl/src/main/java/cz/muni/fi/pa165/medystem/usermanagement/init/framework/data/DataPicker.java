package cz.muni.fi.pa165.medystem.usermanagement.init.framework.data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Random;

public class DataPicker {

    private static final String[] FIRST_NAMES = {
            "John", "Jane", "Michael", "Jessica", "David", "Sarah", "Robert", "Emily", "William", "Ashley"
    };

    private static final String[] LAST_NAMES = {
            "Smith", "Johnson", "Williams", "Jones", "Brown", "Davis", "Miller", "Wilson", "Moore", "Taylor"
    };


    public static class RandomGenerator {

        private static final Random random = new Random();

        public static LocalDate randomDateBetween(int startYear, int endYear) {
            return LocalDate.of(
                    random.nextInt(startYear, endYear),
                    random.nextInt(1, 13),
                    random.nextInt(1, 29)
            );
        }

        public static String randomFirstName() {
            return FIRST_NAMES[random.nextInt(FIRST_NAMES.length)];
        }

        public static String randomLastName() {
            return LAST_NAMES[random.nextInt(LAST_NAMES.length)];
        }

        public static String randomBirthNumber() {
            return random.nextInt(100000, 999999) + "/" + random.nextInt(1000, 9999);
        }

    }
}
