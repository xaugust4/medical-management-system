package cz.muni.fi.pa165.medystem.usermanagement.exception;

import jakarta.persistence.EntityNotFoundException;

public class RoleNotFoundException extends EntityNotFoundException {

    public RoleNotFoundException(String message) {
        super(message);
    }

    public RoleNotFoundException(String message, Exception cause) {
        super(message, cause);
    }
}
