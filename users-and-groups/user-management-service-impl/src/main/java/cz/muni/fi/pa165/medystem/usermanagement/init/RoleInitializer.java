package cz.muni.fi.pa165.medystem.usermanagement.init;

import cz.muni.fi.pa165.medystem.usermanagement.entity.Role;
import cz.muni.fi.pa165.medystem.usermanagement.init.framework.InitOrderConstants;
import cz.muni.fi.pa165.medystem.usermanagement.init.framework.entity.AbstractInitializer;
import cz.muni.fi.pa165.medystem.usermanagement.repository.RoleRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Component;

import java.util.List;

@Getter
@Setter
@Component
@ConditionalOnExpression("${init.role.enabled:false} || ${init.enabled-all:false}")
public class RoleInitializer extends AbstractInitializer<Long, Role, RoleRepository>{

    public static final String ADMIN = "ADMIN";
    public static final String DOCTOR = "DOCTOR";
    public static final String PATIENT = "PATIENT";

    private static final int ORDER = InitOrderConstants.ROLE_ORDER;

    private long roleIdStart = 1000;


    protected RoleInitializer(RoleRepository repository) {
        super(repository, ORDER);
    }

    @Override
    protected List<Role> initializeEntities() {
        return List.of(
                initializeEntity("ADMIN", "Admin role"),
                initializeEntity("DOCTOR", "Doctor role"),
                initializeEntity("PATIENT", "Patient role")
        );
    }

    private Role initializeEntity(String roleName, String description) {
        var role = new Role();
        role.setId(roleIdStart++);
        role.setRoleName(roleName);
        role.setDescription(description);
        return role;
    }


}
