package cz.muni.fi.pa165.medystem.usermanagement.facade;

import cz.muni.fi.pa165.medystem.usermanagement.dto.RoleViewResponseDto;
import cz.muni.fi.pa165.medystem.usermanagement.entity.Role;
import cz.muni.fi.pa165.medystem.usermanagement.mapper.RoleMapper;
import cz.muni.fi.pa165.medystem.usermanagement.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class RoleFacade {

    private final RoleService roleService;
    private final RoleMapper roleMapper;

    @Autowired
    public RoleFacade(RoleService roleService, RoleMapper roleMapper) {
        this.roleService = roleService;
        this.roleMapper = roleMapper;
    }

    public RoleViewResponseDto createRole(RoleViewResponseDto roleViewResponseDto) {
        Role role = roleMapper.roleViewDtoToRole(roleViewResponseDto);
        role = roleService.createRole(role);
        return roleMapper.roleToRoleViewDto(role);
    }

    public RoleViewResponseDto updateRole(Long id, RoleViewResponseDto roleViewResponseDto) {
        Role roleDetails = roleMapper.roleViewDtoToRole(roleViewResponseDto);
        Role updatedRole = roleService.updateRole(id, roleDetails);
        return roleMapper.roleToRoleViewDto(updatedRole);
    }

    public void deleteRole(Long id) {
        roleService.deleteRole(id);
    }

    public RoleViewResponseDto getRoleById(Long id) {
        Role role = roleService.getRoleById(id);
        return roleMapper.roleToRoleViewDto(role);
    }

    public Page<RoleViewResponseDto> getAllRoles(Pageable pageable) {
        return roleService.getAllRoles(pageable)
                .map(roleMapper::roleToRoleViewDto);
    }
}
