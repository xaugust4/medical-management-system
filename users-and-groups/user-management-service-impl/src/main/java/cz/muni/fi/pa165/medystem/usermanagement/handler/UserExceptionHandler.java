package cz.muni.fi.pa165.medystem.usermanagement.handler;

import cz.muni.fi.pa165.medystem.usermanagement.controller.UserRestController;
import cz.muni.fi.pa165.medystem.usermanagement.exception.UserNotFoundException;
import jakarta.persistence.PersistenceException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice(assignableTypes = {UserRestController.class})
public class UserExceptionHandler extends GeneralExceptionHandler {

    @ExceptionHandler(value = UserNotFoundException.class)
    protected ResponseEntity<Object> handleUserNotFoundException(UserNotFoundException ex) {
        String error = "User not found";
        return buildResponseEntity(new ApiError(HttpStatus.NOT_FOUND, error, ex));
    }

    @ExceptionHandler(value = PersistenceException.class)
    protected ResponseEntity<Object> handlePersistenceException(PersistenceException ex) {
        if (ex.getMessage().contains("violates unique constraint")) {
            return buildResponseEntity(new ApiError(HttpStatus.CONFLICT, "User with desired username already exists", ex));
        }
        String error = "Error when persisting the user";
        return buildResponseEntity(new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, error, ex));
    }
}
