package cz.muni.fi.pa165.medystem.usermanagement.handler;

import cz.muni.fi.pa165.medystem.usermanagement.controller.RoleRestController;
import cz.muni.fi.pa165.medystem.usermanagement.exception.UserNotFoundException;
import jakarta.persistence.PersistenceException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice(assignableTypes = {RoleRestController.class})
public class RoleExceptionHandler extends GeneralExceptionHandler {

    @ExceptionHandler(value = UserNotFoundException.class)
    protected ResponseEntity<Object> handleUserNotFoundException(UserNotFoundException ex) {
        String error = "Role not found";
        return buildResponseEntity(new ApiError(HttpStatus.NOT_FOUND, error, ex));
    }

    @ExceptionHandler(value = PersistenceException.class)
    protected ResponseEntity<Object> handlePersistenceException(PersistenceException ex) {
        String error = "Error when persisting the role";
        return buildResponseEntity(new ApiError(HttpStatus.NOT_FOUND, error, ex));
    }

}
