openapi: 3.0.1
info:
  title: Users and Roles API
  description: API for managing users and their roles
  version: v1
servers:
  - url: http://localhost:8080
    description: Generated server url
tags:
  - name: Role
    description: Operations related to Roles
  - name: User
    description: Operations related to Users
paths:
  /api/users/{id}:
    get:
      tags:
        - User
      summary: Get a user by ID
      description: Retrieves details of a specific user by ID
      operationId: getUserById
      parameters:
        - name: id
          in: path
          description: The ID of the user to retrieve
          required: true
          schema:
            type: integer
            format: int64
      responses:
        '200':
          description: User found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UserViewResponseDto'
        '404':
          description: User not found
    put:
      tags:
        - User
      summary: Update a user
      description: Updates the details of an existing user specified by ID
      operationId: updateUser
      parameters:
        - name: id
          in: path
          description: The ID of the user to update
          required: true
          schema:
            type: integer
            format: int64
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/UserUpdateRequestDto'
        required: true
      responses:
        '200':
          description: User updated
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UserViewResponseDto'
        '404':
          description: User not found
        '500':
          description: Error when persisting the user
    delete:
      tags:
        - User
      summary: Delete a user
      description: Deletes a user specified by ID
      operationId: deleteUser
      parameters:
        - name: id
          in: path
          description: The ID of the user to delete
          required: true
          schema:
            type: integer
            format: int64
      responses:
        '204':
          description: User deleted
        '404':
          description: User not found
  /api/users/roles/{id}:
    get:
      tags:
        - Role
      summary: Get a role by ID
      description: Retrieves details of a specific role by ID
      operationId: getRoleById
      parameters:
        - name: id
          in: path
          description: The ID of the role to retrieve
          required: true
          schema:
            type: integer
            format: int64
      responses:
        '200':
          description: Role found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RoleViewResponseDto'
        '404':
          description: Role not found
        '500':
          description: Internal server error
    put:
      tags:
        - Role
      summary: Update an existing role
      description: Updates the details of an existing role specified by ID
      operationId: updateRole
      parameters:
        - name: id
          in: path
          description: The ID of the role to update
          required: true
          schema:
            type: integer
            format: int64
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/RoleViewResponseDto'
        required: true
      responses:
        '200':
          description: Role updated
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RoleViewResponseDto'
        '404':
          description: Role not found
        '500':
          description: Internal server error
    delete:
      tags:
        - Role
      summary: Delete a role
      description: Deletes a role specified by ID
      operationId: deleteRole
      parameters:
        - name: id
          in: path
          description: The ID of the role to delete
          required: true
          schema:
            type: integer
            format: int64
      responses:
        '204':
          description: Role deleted
        '404':
          description: Role not found
        '500':
          description: Internal server error
  /api/users/roles/create:
    post:
      tags:
        - Role
      summary: Create a new role
      description: Creates a new role with the specified details
      operationId: createRole
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/RoleViewResponseDto'
        required: true
      responses:
        '201':
          description: Role created
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RoleViewResponseDto'
        '409':
          description: Role with desired details already exists
        '500':
          description: Internal server error due to persistence issue
  /api/users/create:
    post:
      tags:
        - User
      summary: Create a new user
      description: Creates a new user with the given details
      operationId: createUser
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/UserCreateRequestDto'
        required: true
      responses:
        '201':
          description: User created
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UserViewResponseDto'
        '409':
          description: User with desired username already exists
        '500':
          description: Error when persisting the user
  /api/users/username/{username}:
    get:
      tags:
        - User
      summary: Get a user by username
      description: Retrieves details of a specific user by username
      operationId: getUserByUsername
      parameters:
        - name: username
          in: path
          description: The username of the user to retrieve
          required: true
          schema:
            type: string
      responses:
        '200':
          description: User found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UserViewResponseDto'
  /api/users/roles/all:
    get:
      tags:
        - Role
      summary: Get all roles
      description: Retrieves details of all roles
      operationId: getAllRoles
      parameters:
        - name: page
          in: query
          required: false
          schema:
            type: integer
            format: int32
            default: 0
        - name: size
          in: query
          required: false
          schema:
            maximum: 50
            type: integer
            format: int32
            default: 10
        - name: sort
          in: query
          required: false
          schema:
            type: array
            items:
              type: string
            default:
              - id
              - asc
      responses:
        '200':
          description: Roles found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Page'
  /api/users/all:
    get:
      tags:
        - User
      summary: Get all users
      description: Retrieves details of all users
      operationId: getAllUsers
      parameters:
        - name: page
          in: query
          required: false
          schema:
            type: integer
            format: int32
            default: 0
        - name: size
          in: query
          required: false
          schema:
            maximum: 50
            type: integer
            format: int32
            default: 10
        - name: sort
          in: query
          required: false
          schema:
            type: array
            items:
              type: string
            default:
              - id
              - asc
      responses:
        '200':
          description: Users found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Page'
components:
  schemas:
    UserUpdateRequestDto:
      required:
        - firstName
        - lastName
        - username
      type: object
      properties:
        username:
          maxLength: 32
          minLength: 5
          type: string
          description: The new or existing username of the user
          example: jane_doe
        firstName:
          maxLength: 32
          minLength: 2
          type: string
          description: The first name of the user
          example: Jane
        lastName:
          maxLength: 32
          minLength: 2
          type: string
          description: The last name of the user
          example: Doe
      description: User update data
    UserViewResponseDto:
      type: object
      properties:
        id:
          type: integer
          description: The unique identifier of the user
          format: int64
          example: 1
        username:
          type: string
          description: The username of the user
          example: john_doe
        firstName:
          type: string
          description: The first name of the user
          example: John
        lastName:
          type: string
          description: The last name of the user
          example: Doe
        registrationDate:
          type: string
          description: The registration date of the user in ISO 8601 format
          example: '2023-04-01T12:00:00Z'
        roles:
          uniqueItems: true
          type: array
          description: A set of roles assigned to the user
          example:
            - USER
            - ADMIN
          items:
            type: string
            description: A set of roles assigned to the user
            example: '["USER","ADMIN"]'
      description: Data Transfer Object for viewing a user's details
    RoleViewResponseDto:
      type: object
      properties:
        id:
          type: integer
          description: The unique identifier of the role
          format: int64
          example: 1
        roleName:
          type: string
          description: The name of the role
          example: john_doe
        description:
          type: string
          description: A brief description of the role
          example: Standard user role with basic permissions.
      description: Data Transfer Object for viewing a role's details
    UserCreateRequestDto:
      required:
        - firstName
        - lastName
        - username
      type: object
      properties:
        username:
          maxLength: 32
          minLength: 5
          type: string
          description: The username of the new user
          example: john_doe
        firstName:
          maxLength: 32
          minLength: 2
          type: string
          description: The first name of the new user
          example: John
        lastName:
          maxLength: 32
          minLength: 2
          type: string
          description: The last name of the new user
          example: Doe
      description: User creation data
    Page:
      type: object
      properties:
        totalPages:
          type: integer
          format: int32
        totalElements:
          type: integer
          format: int64
        size:
          type: integer
          format: int32
        content:
          type: array
          items:
            type: object
        number:
          type: integer
          format: int32
        sort:
          type: array
          items:
            $ref: '#/components/schemas/SortObject'
        first:
          type: boolean
        last:
          type: boolean
        numberOfElements:
          type: integer
          format: int32
        pageable:
          $ref: '#/components/schemas/PageableObject'
        empty:
          type: boolean
    PageableObject:
      type: object
      properties:
        offset:
          type: integer
          format: int64
        sort:
          type: array
          items:
            $ref: '#/components/schemas/SortObject'
        pageSize:
          type: integer
          format: int32
        pageNumber:
          type: integer
          format: int32
        paged:
          type: boolean
        unpaged:
          type: boolean
    SortObject:
      type: object
      properties:
        direction:
          type: string
        nullHandling:
          type: string
        ascending:
          type: boolean
        property:
          type: string
        ignoreCase:
          type: boolean
