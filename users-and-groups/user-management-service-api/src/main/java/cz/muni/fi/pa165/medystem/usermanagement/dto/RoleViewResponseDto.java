package cz.muni.fi.pa165.medystem.usermanagement.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Schema(description = "Data Transfer Object for viewing a role's details")
public class RoleViewResponseDto {

    @Schema(description = "The unique identifier of the role", example = "1")
    private Long id;

    @Schema(description = "The name of the role", example = "john_doe")
    private String roleName;

    @Schema(description = "A brief description of the role", example = "Standard user role with basic permissions.")
    private String description;

    @Override
    public String toString() {
        return "RoleViewDto{" +
                "id=" + id +
                ", roleName='" + roleName + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
