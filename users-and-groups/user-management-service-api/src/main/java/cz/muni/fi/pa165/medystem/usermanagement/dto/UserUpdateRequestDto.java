package cz.muni.fi.pa165.medystem.usermanagement.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Schema(description = "Data Transfer Object for updating an existing user")
public class UserUpdateRequestDto {

    @Schema(description = "The new or existing username of the user", example = "jane_doe")
    @Size(min = 5, max = 32, message = "{validation.name.size}")
    @NotNull
    private String username;

    @Schema(description = "The first name of the user", example = "Jane")
    @Size(min = 2, max = 32, message = "{validation.name.size}")
    @NotNull
    private String firstName;

    @Schema(description = "The last name of the user", example = "Doe")
    @Size(min = 2, max = 32, message = "{validation.name.size}")
    @NotNull
    private String lastName;

    @Override
    public String toString() {
        return "UserUpdateDto{" +
                "username='" + username + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}
