package cz.muni.fi.pa165.medystem.usermanagement.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Schema(description = "Data Transfer Object for viewing a user's details")
public class UserViewResponseDto {

    @Schema(description = "The unique identifier of the user", example = "1")
    private Long id;

    @Schema(description = "The username of the user", example = "john_doe")
    private String username;

    @Schema(description = "The first name of the user", example = "John")
    private String firstName;

    @Schema(description = "The last name of the user", example = "Doe")
    private String lastName;

    @Schema(description = "The registration date of the user in ISO 8601 format", example = "2023-04-01T12:00:00Z")
    private String registrationDate;

    @Schema(description = "A set of roles assigned to the user", example = "[\"USER\", \"ADMIN\"]")
    private Set<String> roles;

    @Override
    public String toString() {
        return "UserViewDto{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", registrationDate='" + registrationDate + '\'' +
                ", roles=" + roles +
                '}';
    }
}
