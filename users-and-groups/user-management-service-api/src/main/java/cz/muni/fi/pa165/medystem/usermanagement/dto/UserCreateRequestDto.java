package cz.muni.fi.pa165.medystem.usermanagement.dto;


import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Schema(description = "Data Transfer Object for creating a new user")
public class UserCreateRequestDto {

    @Schema(description = "The username of the new user", example = "john_doe", required = true)
    @Size(min = 5, max = 32, message = "{validation.name.size}")
    @NotNull
    private String username;

    @Schema(description = "The first name of the new user", example = "John", required = true)
    @Size(min = 2, max = 32, message = "{validation.name.size}")
    @NotNull
    private String firstName;

    @Schema(description = "The last name of the new user", example = "Doe", required = true)
    @Size(min = 2, max = 32, message = "{validation.name.size}")
    @NotNull
    private String lastName;

    @Override
    public String toString() {
        return "UserCreateDto{" +
                "username='" + username + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}
